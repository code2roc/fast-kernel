package com.code2roc.fastboot.condition;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

public class DefaultWebConfigCondition implements Condition {
    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        String projectName = context.getEnvironment().getProperty("system.projectName");
        return projectName.equals("fastboot");
    }
}
