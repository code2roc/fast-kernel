package com.code2roc.fastboot.config;

import com.code2roc.fastboot.Intecept.PageRedirectInterceptor;
import com.code2roc.fastboot.condition.DefaultWebConfigCondition;
import com.code2roc.fastboot.framework.global.WebConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;

import javax.servlet.MultipartConfigElement;

@Configuration
@EnableTransactionManagement
@EnableAspectJAutoProxy
@EnableAsync
@Conditional({DefaultWebConfigCondition.class})
public class BootWebConfig extends WebConfig {
    @Autowired
    private PageRedirectInterceptor pageRedirectInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(pageRedirectInterceptor).addPathPatterns("/**");
        super.addInterceptors(registry);
    }

    @Value("${system.maxFileSize}")
    private int maxFileSize;

    @Bean
    public MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        //  单个数据大小
        factory.setMaxFileSize(maxFileSize + "MB"); // KB,MB
        /// 总上传数据大小
        factory.setMaxRequestSize(maxFileSize * 10 + "MB");
        return factory.createMultipartConfig();
    }
}
