package com.code2roc.fastboot.model;


import com.code2roc.fastboot.framework.util.CommonUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ZTreeNode {
    private String id;
    private String name;
    private String pId;
    private Boolean checked;
    private Boolean topNode;
    private Boolean isParent;
    private Boolean open;
    private String icon;
    private String iconClose;
    private String iconOpen;
    private Boolean nocheck;
    private Integer sotNum = 0;
    private List<ZTreeNode> children;
    private Map<String, Object> attributes;
    public ZTreeNode(){
        attributes = new HashMap<String, Object>();
        isParent = false;
        checked = false;
        topNode = false;
        nocheck = false;
        icon = CommonUtil.getRootPath() + "images/tree_leaf_icon.png";
        iconClose = CommonUtil.getRootPath() + "images/tree_folder_iocn.png";
        iconOpen = CommonUtil.getRootPath() + "images/tree_folderopen_iocn.png";
    }

    public void addAttribute(String key,Object value){
        if(attributes.containsKey(key)){
            attributes.replace(key,value);
        }else{
            attributes.put(key,value);
        }
    }

    public void addChildNode(ZTreeNode node){
        if(children==null){
            children = new ArrayList<>();
        }
        children.add(node);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getpId() {
        return pId;
    }

    public void setpId(String pId) {
        this.pId = pId;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    public Boolean getTopNode() {
        return topNode;
    }

    public void setTopNode(Boolean topNode) {
        this.topNode = topNode;
    }

    public Boolean getParent() {
        return isParent;
    }

    public void setParent(Boolean parent) {
        isParent = parent;
    }

    public Boolean getOpen() {
        return open;
    }

    public void setOpen(Boolean open) {
        this.open = open;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getIconClose() {
        return iconClose;
    }

    public void setIconClose(String iconClose) {
        this.iconClose = iconClose;
    }

    public String getIconOpen() {
        return iconOpen;
    }

    public void setIconOpen(String iconOpen) {
        this.iconOpen = iconOpen;
    }

    public Boolean getNocheck() {
        return nocheck;
    }

    public void setNocheck(Boolean nocheck) {
        this.nocheck = nocheck;
    }

    public Integer getSotNum() {
        return sotNum;
    }

    public void setSotNum(Integer sotNum) {
        this.sotNum = sotNum;
    }

    public List<ZTreeNode> getChildren() {
        return children;
    }

    public void setChildren(List<ZTreeNode> children) {
        this.children = children;
    }

    public Map<String, Object> getAttributes() {
        return attributes;
    }

    public void setAttributes(Map<String, Object> attributes) {
        this.attributes = attributes;
    }
}
