package com.code2roc.fastboot.template;

import com.code2roc.fastboot.util.AuthUtil;
import com.code2roc.fastboot.framework.template.BaseController;
import org.springframework.beans.factory.annotation.Autowired;

public class BaseBootController extends BaseController {
    @Autowired
    public AuthUtil authUtil;
}
