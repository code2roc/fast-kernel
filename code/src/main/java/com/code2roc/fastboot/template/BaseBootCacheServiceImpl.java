package com.code2roc.fastboot.template;

import com.code2roc.fastboot.framework.datasource.TargetDataSource;
import com.code2roc.fastboot.framework.template.BaseCacheServiceImpl;
import com.code2roc.fastboot.framework.util.CommonUtil;
import com.code2roc.fastboot.framework.util.StringUtil;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
@TargetDataSource
public class BaseBootCacheServiceImpl<T extends BaseBootModel> extends BaseCacheServiceImpl<T> {
    @Override
    public void insert(T entity) {
        if(StringUtil.isEmpty(entity.getRow_id())){
            entity.setRow_id(CommonUtil.getNewGuid());
        }
        entity.setGmt_create(new Date());
        entity.setGmt_modified(new Date());
        super.insert(entity);
    }

    @Override
    public void update(T entity) {
        entity.setGmt_modified(new Date());
        super.update(entity);
    }
}
