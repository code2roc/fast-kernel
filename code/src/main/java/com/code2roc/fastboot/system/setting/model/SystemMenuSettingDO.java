package com.code2roc.fastboot.system.setting.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.code2roc.fastboot.template.BaseBootModel;

import java.util.Date;

@TableName("system_menu_setting")
public class SystemMenuSettingDO extends BaseBootModel {
    @TableId(value = "row_id")
    private String row_id;
    @TableField(value = "sort_num")
    private Integer sort_num;
    @TableField(value = "gmt_create")
    private Date gmt_create;
    @TableField(value = "gmt_modified")
    private Date gmt_modified;
    @TableField("menu_id")
    private String menu_id;
    @TableField("setting_mode")
    private Integer setting_mode;
    @TableField("setting_id")
    private String setting_id;

    @TableField(exist = false)
    private String setting_name;

    public String getMenu_id() {
        return menu_id;
    }

    public void setMenu_id(String menu_id) {
        this.menu_id = menu_id;
    }

    public Integer getSetting_mode() {
        return setting_mode;
    }

    public void setSetting_mode(Integer setting_mode) {
        this.setting_mode = setting_mode;
    }

    public String getSetting_id() {
        return setting_id;
    }

    public void setSetting_id(String setting_id) {
        this.setting_id = setting_id;
    }

    @Override
    public String getRow_id() {
        return row_id;
    }

    @Override
    public void setRow_id(String row_id) {
        this.row_id = row_id;
    }

    @Override
    public Integer getSort_num() {
        return sort_num;
    }

    @Override
    public void setSort_num(Integer sort_num) {
        this.sort_num = sort_num;
    }

    @Override
    public Date getGmt_create() {
        return gmt_create;
    }

    @Override
    public void setGmt_create(Date gmt_create) {
        this.gmt_create = gmt_create;
    }

    @Override
    public Date getGmt_modified() {
        return gmt_modified;
    }

    @Override
    public void setGmt_modified(Date gmt_modified) {
        this.gmt_modified = gmt_modified;
    }
}
