package com.code2roc.fastboot.system.common.service.impl;

import com.code2roc.fastboot.system.common.model.SystemAttachDO;
import com.code2roc.fastboot.system.common.service.ISystemAttachService;
import com.code2roc.fastboot.template.BaseBootServiceImpl;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
public class SystemAttachServiceImpl extends BaseBootServiceImpl<SystemAttachDO> implements ISystemAttachService {
    @Override
    public List<SystemAttachDO> selectListByGroupGuid(String group_guid) {
        String sql = "group_guid = #{group_guid}";
        HashMap paramMap = new HashMap();
        paramMap.put("group_guid",group_guid);
        return selectList("*",sql,"gmt_create",paramMap);
    }

    @Override
    public List<SystemAttachDO> selectListByGroupGuid(String group_guid, String group_type) {
        String sql = "group_guid = #{group_guid} and group_type = #{group_type}";
        HashMap paramMap = new HashMap();
        paramMap.put("group_guid",group_guid);
        paramMap.put("group_type",group_type);
        return selectList("*",sql,"gmt_create",paramMap);
    }
}
