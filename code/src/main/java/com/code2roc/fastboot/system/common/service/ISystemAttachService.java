package com.code2roc.fastboot.system.common.service;

import com.code2roc.fastboot.system.common.model.SystemAttachDO;
import com.code2roc.fastboot.template.BaseBootService;

import java.util.List;

public interface ISystemAttachService extends BaseBootService<SystemAttachDO> {
    List<SystemAttachDO> selectListByGroupGuid(String group_guid);

    List<SystemAttachDO> selectListByGroupGuid(String group_guid,String group_type);
}
