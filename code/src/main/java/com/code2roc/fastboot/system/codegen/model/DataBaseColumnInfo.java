package com.code2roc.fastboot.system.codegen.model;

public class DataBaseColumnInfo {
    private String table_name;
    private String column_name;
    private String data_type;
    private String numeric_precision;
    private String numeric_scale;
    private String character_maximum_length;
    private String column_comment;

    public String getTable_name() {
        return table_name;
    }

    public void setTable_name(String table_name) {
        this.table_name = table_name;
    }

    public String getColumn_name() {
        return column_name;
    }

    public void setColumn_name(String column_name) {
        this.column_name = column_name;
    }

    public String getData_type() {
        return data_type;
    }

    public void setData_type(String data_type) {
        this.data_type = data_type;
    }

    public String getNumeric_precision() {
        return numeric_precision;
    }

    public void setNumeric_precision(String numeric_precision) {
        this.numeric_precision = numeric_precision;
    }

    public String getNumeric_scale() {
        return numeric_scale;
    }

    public void setNumeric_scale(String numeric_scale) {
        this.numeric_scale = numeric_scale;
    }

    public String getCharacter_maximum_length() {
        return character_maximum_length;
    }

    public void setCharacter_maximum_length(String character_maximum_length) {
        this.character_maximum_length = character_maximum_length;
    }

    public String getColumn_comment() {
        return column_comment;
    }

    public void setColumn_comment(String column_comment) {
        this.column_comment = column_comment;
    }
}
