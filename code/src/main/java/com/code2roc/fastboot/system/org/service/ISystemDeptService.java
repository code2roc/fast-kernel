package com.code2roc.fastboot.system.org.service;

import com.code2roc.fastboot.system.org.model.SystemDeptDO;
import com.code2roc.fastboot.template.BaseBootService;

import java.util.List;

public interface ISystemDeptService extends BaseBootService<SystemDeptDO> {
    List<String> getAllSubDeptIDList(String group_id);

    boolean checkExistSub(String parent_id);

    String getFullPath(String dept_id, List<SystemDeptDO> lst_All);
}
