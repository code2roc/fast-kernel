package com.code2roc.fastboot.system.setting.service;

import com.code2roc.fastboot.system.setting.model.SystemConfigDO;
import com.code2roc.fastboot.template.BaseBootService;

public interface ISystemConfigService extends BaseBootService<SystemConfigDO> {
    SystemConfigDO selectOneByConfigName(String config_name);
}
