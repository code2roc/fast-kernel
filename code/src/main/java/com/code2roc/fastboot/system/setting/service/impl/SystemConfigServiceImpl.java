package com.code2roc.fastboot.system.setting.service.impl;

import com.code2roc.fastboot.system.setting.model.SystemConfigDO;
import com.code2roc.fastboot.system.setting.service.ISystemConfigService;
import com.code2roc.fastboot.template.BaseBootServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class SystemConfigServiceImpl  extends BaseBootServiceImpl<SystemConfigDO> implements ISystemConfigService {
    @Override
    public SystemConfigDO selectOneByConfigName(String config_name) {
        return selectOneByField("config_name",config_name);
    }
}
