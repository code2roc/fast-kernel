package com.code2roc.fastboot.system.codegen.controller;

import com.code2roc.fastboot.system.codegen.model.SystemTableDO;
import com.code2roc.fastboot.system.codegen.service.ISystemColumnService;
import com.code2roc.fastboot.system.codegen.service.ISystemTableService;
import com.code2roc.fastboot.template.BaseBootController;
import com.code2roc.fastboot.framework.formupdate.UpdateRequestBody;
import com.code2roc.fastboot.framework.model.Result;
import com.code2roc.fastboot.framework.util.ConvertOp;
import com.code2roc.fastboot.framework.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/system/codegen/table")
public class SystemTableController extends BaseBootController {
    @Autowired
    private ISystemTableService service;
    @Autowired
    private ISystemColumnService columnService;

    @ResponseBody
    @PostMapping("/list")
    public Object list(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        String sql = "1=1";
        HashMap<String, Object> paramMap = new HashMap<>();
        String table_name = ConvertOp.convert2String(params.get("table_name"));
        String table_description = ConvertOp.convert2String(params.get("table_description"));
        if (!StringUtil.isEmpty(table_name)) {
            sql += " and table_name like #{table_name}";
            paramMap.put("table_name", "%" + table_name + "%");
        }
        if (!StringUtil.isEmpty(table_description)) {
            sql += " and table_description like #{table_description}";
            paramMap.put("table_description", "%" + table_description + "%");
        }
        List<SystemTableDO> rows = service.selectPageList("*", sql, "sort_num desc", paramMap);
        int total = service.selectCount(sql, paramMap);
        result.add("rows", rows);
        result.add("total", total);
        return result;
    }

    @ResponseBody
    @PostMapping("/detail")
    public Object detail(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        String row_id = ConvertOp.convert2String(params.get("row_id"));
        result.add("obj", service.selectOne(row_id));
        return result;
    }

    @ResponseBody
    @PostMapping("/update")
    @Transactional(rollbackFor = {Exception.class},isolation = Isolation.READ_UNCOMMITTED)
    public Object update(@UpdateRequestBody SystemTableDO entity) {
        Result result = Result.okResult();
        service.update(entity);
        return result;
    }

    @ResponseBody
    @PostMapping("/delete")
    @Transactional(rollbackFor = {Exception.class},isolation = Isolation.READ_UNCOMMITTED)
    public Object delete(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        String row_id = ConvertOp.convert2String(params.get("row_id"));
        service.delete(row_id);
        columnService.deleteByField("table_id",row_id);
        return result;
    }
}
