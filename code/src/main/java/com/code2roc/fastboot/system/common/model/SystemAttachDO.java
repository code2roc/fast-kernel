package com.code2roc.fastboot.system.common.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.code2roc.fastboot.template.BaseBootModel;

import java.util.Date;

@TableName("system_attach")
public class SystemAttachDO extends BaseBootModel {
    @TableId(value = "row_id")
    private String row_id;
    @TableField(value = "sort_num")
    private Integer sort_num;
    @TableField(value = "gmt_create")
    private Date gmt_create;
    @TableField(value = "gmt_modified")
    private Date gmt_modified;
    @TableField("attach_name")
    private String attach_name;
    @TableField("attach_type")
    private String attach_type;
    @TableField("attach_size")
    private long attach_size;
    @TableField("attach_storage_type")
    private Integer attach_storage_type;
    @TableField("attach_virtual_path")
    private String attach_virtual_path;
    @TableField("group_guid")
    private String group_guid;
    @TableField("group_type")
    private String group_type;
    @TableField("attach_status")
    private Integer attach_status;
    @TableField("upload_user_id")
    private String upload_user_id;
    @TableField("upload_user_name")
    private String upload_user_name;

    public String getAttach_name() {
        return attach_name;
    }

    public void setAttach_name(String attach_name) {
        this.attach_name = attach_name;
    }

    public String getAttach_type() {
        return attach_type;
    }

    public void setAttach_type(String attach_type) {
        this.attach_type = attach_type;
    }

    public long getAttach_size() {
        return attach_size;
    }

    public void setAttach_size(long attach_size) {
        this.attach_size = attach_size;
    }

    public Integer getAttach_storage_type() {
        return attach_storage_type;
    }

    public void setAttach_storage_type(Integer attach_storage_type) {
        this.attach_storage_type = attach_storage_type;
    }

    public String getAttach_virtual_path() {
        return attach_virtual_path;
    }

    public void setAttach_virtual_path(String attach_virtual_path) {
        this.attach_virtual_path = attach_virtual_path;
    }

    public String getGroup_guid() {
        return group_guid;
    }

    public void setGroup_guid(String group_guid) {
        this.group_guid = group_guid;
    }

    public String getGroup_type() {
        return group_type;
    }

    public void setGroup_type(String group_type) {
        this.group_type = group_type;
    }

    public Integer getAttach_status() {
        return attach_status;
    }

    public void setAttach_status(Integer attach_status) {
        this.attach_status = attach_status;
    }

    public String getUpload_user_id() {
        return upload_user_id;
    }

    public void setUpload_user_id(String upload_user_id) {
        this.upload_user_id = upload_user_id;
    }

    public String getUpload_user_name() {
        return upload_user_name;
    }

    public void setUpload_user_name(String upload_user_name) {
        this.upload_user_name = upload_user_name;
    }

    @Override
    public String getRow_id() {
        return row_id;
    }

    @Override
    public void setRow_id(String row_id) {
        this.row_id = row_id;
    }

    @Override
    public Integer getSort_num() {
        return sort_num;
    }

    @Override
    public void setSort_num(Integer sort_num) {
        this.sort_num = sort_num;
    }

    @Override
    public Date getGmt_create() {
        return gmt_create;
    }

    @Override
    public void setGmt_create(Date gmt_create) {
        this.gmt_create = gmt_create;
    }

    @Override
    public Date getGmt_modified() {
        return gmt_modified;
    }

    @Override
    public void setGmt_modified(Date gmt_modified) {
        this.gmt_modified = gmt_modified;
    }
}
