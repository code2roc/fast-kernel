package com.code2roc.fastboot.system.common.service.impl;

import com.code2roc.fastboot.system.common.model.SystemJobDO;
import com.code2roc.fastboot.system.common.service.ISystemJobService;
import com.code2roc.fastboot.template.BaseBootServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class SystemJobServiceImpl extends BaseBootServiceImpl<SystemJobDO> implements ISystemJobService {
}
