package com.code2roc.fastboot.system.common.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.code2roc.fastboot.template.BaseBootModel;

import java.util.Date;

@TableName("system_job")
public class SystemJobDO extends BaseBootModel {
    @TableId(value = "row_id")
    private String row_id;
    @TableField(value = "sort_num")
    private Integer sort_num;
    @TableField(value = "gmt_create")
    private Date gmt_create;
    @TableField(value = "gmt_modified")
    private Date gmt_modified;
    @TableField("job_name")
    private String job_name;
    @TableField("job_type")
    private Integer job_type;
    @TableField("job_class_path")
    private String job_class_path;
    @TableField("execute_peroid")
    private Integer execute_peroid;
    @TableField("execute_unit")
    private Integer execute_unit;
    @TableField("corn_express")
    private String corn_express;
    @TableField("job_status")
    private Integer job_status;
    @TableField("remark")
    private String remark;

    public String getJob_name() {
        return job_name;
    }

    public void setJob_name(String job_name) {
        this.job_name = job_name;
    }

    public Integer getJob_type() {
        return job_type;
    }

    public void setJob_type(Integer job_type) {
        this.job_type = job_type;
    }

    public String getJob_class_path() {
        return job_class_path;
    }

    public void setJob_class_path(String job_class_path) {
        this.job_class_path = job_class_path;
    }

    public Integer getExecute_peroid() {
        return execute_peroid;
    }

    public void setExecute_peroid(Integer execute_peroid) {
        this.execute_peroid = execute_peroid;
    }

    public Integer getExecute_unit() {
        return execute_unit;
    }

    public void setExecute_unit(Integer execute_unit) {
        this.execute_unit = execute_unit;
    }

    public String getCorn_express() {
        return corn_express;
    }

    public void setCorn_express(String corn_express) {
        this.corn_express = corn_express;
    }

    public Integer getJob_status() {
        return job_status;
    }

    public void setJob_status(Integer job_status) {
        this.job_status = job_status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String getRow_id() {
        return row_id;
    }

    @Override
    public void setRow_id(String row_id) {
        this.row_id = row_id;
    }

    @Override
    public Integer getSort_num() {
        return sort_num;
    }

    @Override
    public void setSort_num(Integer sort_num) {
        this.sort_num = sort_num;
    }

    @Override
    public Date getGmt_create() {
        return gmt_create;
    }

    @Override
    public void setGmt_create(Date gmt_create) {
        this.gmt_create = gmt_create;
    }

    @Override
    public Date getGmt_modified() {
        return gmt_modified;
    }

    @Override
    public void setGmt_modified(Date gmt_modified) {
        this.gmt_modified = gmt_modified;
    }
}
