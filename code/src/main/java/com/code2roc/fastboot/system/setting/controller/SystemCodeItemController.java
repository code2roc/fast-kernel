package com.code2roc.fastboot.system.setting.controller;

import com.alibaba.fastjson.JSON;
import com.code2roc.fastboot.framework.cache.AutoRefreshCache;
import com.code2roc.fastboot.framework.cache.CacheUtil;
import com.code2roc.fastboot.system.cache.DataBaseDicCacheInfo;
import com.code2roc.fastboot.system.setting.model.SystemCodeItemDO;
import com.code2roc.fastboot.system.setting.model.SystemCodeMainDO;
import com.code2roc.fastboot.system.setting.service.ISystemCodeItemService;
import com.code2roc.fastboot.system.setting.service.ISystemCodeMainService;
import com.code2roc.fastboot.template.BaseBootController;
import com.code2roc.fastboot.framework.auth.AnonymousAccess;
import com.code2roc.fastboot.framework.datadic.CodeItem;
import com.code2roc.fastboot.framework.model.Result;
import com.code2roc.fastboot.framework.util.ConvertOp;
import com.code2roc.fastboot.framework.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/system/setting/codeitem")
public class SystemCodeItemController extends BaseBootController {
    @Autowired
    private ISystemCodeItemService service;
    @Autowired
    private CacheUtil cacheUtil;

    @ResponseBody
    @PostMapping("/getItemList")
    public Object getItemList(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        String sql = "1=1";
        HashMap<String, Object> paramMap = new HashMap<>();
        String code_id = ConvertOp.convert2String(params.get("code_id"));
        List<SystemCodeItemDO> codeItemDOList = service.selectListByField("code_id", code_id);
        result.add("obj", codeItemDOList);
        return result;
    }

    @ResponseBody
    @PostMapping("/updateItemList")
    @AutoRefreshCache("DataBaseDic")
    @Transactional(rollbackFor = {Exception.class}, isolation = Isolation.READ_UNCOMMITTED)
    public Object updateItemList(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        String code_id = ConvertOp.convert2String(params.get("code_id"));
        String entityListStr = ConvertOp.convert2String(params.get("entityList"));
        List<SystemCodeItemDO> entityList = JSON.parseArray(entityListStr, SystemCodeItemDO.class);
        service.deleteByField("code_id", code_id);
        for (SystemCodeItemDO entity : entityList) {
            if(!StringUtil.isEmpty(entity.getItem_text())){
                service.insert(entity);
            }
        }
        return result;
    }

    @ResponseBody
    @PostMapping("/getCodeDetail")
    @AnonymousAccess
    public Object getCodeDetail(@RequestBody Map<String, Object> params){
        String codeName = ConvertOp.convert2String(params.get("codeName"));
        if(StringUtil.isEmpty(codeName)){
            return Result.errorResult().add("msg","代码项名称不能为空");
        }
        if(!cacheUtil.existCache("DataBaseDic",codeName)){
            return Result.errorResult().add("msg","代码项不存在");
        }else{
            DataBaseDicCacheInfo dataBaseDicCacheInfo = (DataBaseDicCacheInfo)cacheUtil.getCache("DataBaseDic",codeName);
            return Result.okResult().add("obj",dataBaseDicCacheInfo.getCodeItemList());
        }
    }
}
