package com.code2roc.fastboot.system.enums;

public class SocketEnum {
    public enum SocketInfo {
        NewSystemMessage("Message", "NewMessage"),
        TokenModelChange("Auth", "TokenModelChange");
        private String socketType;
        private String socketHandleType;

        private SocketInfo(String socketType, String socketHandleType) {
            this.socketType = socketType;
            this.socketHandleType = socketHandleType;
        }

        public String getSocketType() {
            return socketType;
        }

        public void setSocketType(String socketType) {
            this.socketType = socketType;
        }

        public String getSocketHandleType() {
            return socketHandleType;
        }

        public void setSocketHandleType(String socketHandleType) {
            this.socketHandleType = socketHandleType;
        }
    }
}
