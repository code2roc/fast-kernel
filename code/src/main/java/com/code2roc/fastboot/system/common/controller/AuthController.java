package com.code2roc.fastboot.system.common.controller;

import com.code2roc.fastboot.model.UserTokenModel;
import com.code2roc.fastboot.system.org.bizlogic.UserLogic;
import com.code2roc.fastboot.system.org.model.SystemDeptDO;
import com.code2roc.fastboot.system.org.model.SystemRoleDO;
import com.code2roc.fastboot.system.org.model.SystemUserDO;
import com.code2roc.fastboot.system.org.service.ISystemDeptService;
import com.code2roc.fastboot.system.org.service.ISystemUserService;
import com.code2roc.fastboot.template.BaseBootController;
import com.code2roc.fastboot.framework.model.Result;
import com.code2roc.fastboot.framework.util.ConvertOp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/system/common/auth")
public class AuthController extends BaseBootController {
    @Autowired
    private ISystemUserService userService;
    @Autowired
    private UserLogic userLogic;
    @Autowired
    private ISystemDeptService deptService;

    @ResponseBody
    @PostMapping("/userLogin")
    public Object userLogin(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        String loginName = ConvertOp.convert2String(params.get("loginName"));
        String password = ConvertOp.convert2String(params.get("password"));
        boolean success = false;
        SystemUserDO userDO = userService.selectOneByField("login_name", loginName);
        if (userDO != null && userDO.getPassword().equals(password)) {
            success = true;
            //生成Token
            UserTokenModel userTokenModel = new UserTokenModel();
            userTokenModel.setUserID(userDO.getRow_id());
            userTokenModel.setUserName(userDO.getUser_name());
            userTokenModel.setLoginName(loginName);
            SystemDeptDO deptDO = deptService.selectOne(userDO.getDept_id());
            if (deptDO != null) {
                userTokenModel.setDeptID(deptDO.getRow_id());
                userTokenModel.setDeptName(deptDO.getDept_name());
            }
            userDO = userLogic.detail(userDO.getRow_id());
            if (userDO.getRoleList() != null) {
                String roleID = "";
                String roleName = "";
                List<SystemRoleDO> roleDOList = userDO.getRoleList().stream().filter(a -> a.isRegist()).collect(Collectors.toList());
                for (SystemRoleDO roleDO : roleDOList) {
                    roleName += roleDO.getRole_name() + "★";
                    roleID += roleDO.getRow_id() + "★";
                }
                userTokenModel.setRoleID(roleID);
                userTokenModel.setRoleName(roleName);
            }
            String token = authUtil.createToken(userTokenModel);
            result.add("token", token);
            result.add("tokenModel", userTokenModel);
        }
        if (success) {
            return result;
        } else {
            return Result.errorResult().setMsg("用户名密码不正确");
        }
    }

    @ResponseBody
    @PostMapping("/userLogOut")
    public Object userLogOut(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        String token = ConvertOp.convert2String(params.get("token"));
        authUtil.removeToken(token);
        return result;
    }

    @ResponseBody
    @PostMapping("/getUserTokenInfo")
    public Object getUserTokenInfo(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        String token = ConvertOp.convert2String(params.get("token"));
        UserTokenModel userTokenModel = authUtil.getUserTokenModel(token);
        result.add("obj", userTokenModel);
        return result;
    }

    @ResponseBody
    @PostMapping("/checkUserInfo")
    public Object checkUserInfo(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        String token = String.valueOf(params.get("token"));
        if (!authUtil.checkTokenValid(token)) {
            result.setCode(-100);
            result.setMsg("身份信息过期或不正确,请重新登");
        }
        return result;
    }
}
