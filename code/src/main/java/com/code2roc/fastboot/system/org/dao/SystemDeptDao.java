package com.code2roc.fastboot.system.org.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.code2roc.fastboot.system.org.model.SystemDeptDO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SystemDeptDao extends BaseMapper<SystemDeptDO> {
}
