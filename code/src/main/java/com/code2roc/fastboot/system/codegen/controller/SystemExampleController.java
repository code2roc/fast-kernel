package com.code2roc.fastboot.system.codegen.controller;

import com.code2roc.fastboot.system.codegen.model.SystemExampleDO;
import com.code2roc.fastboot.system.codegen.service.ISystemExampleService;
import com.code2roc.fastboot.template.BaseBootController;
import com.code2roc.fastboot.framework.model.Result;
import com.code2roc.fastboot.framework.util.ConvertOp;
import com.code2roc.fastboot.framework.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.code2roc.fastboot.framework.formupdate.UpdateRequestBody;
import com.code2roc.fastboot.framework.model.Page;
import com.code2roc.fastboot.framework.submitlock.SubmitLock;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/system/codegen/example")
public class SystemExampleController extends BaseBootController {

    @Autowired
    private ISystemExampleService systemExampleService;

    @ResponseBody
    @PostMapping("/list")
    public Object list(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        HashMap<String, Object> paramMap = new HashMap<>();
        String sql = "1=1";

        String account_name = ConvertOp.convert2String(params.get("account_name"));
        if (!StringUtil.isEmpty(account_name)) {
            sql += " and account_name like #{account_name}";
            paramMap.put("account_name", "%" + account_name + "%");
        }

        List<SystemExampleDO> rows = systemExampleService.selectPageList("*", sql, "sort_num desc", paramMap);
        int total = systemExampleService.selectCount(sql,paramMap);
        result.add("rows", rows);
        result.add("total", total);
        return result;
    }

    @ResponseBody
    @PostMapping("/insert")
    @SubmitLock
    @Transactional(rollbackFor = {Exception.class}, isolation = Isolation.READ_UNCOMMITTED)
    public Object insert(@RequestBody SystemExampleDO entity) {
        Result result = Result.okResult();
        systemExampleService.insert(entity);
        return result;
    }

    @ResponseBody
    @PostMapping("/detail")
    public Object detail(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        String row_id = ConvertOp.convert2String(params.get("row_id"));
        result.add("obj", systemExampleService.selectOne(row_id));
        return result;
    }

    @ResponseBody
    @PostMapping("/update")
    @Transactional(rollbackFor = {Exception.class}, isolation = Isolation.READ_UNCOMMITTED)
    public Object update(@UpdateRequestBody SystemExampleDO entity) {
        Result result = Result.okResult();
        systemExampleService.update(entity);
        return result;
    }


    @ResponseBody
    @PostMapping("/delete")
    @Transactional(rollbackFor = {Exception.class}, isolation = Isolation.READ_UNCOMMITTED)
    public Object delete(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        String row_id = ConvertOp.convert2String(params.get("row_id"));
        systemExampleService.delete(row_id);
        return result;
    }

}
