package com.code2roc.fastboot.system.codegen.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.code2roc.fastboot.template.BaseBootModel;

import java.util.Date;

@TableName("system_table")
public class SystemTableDO extends BaseBootModel {
    @TableId(value = "row_id")
    private String row_id;
    @TableField(value = "table_name")
    private String table_name;
    @TableField(value = "table_description")
    private String table_description;
    @TableField(value = "package_name")
    private String package_name;
    @TableField(value = "route_path")
    private String route_path;
    @TableField(value = "remark")
    private String remark;
    @TableField(value = "sort_num")
    private Integer sort_num;
    @TableField(value = "gmt_create")
    private Date gmt_create;
    @TableField(value = "gmt_modified")
    private Date gmt_modified;

    public String getRow_id() {
        return row_id;
    }

    public void setRow_id(String row_id) {
        this.row_id = row_id;
    }

    public Integer getSort_num() {
        return sort_num;
    }

    public void setSort_num(Integer sort_num) {
        this.sort_num = sort_num;
    }

    public Date getGmt_create() {
        return gmt_create;
    }

    public void setGmt_create(Date gmt_create) {
        this.gmt_create = gmt_create;
    }

    public Date getGmt_modified() {
        return gmt_modified;
    }

    public void setGmt_modified(Date gmt_modified) {
        this.gmt_modified = gmt_modified;
    }

    public String getTable_name() {
        return table_name;
    }

    public void setTable_name(String table_name) {
        this.table_name = table_name;
    }

    public String getTable_description() {
        return table_description;
    }

    public void setTable_description(String table_description) {
        this.table_description = table_description;
    }

    public String getPackage_name() {
        return package_name;
    }

    public void setPackage_name(String package_name) {
        this.package_name = package_name;
    }

    public String getRoute_path() {
        return route_path;
    }

    public void setRoute_path(String route_path) {
        this.route_path = route_path;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
