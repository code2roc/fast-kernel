package com.code2roc.fastboot.system.enums;

import com.code2roc.fastboot.framework.datadic.IConvertEnumToCodeItem;

public class JobEnum {
    public enum JobType implements IConvertEnumToCodeItem {
        CycleJob(10, "循环任务"), CornJob(20, "表达式任务");
        private int _value;
        private String _name;

        private JobType(int value, String name) {
            set_value(value);
            set_name((name));
        }

        public int get_value() {
            return _value;
        }

        public void set_value(int _value) {
            this._value = _value;
        }

        public String get_name() {
            return _name;
        }

        public void set_name(String _name) {
            this._name = _name;
        }

        @Override
        public String toString() {
            return _name;
        }

        @Override
        public String getCodeName() {
            return "定时任务类别";
        }
    }

    public enum ExecuteUnit implements IConvertEnumToCodeItem {
        Second(10, "秒"), Minute(20, "分"), Hour(30, "时");
        private int _value;
        private String _name;

        private ExecuteUnit(int value, String name) {
            set_value(value);
            set_name((name));
        }

        public int get_value() {
            return _value;
        }

        public void set_value(int _value) {
            this._value = _value;
        }

        public String get_name() {
            return _name;
        }

        public void set_name(String _name) {
            this._name = _name;
        }

        @Override
        public String toString() {
            return _name;
        }

        @Override
        public String getCodeName() {
            return "定时任务间隔单位";
        }
    }

    public enum JobStatus implements IConvertEnumToCodeItem {
        Open(10, "开启"), Close(20, "关闭");
        private int _value;
        private String _name;

        private JobStatus(int value, String name) {
            set_value(value);
            set_name((name));
        }

        public int get_value() {
            return _value;
        }

        public void set_value(int _value) {
            this._value = _value;
        }

        public String get_name() {
            return _name;
        }

        public void set_name(String _name) {
            this._name = _name;
        }

        @Override
        public String toString() {
            return _name;
        }

        @Override
        public String getCodeName() {
            return "定时任务状态";
        }
    }
}
