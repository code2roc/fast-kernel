package com.code2roc.fastboot.system.org.controller;

import com.code2roc.fastboot.system.enums.CommonEnum;
import com.code2roc.fastboot.system.org.bizlogic.RoleLogic;
import com.code2roc.fastboot.system.org.model.SystemRoleDO;
import com.code2roc.fastboot.system.org.service.ISystemRoleService;
import com.code2roc.fastboot.system.setting.service.ISystemGroupService;
import com.code2roc.fastboot.template.BaseBootController;
import com.code2roc.fastboot.framework.formupdate.UpdateRequestBody;
import com.code2roc.fastboot.framework.model.Result;
import com.code2roc.fastboot.framework.util.ConvertOp;
import com.code2roc.fastboot.framework.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/system/org/role")
public class SystemRoleController extends BaseBootController {
    @Autowired
    private ISystemRoleService service;
    @Autowired
    private ISystemGroupService groupService;
    @Autowired
    private RoleLogic roleLogic;

    @ResponseBody
    @PostMapping("/list")
    public Object list(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        String sql = "1=1";
        HashMap<String, Object> paramMap = new HashMap<>();
        String role_name = ConvertOp.convert2String(params.get("role_name"));
        String group_id = ConvertOp.convert2String(params.get("group_id"));
        if (StringUtil.isEmpty(group_id)) {
            if (!StringUtil.isEmpty(role_name)) {
                sql += " and role_name like #{role_name}";
                paramMap.put("role_name", "%" + role_name + "%");
            }
        } else {
            sql += groupService.getGroupIDSearchSQL(group_id);
            if (!StringUtil.isEmpty(role_name)) {
                sql += " and role_name like #{role_name}";
                paramMap.put("role_name", "%" + role_name + "%");
            }
        }

        List<SystemRoleDO> rows = service.selectPageList("*", sql, "sort_num desc", paramMap);
        int total = service.selectCount(sql, paramMap);
        result.add("rows", rows);
        result.add("total", total);
        return result;
    }

    @ResponseBody
    @PostMapping("/getAllRoleList")
    public Object getAllRoleList(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        List<SystemRoleDO> obj = service.selectAllList();
        result.add("obj", obj);
        return result;
    }

    @ResponseBody
    @PostMapping("/insert")
    @Transactional(rollbackFor = {Exception.class},isolation = Isolation.READ_UNCOMMITTED)
    public Object insert(@RequestBody SystemRoleDO entity) {
        Result result = Result.okResult();
        if (service.checkExist("role_name", entity.getRole_name())) {
            return Result.errorResult().setMsg("角色名已存在");
        }
        roleLogic.insert(entity);
        return result;
    }

    @ResponseBody
    @PostMapping("/detail")
    public Object detail(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        String row_id = ConvertOp.convert2String(params.get("row_id"));
        result.add("obj", roleLogic.detail(row_id));
        return result;
    }

    @ResponseBody
    @PostMapping("/update")
    @Transactional(rollbackFor = {Exception.class},isolation = Isolation.READ_UNCOMMITTED)
    public Object update(@UpdateRequestBody SystemRoleDO entity) {
        Result result = Result.okResult();
        if (service.checkExist("role_name", entity.getRole_name(), entity.getRow_id())) {
            return Result.errorResult().setMsg("角色名已存在");
        }
        roleLogic.update(entity);
        return result;
    }

    @ResponseBody
    @PostMapping("/delete")
    @Transactional(rollbackFor = {Exception.class},isolation = Isolation.READ_UNCOMMITTED)
    public Object delete(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        String row_id = ConvertOp.convert2String(params.get("row_id"));
        SystemRoleDO entity = service.selectOne(row_id);
        if (entity.getSystem_init() == CommonEnum.YesOrNo.Yes.get_value()) {
            return Result.errorResult().setMsg("系统内置数据，禁止删除");
        }
        roleLogic.delete(row_id);
        return result;
    }

    @ResponseBody
    @PostMapping("/exportData")
    public Object exportData(@RequestBody Map<String, Object> params) throws Exception {
        Result result = Result.okResult();
        String fileid = roleLogic.exportData();
        result.add("fileid", fileid);
        return result;
    }

    @ResponseBody
    @PostMapping("/importData")
    public Object importData(@RequestParam("file") MultipartFile file, HttpServletRequest request) throws Exception {
        Result result = Result.okResult();
        String errormsg = roleLogic.importData(file.getInputStream());
        if (!StringUtil.isEmpty(errormsg)) {
            return Result.errorResult().setMsg(errormsg);
        }
        return result;
    }
}
