package com.code2roc.fastboot.system.cache;

import com.code2roc.fastboot.framework.datadic.CodeItem;
import com.code2roc.fastboot.framework.template.BaseSystemObject;

import java.util.List;

public class DataBaseDicCacheInfo extends BaseSystemObject {
    private String codeName;
    private List<CodeItem> codeItemList;

    public String getCodeName() {
        return codeName;
    }

    public void setCodeName(String codeName) {
        this.codeName = codeName;
    }

    public List<CodeItem> getCodeItemList() {
        return codeItemList;
    }

    public void setCodeItemList(List<CodeItem> codeItemList) {
        this.codeItemList = codeItemList;
    }
}
