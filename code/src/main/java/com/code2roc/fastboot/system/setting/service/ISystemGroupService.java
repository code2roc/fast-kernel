package com.code2roc.fastboot.system.setting.service;

import com.code2roc.fastboot.system.setting.model.SystemGroupDO;
import com.code2roc.fastboot.template.BaseBootService;

import java.util.List;

public interface ISystemGroupService  extends BaseBootService<SystemGroupDO> {
    boolean checkExistSub(String parent_id);

    List<String> getAllSubGroupIDList(String group_id);

    String getGroupIDSearchSQL(String group_id);
}
