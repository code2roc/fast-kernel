package com.code2roc.fastboot.system.common.controller;

import com.code2roc.fastboot.system.common.bizlogic.JobLogic;
import com.code2roc.fastboot.system.common.model.SystemJobDO;
import com.code2roc.fastboot.system.common.service.ISystemJobService;
import com.code2roc.fastboot.template.BaseBootController;
import com.code2roc.fastboot.framework.formupdate.UpdateRequestBody;
import com.code2roc.fastboot.framework.model.Result;
import com.code2roc.fastboot.framework.util.ConvertOp;
import com.code2roc.fastboot.framework.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/system/common/job")
public class SystemJobController extends BaseBootController {
    @Autowired
    private ISystemJobService service;
    @Autowired
    private JobLogic jobLogic;

    @ResponseBody
    @PostMapping("/list")
    public Object list(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        String sql = "1=1";
        HashMap<String, Object> paramMap = new HashMap<>();
        String job_name = ConvertOp.convert2String(params.get("job_name"));
        if (!StringUtil.isEmpty(job_name)) {
            sql += " and job_name like #{job_name}";
            paramMap.put("job_name", "%" + job_name + "%");
        }
        List<SystemJobDO> rows = service.selectPageList("*", sql, "sort_num desc", paramMap);
        int total = service.selectCount(sql, paramMap);
        result.add("rows", rows);
        result.add("total", total);
        return result;
    }

    @ResponseBody
    @PostMapping("/insert")
    @Transactional(rollbackFor = {Exception.class},isolation = Isolation.READ_UNCOMMITTED)
    public Object insert(@RequestBody SystemJobDO entity) {
        Result result = Result.okResult();
        service.insert(entity);
        return result;
    }

    @ResponseBody
    @PostMapping("/detail")
    public Object detail(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        String row_id = ConvertOp.convert2String(params.get("row_id"));
        result.add("obj", service.selectOne(row_id));
        return result;
    }

    @ResponseBody
    @PostMapping("/update")
    @Transactional(rollbackFor = {Exception.class},isolation = Isolation.READ_UNCOMMITTED)
    public Object update(@UpdateRequestBody SystemJobDO entity) {
        Result result = Result.okResult();
        service.update(entity);
        return result;
    }

    @ResponseBody
    @PostMapping("/delete")
    @Transactional(rollbackFor = {Exception.class},isolation = Isolation.READ_UNCOMMITTED)
    public Object delete(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        String row_id = ConvertOp.convert2String(params.get("row_id"));
        service.delete(row_id);
        return result;
    }

    @ResponseBody
    @PostMapping("/updateStatus")
    public Object updateStatus(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        String row_id = ConvertOp.convert2String(params.get("row_id"));
        int job_status = ConvertOp.convert2Int(params.get("job_status"));
        jobLogic.updateStatus(row_id, job_status);
        return result;
    }

    @ResponseBody
    @PostMapping("/executeOnce")
    public Object executeOnce(@RequestBody Map<String, Object> params) throws Exception {
        Result result = Result.okResult();
        String row_id = ConvertOp.convert2String(params.get("row_id"));
        jobLogic.executeOnce(row_id);
        return result;
    }
}
