package com.code2roc.fastboot.system.common.controller;

import com.code2roc.fastboot.template.BaseBootController;
import com.code2roc.fastboot.framework.model.Result;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/system/common/config")
public class ConfigController extends BaseBootController {
    @Value("${system.systemName}")
    private String systemName;
    @Value("${system.staticFileVersion}")
    private String staticFileVersion;
    @Value("${system.fileChunkSize}")
    private int fileChunkSize;


    @ResponseBody
    @PostMapping("/getConfig")
    public Object userLogin(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        HashMap config = new HashMap();
        config.put("systemName",systemName);
        config.put("staticFileVersion",staticFileVersion);
        config.put("fileChunkSize",fileChunkSize);
        result.add("obj",config);
        return result;
    }
}
