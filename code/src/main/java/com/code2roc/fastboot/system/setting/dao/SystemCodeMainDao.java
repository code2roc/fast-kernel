package com.code2roc.fastboot.system.setting.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.code2roc.fastboot.system.setting.model.SystemCodeMainDO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SystemCodeMainDao extends BaseMapper<SystemCodeMainDO> {
}
