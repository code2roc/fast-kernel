package com.code2roc.fastboot.system.org.service.impl;

import com.code2roc.fastboot.system.org.model.SystemRoleSettingDO;
import com.code2roc.fastboot.system.org.service.ISystemRoleSettingService;
import com.code2roc.fastboot.template.BaseBootServiceImpl;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class SystemRoleSettingServiceImpl extends BaseBootServiceImpl<SystemRoleSettingDO> implements ISystemRoleSettingService {
    @Override
    public void delete(String user_id, String role_id) {
        String sql = "delete from system_role_setting where user_id = #{user_id} and role_id = #{role_id}";
        HashMap paramMap = new HashMap();
        paramMap.put("user_id", user_id);
        paramMap.put("role_id", role_id);
        commonDTO.executeSQL(sql, paramMap);
    }

    @Override
    public void deleteByRoleID(String role_id) {
        String sql = "delete from system_role_setting where role_id = #{role_id}";
        HashMap paramMap = new HashMap();
        paramMap.put("role_id", role_id);
        commonDTO.executeSQL(sql, paramMap);
    }

    @Override
    public void deleteByUserID(String user_id) {
        String sql = "delete from system_role_setting where user_id = #{user_id}";
        HashMap paramMap = new HashMap();
        paramMap.put("user_id", user_id);
        commonDTO.executeSQL(sql, paramMap);
    }
}
