package com.code2roc.fastboot.system.codegen.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.code2roc.fastboot.template.BaseBootModel;

import java.util.Date;

@TableName("system_column")
public class SystemColumnDO extends BaseBootModel {
    @TableId(value = "row_id")
    private String row_id;
    @TableField(value = "table_id")
    private String table_id;
    @TableField(value = "column_name")
    private String column_name;
    @TableField(value = "column_description")
    private String column_description;
    @TableField(value = "column_type")
    private String column_type;
    @TableField(value = "sort_num")
    private Integer sort_num;
    @TableField(value = "gmt_create")
    private Date gmt_create;
    @TableField(value = "gmt_modified")
    private Date gmt_modified;

    public String getRow_id() {
        return row_id;
    }

    public void setRow_id(String row_id) {
        this.row_id = row_id;
    }

    public Integer getSort_num() {
        return sort_num;
    }

    public void setSort_num(Integer sort_num) {
        this.sort_num = sort_num;
    }

    public Date getGmt_create() {
        return gmt_create;
    }

    public void setGmt_create(Date gmt_create) {
        this.gmt_create = gmt_create;
    }

    public Date getGmt_modified() {
        return gmt_modified;
    }

    public void setGmt_modified(Date gmt_modified) {
        this.gmt_modified = gmt_modified;
    }

    public String getColumn_name() {
        return column_name;
    }

    public void setColumn_name(String column_name) {
        this.column_name = column_name;
    }

    public String getColumn_description() {
        return column_description;
    }

    public void setColumn_description(String column_description) {
        this.column_description = column_description;
    }

    public String getColumn_type() {
        return column_type;
    }

    public void setColumn_type(String column_type) {
        this.column_type = column_type;
    }

    public String getTable_id() {
        return table_id;
    }

    public void setTable_id(String table_id) {
        this.table_id = table_id;
    }

}
