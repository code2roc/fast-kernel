package com.code2roc.fastboot.system.setting.service;

import com.code2roc.fastboot.system.setting.model.SystemCodeItemDO;
import com.code2roc.fastboot.template.BaseBootService;

public interface ISystemCodeItemService  extends BaseBootService<SystemCodeItemDO> {
}
