package com.code2roc.fastboot.system.codegen.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.code2roc.fastboot.system.codegen.model.SystemTableDO;
import com.code2roc.fastboot.system.setting.model.SystemCodeItemDO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SystemTableDao extends BaseMapper<SystemTableDO> {
}
