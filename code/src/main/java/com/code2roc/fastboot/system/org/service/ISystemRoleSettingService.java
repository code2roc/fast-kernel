package com.code2roc.fastboot.system.org.service;

import com.code2roc.fastboot.system.org.model.SystemRoleSettingDO;
import com.code2roc.fastboot.template.BaseBootService;

public interface ISystemRoleSettingService extends BaseBootService<SystemRoleSettingDO> {
    void delete(String user_id,String role_id);
    void deleteByRoleID(String role_id);
    void deleteByUserID(String user_id);
}
