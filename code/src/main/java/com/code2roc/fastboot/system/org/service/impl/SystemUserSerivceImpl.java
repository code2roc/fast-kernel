package com.code2roc.fastboot.system.org.service.impl;

import com.code2roc.fastboot.system.org.model.SystemUserDO;
import com.code2roc.fastboot.system.org.service.ISystemUserService;
import com.code2roc.fastboot.template.BaseBootServiceImpl;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class SystemUserSerivceImpl extends BaseBootServiceImpl<SystemUserDO> implements ISystemUserService {
    @Override
    public boolean checkExistUserByeDeptID(String dept_id) {
        String sql = "dept_id = #{dept_id}";
        HashMap paramMap = new HashMap();
        paramMap.put("dept_id", dept_id);
        return selectCount(sql, paramMap) > 0;
    }
}
