package com.code2roc.fastboot.system.codegen.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.code2roc.fastboot.system.codegen.model.SystemTableDO;
import com.code2roc.fastboot.system.codegen.service.ISystemTableService;
import com.code2roc.fastboot.template.BaseBootServiceImpl;
import com.code2roc.fastboot.framework.util.CommonUtil;
import com.code2roc.fastboot.framework.util.StringUtil;
import org.springframework.stereotype.Service;

@Service
public class SystemTableServiceImpl extends BaseBootServiceImpl<SystemTableDO> implements ISystemTableService {
    @Override
    public void insert(SystemTableDO entity) {
        if (StringUtil.isEmpty(entity.getRow_id())) {
            entity.setRow_id(CommonUtil.getNewGuid());
        }
       getMapper().insert(entity);
    }

    @Override
    public void update(SystemTableDO entity) {
        UpdateWrapper wrapper = this.commonWrapper.getUpdateWrapperFillKey(entity);
        getMapper().update(entity,wrapper);
    }
}
