package com.code2roc.fastboot.system.setting.service;

import com.code2roc.fastboot.system.setting.model.SystemMenuSettingDO;
import com.code2roc.fastboot.template.BaseBootService;

public interface ISystemMenuSettingService extends BaseBootService<SystemMenuSettingDO> {
    void deleteSetting(String menu_id,String setting_id);
    void deleteSetting(String menu_id);
    void deleteSettingBySettingID(String setting_id);
}
