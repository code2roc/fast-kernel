package com.code2roc.fastboot.system.setting.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.code2roc.fastboot.template.BaseBootModel;

import java.util.Date;

@TableName("system_group")
public class SystemGroupDO extends BaseBootModel {
    @TableId(value = "row_id")
    private String row_id;
    @TableField(value = "sort_num")
    private Integer sort_num;
    @TableField(value = "gmt_create")
    private Date gmt_create;
    @TableField(value = "gmt_modified")
    private Date gmt_modified;
    @TableField("group_name")
    private String group_name;
    @TableField("group_tag")
    private String group_tag;
    @TableField("parent_id")
    private String parent_id;
    @TableField("system_init")
    private Integer system_init;

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }

    public String getGroup_tag() {
        return group_tag;
    }

    public void setGroup_tag(String group_tag) {
        this.group_tag = group_tag;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public Integer getSystem_init() {
        return system_init;
    }

    public void setSystem_init(Integer system_init) {
        this.system_init = system_init;
    }

    @Override
    public String getRow_id() {
        return row_id;
    }

    @Override
    public void setRow_id(String row_id) {
        this.row_id = row_id;
    }

    @Override
    public Integer getSort_num() {
        return sort_num;
    }

    @Override
    public void setSort_num(Integer sort_num) {
        this.sort_num = sort_num;
    }

    @Override
    public Date getGmt_create() {
        return gmt_create;
    }

    @Override
    public void setGmt_create(Date gmt_create) {
        this.gmt_create = gmt_create;
    }

    @Override
    public Date getGmt_modified() {
        return gmt_modified;
    }

    @Override
    public void setGmt_modified(Date gmt_modified) {
        this.gmt_modified = gmt_modified;
    }
}
