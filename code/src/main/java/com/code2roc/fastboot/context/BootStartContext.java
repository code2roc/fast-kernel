package com.code2roc.fastboot.context;

import com.code2roc.fastboot.system.common.bizlogic.JobLogic;
import com.code2roc.fastboot.listener.FileListenerFactory;
import com.code2roc.fastboot.framework.systemload.ISystemStartExecute;
import com.code2roc.fastboot.util.FrameJarUtil;
import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;

@Service
public class BootStartContext implements ISystemStartExecute {
    @Value("${spring.profiles.active}")
    private String active;
    @Autowired
    private FileListenerFactory fileListenerFactory;
    @Autowired
    private JobLogic jobLogic;

    @Override
    public void execute() {
        //开启定时任务
        jobLogic.initQuartzTask();
        //拷贝框架静态页面
        FrameJarUtil.copyFrameStaticFile("com.code2roc.fastboot", true);
        //开发模式启用
        if (active.equals("dev")) {
            // 创建监听者
            String monitorDir = "src/main/resources/public";
            File file = new File(monitorDir);
            if (!file.exists()) {
                monitorDir = "src/main/resources/static";
            }
            FileAlterationMonitor fileAlterationMonitor = fileListenerFactory.getStaticResourceMonitor(monitorDir);
            try {
                fileAlterationMonitor.start();
                System.out.println("启动开发模式静态文件拷贝成功");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
