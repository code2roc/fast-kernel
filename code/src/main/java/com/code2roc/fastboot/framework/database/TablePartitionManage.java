package com.code2roc.fastboot.framework.database;

import com.code2roc.fastboot.framework.util.ConvertOp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class TablePartitionManage {
    @Autowired
    private CommonDTO commonDTO;

    public void addPartition(String tableName, String rangeFieldName, int partType) {
        String sql = "ALTER TABLE `" + tableName + "`  PARTITION BY RANGE COLUMNS (" + rangeFieldName + ")";
        List<String> groupList = new ArrayList<>();
        sql += "(";
        if (partType == 10) {
            //按年分区
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMM");
            //查询前6位数量
            List<LinkedHashMap<String, Object>> result = commonDTO.selectList(tableName, "DISTINCT(left(" + rangeFieldName + ",6)) as GroupName", "1=1", "", null);
            for (LinkedHashMap<String, Object> item : result) {
                groupList.add(ConvertOp.convert2String(item.get("GroupName")));
            }
            if (groupList.size() > 0) {
                Collections.sort(groupList);
                //firt区间删除
                groupList.remove(0);
                //last区间往后推一个
                try {
                    Calendar calendar = Calendar.getInstance();
                    Date last = simpleDateFormat.parse(groupList.get(groupList.size() - 1));
                    calendar.setTime(last);
                    calendar.add(Calendar.MONTH, 1);
                    String afterPart = simpleDateFormat.format(calendar.getTime());
                    groupList.add(afterPart);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } else if (partType == 20) {
            //按月分区
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy");
            //查询前4位数量
            List<LinkedHashMap<String, Object>> result = commonDTO.selectList(tableName, "DISTINCT(left(" + rangeFieldName + ",4)) as GroupName", "1=1", "", null);
            for (LinkedHashMap<String, Object> item : result) {
                groupList.add(ConvertOp.convert2String(item.get("GroupName")));
            }
            if (groupList.size() > 0) {
                Collections.sort(groupList);
                //firt区间删除
                groupList.remove(0);
                //last区间往后推一个
                try {
                    Calendar calendar = Calendar.getInstance();
                    Date last = simpleDateFormat.parse(groupList.get(groupList.size() - 1));
                    calendar.setTime(last);
                    calendar.add(Calendar.YEAR, 1);
                    String afterPart = simpleDateFormat.format(calendar.getTime());
                    groupList.add(afterPart);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }
        for (int i = 0; i < groupList.size(); i++) {
            String key = groupList.get(i);
            sql += "PARTITION  P" + key + "  VALUES LESS THAN  ('" + key + "')";
            if (i != groupList.size() - 1) {
                sql += ",";
            }
        }
        sql += ");";
        commonDTO.executeSQL(sql, null);
    }

    public void deletePartition(String tableName) {
        String sql = "ALTER TABLE " + tableName + " REMOVE PARTITIONING";
        commonDTO.executeSQL(sql, null);
    }

    public List<PartitionItemDetail> getDetail(String tableName) {
        String columns = "PARTITION_NAME as PartitionName,TABLE_ROWS as TableRows";
        String sql = "TABLE_SCHEMA=SCHEMA() AND TABLE_NAME='" + tableName + "'";
        return commonDTO.selectList(PartitionItemDetail.class, "information_schema.PARTITIONS", columns, sql, "",null);
    }

    public void checkPartition(String tableName, String fieldValue,int partType) {
        String groupName = "";
        if (partType==10) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy");
                String dataTag = fieldValue.substring(0, 4);
                Calendar calendar = Calendar.getInstance();
                Date last = simpleDateFormat.parse(dataTag);
                calendar.setTime(last);
                calendar.add(Calendar.YEAR, 1);
                groupName = "P" + simpleDateFormat.format(calendar.getTime());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (partType==20) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMM");
                String dataTag = fieldValue.substring(0, 6);
                Calendar calendar = Calendar.getInstance();
                Date last = simpleDateFormat.parse(dataTag);
                calendar.setTime(last);
                calendar.add(Calendar.MONTH, 1);
                groupName = "p" + simpleDateFormat.format(calendar.getTime());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //添加新的分区
        String sql = "ALTER TABLE `" + tableName + "` ADD PARTITION ( PARTITION " + groupName + " VALUES LESS THAN ('" + groupName.replace("P", "") + "') );";
        commonDTO.executeSQL(sql, null);
    }
}
