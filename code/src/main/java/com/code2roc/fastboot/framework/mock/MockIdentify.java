package com.code2roc.fastboot.framework.mock;

import com.code2roc.fastboot.framework.auth.AnonymousAccess;

import java.lang.annotation.*;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@AnonymousAccess
public @interface MockIdentify {
    String loginName() default "sysadmin";
}
