package com.code2roc.fastboot.framework.socket;

import com.code2roc.fastboot.framework.auth.TokenModel;
import com.code2roc.fastboot.framework.auth.TokenUtil;
import com.code2roc.fastboot.framework.model.Result;
import com.code2roc.fastboot.framework.util.BeanUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class SSESocketManage {
    private static Map<String, SseEmitter> sseCache = new ConcurrentHashMap<>();
    private static TokenUtil tokenUtil = BeanUtil.getBean(TokenUtil.class);
    private static Logger logger = LoggerFactory.getLogger(SSESocketManage.class);

    //打开连接
    public static SseEmitter createConnect(String token, String clientID) {
        TokenModel tokenModel = tokenUtil.getTokenModel();
        String clientId = token + "|" + clientID + "|" + tokenModel.getUserID();
        // 设置超时时间，0表示不过期。默认30秒，超过时间未完成会抛出异常：AsyncRequestTimeoutException
        SseEmitter sseEmitter = new SseEmitter(0L);
        sseCache.put(clientId, sseEmitter);
        try {
            sseEmitter.send(SseEmitter.event().id("Connect").data(Result.okResult().setMsg("sse连接成功")));
            logger.debug("sse连接成功，客户端：" + clientId);
        } catch (IOException e) {
            removeUser(clientId);
            e.printStackTrace();
        }
        return sseEmitter;
    }

    //关闭连接
    public static void closeConnect(String token, String clientID) {
        TokenModel tokenModel = tokenUtil.getTokenModel();
        String clientId = token + "|" + clientID + "|" + tokenModel.getUserID();
        SseEmitter sseEmitter = sseCache.get(clientId);
        if (sseEmitter != null) {
            logger.debug("sse关闭连接，客户端：" + clientId);
            removeUser(clientId);
        }
    }

    //发送消息给所有连接客户端
    public static void sendMessage(Result result) {
        checkConnection();
        for (String clientId : sseCache.keySet()) {
            SseEmitter sseEmitter = sseCache.get(clientId);
            SseEmitter.SseEventBuilder sendData = SseEmitter.event().id("Notice").data(result, MediaType.APPLICATION_JSON);
            try {
                sseEmitter.send(sendData);
            } catch (Exception e) {
                removeUser(clientId);
                e.printStackTrace();
            }
        }

    }

    //发送消息给客户端
    public static void sendMessage(String clientId, Result result) {
        checkConnection();
        try {
            if (sseCache.containsKey(clientId)) {
                SseEmitter sseEmitter = sseCache.get(clientId);
                SseEmitter.SseEventBuilder sendData = SseEmitter.event().id("Notice").data(result, MediaType.APPLICATION_JSON);
                sseEmitter.send(sendData);
            }
        } catch (Exception e) {
            removeUser(clientId);
            e.printStackTrace();
        }
    }

    //发送消息给用户
    public static void sendMessageToUser(String uguid, Result result) {
        checkConnection();
        for (String clientId : sseCache.keySet()) {
            if (clientId.endsWith(uguid)) {
                String token = clientId.split("\\|")[0];
                if (tokenUtil.checkTokenValid(token)) {
                    TokenModel tokenModel = tokenUtil.getTokenModel(token);
                    if (tokenModel.getUserID().equals(uguid)) {
                        SseEmitter sseEmitter = sseCache.get(clientId);
                        SseEmitter.SseEventBuilder sendData = SseEmitter.event().id("Notice").data(result, MediaType.APPLICATION_JSON);
                        try {
                            sseEmitter.send(sendData);
                        } catch (Exception e) {
                            removeUser(clientId);
                            e.printStackTrace();
                        }

                    }
                } else {
                    if (sseCache.containsKey(clientId)) {
                        removeUser(clientId);
                    }
                }
            }
        }

    }


    public static void checkConnection() {
        for (String clientId : sseCache.keySet()) {
            SseEmitter sseEmitter = sseCache.get(clientId);
            SseEmitter.SseEventBuilder sendData = SseEmitter.event().id("Connect").data(new Result().setMsg("heartbeat"), MediaType.APPLICATION_JSON);
            try {
                sseEmitter.send(sendData);
            } catch (Exception e) {
                removeUser(clientId);
            }
        }
        logger.debug("sse总连接数量：" + sseCache.keySet().size());
    }


    public static boolean checkConnectExist(String token){
        boolean flag = false;
        for (String socketIDKey : sseCache.keySet()) {
            if (socketIDKey.startsWith(token)) {
                flag = true;
                break;
            }
        }
        return flag;
    }

    public static void deleteConnect(String token){
        for (String socketIDKey : sseCache.keySet()) {
            if (socketIDKey.startsWith(token)) {
                sseCache.remove(socketIDKey);
                break;
            }
        }
    }

    private static void removeUser(String clientId) {
        if (sseCache.containsKey(clientId)) {
            sseCache.remove(clientId);
        }
        logger.debug("sse移除客户端：" + clientId);
    }

}
