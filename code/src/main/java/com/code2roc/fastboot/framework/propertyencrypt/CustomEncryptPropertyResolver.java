package com.code2roc.fastboot.framework.propertyencrypt;

import com.code2roc.fastboot.framework.util.DESEncryptUtil;
import com.ulisesbocchio.jasyptspringboot.EncryptablePropertyResolver;
import org.springframework.stereotype.Component;

@Component
public class CustomEncryptPropertyResolver  implements EncryptablePropertyResolver {
    private static final String ENC_PREFIX="KENC#";

    @Override
    public String resolvePropertyValue(String value) {
        if (value!=null && value.startsWith(ENC_PREFIX)){
            try{
                String str=value.substring(0, value.indexOf("#"));
                String result=value.substring(str.length()+1);
                return DESEncryptUtil.decrypt(result);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return value;
    }
}
