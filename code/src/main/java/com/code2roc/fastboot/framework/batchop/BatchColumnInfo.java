package com.code2roc.fastboot.framework.batchop;

public class BatchColumnInfo {
    private int sqlType;
    private Object value;

    public int getSqlType() {
        return sqlType;
    }

    public void setSqlType(int sqlType) {
        this.sqlType = sqlType;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}
