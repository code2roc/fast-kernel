package com.code2roc.fastboot.framework.file;

import java.io.InputStream;

public interface IFileOperate {

    void storageFile(String bucketName, String key, byte[] bytes) throws Exception;

    void storageFile(String bucketName, String key, InputStream inputStream) throws Exception;

    void storageFileByChunk(String bucketName, String key, InputStream inputStream, String fileMD5, int chunkCount, int chunkIndex, int chunkSize) throws Exception;

    byte[] getFileBytes(String bucketName, String key) throws Exception;

    void copyFile(String originBucketName, String originFileKey, String newBucketName, String newFileKey) throws Exception;

    String getFileUrl(String bucketName, String key) throws Exception;

    void deleteFile(String bucketName, String key) throws Exception;

    FileStoargeProperty getFileStoargeProperty();
}
