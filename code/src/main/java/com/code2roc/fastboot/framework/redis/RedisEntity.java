package com.code2roc.fastboot.framework.redis;

import java.time.Duration;

public class RedisEntity {
    private String host;
    private Integer port;
    private String password;
    private Duration timeout;
    private LettuceEnity lettuce;

    public RedisEntity(){
        timeout = Duration.ofSeconds(60);
        lettuce = new LettuceEnity();
    }


    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public Duration getTimeout() {
        return timeout;
    }

    public void setTimeout(Duration timeout) {
        this.timeout = timeout;
    }

    public LettuceEnity getLettuce() {
        return lettuce;
    }

    public void setLettuce(LettuceEnity lettuce) {
        this.lettuce = lettuce;
    }
}
