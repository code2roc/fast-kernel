package com.code2roc.fastboot.framework.datawrapper;

import java.lang.annotation.*;

@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface Wrapper {
    Class<? extends BaseWrapper<?>>[] value();
}
