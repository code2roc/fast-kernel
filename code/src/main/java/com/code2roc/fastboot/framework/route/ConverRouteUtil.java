package com.code2roc.fastboot.framework.route;

import com.code2roc.fastboot.FastBootApplication;
import com.code2roc.fastboot.framework.cache.CacheTable;
import com.code2roc.fastboot.framework.util.ScanUtil;
import com.code2roc.fastboot.framework.util.StringUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class ConverRouteUtil {
    private static HashMap<String, String> mappingRegist = new HashMap<>();

    public static void initRoute(Class runtimeClass) {
        List<Class<?>> scanClassList = new ArrayList<>();
        scanClassList.addAll(ScanUtil.getAllClassByPackageName_Annotation(runtimeClass.getPackage(), CoverRoute.class));
        if(runtimeClass.getPackage().getName()!= FastBootApplication.class.getPackage().getName()){
            scanClassList.addAll(ScanUtil.getAllClassByPackageName_Annotation(FastBootApplication.class.getPackage(), CacheTable.class));
        }

        for (Class clazz : scanClassList) {
            CoverRoute coverRoute = (CoverRoute) clazz.getAnnotation(CoverRoute.class);
            if (StringUtil.isEmpty(coverRoute.value())) {
                continue;
            }
            RequestMapping requestMapping = (RequestMapping) clazz.getAnnotation(RequestMapping.class);
            String classRoute = "";
            if (requestMapping != null) {
                classRoute = requestMapping.value()[0];
            } else {
                continue;
            }
            List<Method> methodList = Arrays.asList(clazz.getDeclaredMethods());
            for (Method method : methodList) {
                PostMapping postMapping = method.getAnnotation(PostMapping.class);
                String methodRoute = "";
                if (postMapping != null) {
                    methodRoute = postMapping.value()[0];
                } else {
                    GetMapping getMapping = method.getAnnotation(GetMapping.class);
                    if (getMapping != null) {
                        methodRoute = getMapping.value()[0];
                    }
                }
                if (!StringUtil.isEmpty(classRoute) && !StringUtil.isEmpty(methodRoute)) {
                    String orginalRoute = coverRoute.value() + methodRoute;
                    String redirectRoute = classRoute + methodRoute;
                    mappingRegist.put(orginalRoute, redirectRoute);
                }
            }
        }
        if (mappingRegist.size() > 0) {
            System.out.println("扫描路由方法覆盖：" + mappingRegist.size() + "个");
        }
    }

    public static boolean checkExistCover(String orginalRoute) {
        return mappingRegist.containsKey(orginalRoute);
    }

    public static String getRedirectRoute(String orginalRoute) {
        return mappingRegist.get(orginalRoute);
    }
}
