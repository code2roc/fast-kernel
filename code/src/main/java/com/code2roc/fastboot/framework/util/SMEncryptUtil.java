package com.code2roc.fastboot.framework.util;

import com.antherd.smcrypto.sm2.Keypair;
import com.antherd.smcrypto.sm2.Sm2;
import com.antherd.smcrypto.sm3.Sm3;
import com.antherd.smcrypto.sm4.Sm4;
import com.antherd.smcrypto.sm4.Sm4Options;

import java.util.HashMap;
import java.util.Map;

public class SMEncryptUtil {
    public static Map<String, String> createKeys() {
        Keypair keypair = Sm2.generateKeyPairHex();
        Map<String, String> keyPairMap = new HashMap<String, String>();
        keyPairMap.put("publicKey", keypair.getPublicKey());
        keyPairMap.put("privateKey", keypair.getPrivateKey());
        // 返回map
        return keyPairMap;
    }


    /**
     * 加密方法（Sm2 的专门针对前后端分离，非对称秘钥对的方式，暴露出去的公钥，对传输过程中的密码加个密）
     *
     * @param str 待加密数据
     * @return 加密后的密文
     * @author yubaoshan
     * @date 2022/9/15 21:51
     */
    public static String doSm2Encrypt(String str,String publicKey) {
        return Sm2.doEncrypt(str, publicKey);
    }

    /**
     * 解密方法
     * 如果采用加密机的方法，用try catch 捕捉异常，返回原文值即可
     *
     * @param str 密文
     * @return 解密后的明文
     * @author yubaoshan
     * @date 2022/9/15 21:51
     */
    public static String doSm2Decrypt(String str,String privateKey) {
        // 解密
        return Sm2.doDecrypt(str, privateKey);
    }

    /**
     * 加密方法
     *
     * @param str 待加密数据
     * @return 加密后的密文
     * @author yubaoshan
     * @date 2022/9/15 21:51
     */
    public static String doSm4CbcEncrypt(String str,String key) {
        // SM4 加密  cbc模式
        Sm4Options sm4Options4 = new Sm4Options();
        sm4Options4.setMode("cbc");
        sm4Options4.setIv("fedcba98765432100123456789abcdef");
        return Sm4.encrypt(str, key, sm4Options4);
    }

    /**
     * 解密方法
     * 如果采用加密机的方法，用try catch 捕捉异常，返回原文值即可
     *
     * @param str 密文
     * @return 解密后的明文
     * @author yubaoshan
     * @date 2022/9/15 21:51
     */
    public static String doSm4CbcDecrypt(String str,String key) {
        // 解密，cbc 模式，输出 utf8 字符串
        Sm4Options sm4Options8 = new Sm4Options();
        sm4Options8.setMode("cbc");
        sm4Options8.setIv("fedcba98765432100123456789abcdef");
        String docString = Sm4.decrypt(str, key, sm4Options8);
        if (docString.equals("")) {
            System.out.println(">>> 字段解密失败，返回原文值：" + str);
            return str;
        } else {
            return docString;
        }
    }

    /**
     * 纯签名
     *
     * @param str 待签名数据
     * @return 签名结果
     * @author yubaoshan
     * @date 2022/9/15 21:51
     */
    public static String doSignature(String str,String privateKey) {
        return Sm2.doSignature(str, privateKey);
    }

    /**
     * 验证签名结果
     *
     * @param originalStr 签名原文数据
     * @param str         签名结果
     * @return 是否通过
     * @author yubaoshan
     * @date 2022/9/15 21:51
     */
    public static boolean doVerifySignature(String originalStr, String str,String publicKey) {
        return Sm2.doVerifySignature(originalStr, str, publicKey);
    }

    /**
     * 通过杂凑算法取得hash值，用于做数据完整性保护
     *
     * @param str 字符串
     * @return hash 值
     * @author yubaoshan
     * @date 2022/9/15 21:51
     */
    public static String doHashValue(String str) {
        return Sm3.sm3(str);
    }
}
