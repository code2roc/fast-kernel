package com.code2roc.fastboot.framework.datawrapper;

public interface BaseSensitive {
    String doSensitive(String beSensitiveValue);
}
