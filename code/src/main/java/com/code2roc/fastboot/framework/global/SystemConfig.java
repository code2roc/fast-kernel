package com.code2roc.fastboot.framework.global;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "system")
public class SystemConfig {
    private String projectName;
    private String templatePath;
    private String logFilePath;
    private boolean globalTransition;
    private String messageChannel;
    private Integer maxFileSize;

    private String staticLocations;

    public SystemConfig(){
        projectName = "fastboot";
        templatePath = "tempfiles/";
        logFilePath = "logfiles/";
        globalTransition = false;
        messageChannel = "redis";
        staticLocations = "classpath:static/";
        maxFileSize = 50;
    }

    public boolean isGlobalTransition() {
        return globalTransition;
    }

    public void setGlobalTransition(boolean globalTransition) {
        this.globalTransition = globalTransition;
    }

    public String getMessageChannel() {
        return messageChannel;
    }

    public void setMessageChannel(String messageChannel) {
        this.messageChannel = messageChannel;
    }

    public String getTemplatePath() {
        return templatePath;
    }

    public void setTemplatePath(String templatePath) {
        this.templatePath = templatePath;
    }

    public String getLogFilePath() {
        return logFilePath;
    }

    public void setLogFilePath(String logFilePath) {
        this.logFilePath = logFilePath;
    }

    public String getStaticLocations() {
        return staticLocations;
    }

    public void setStaticLocations(String staticLocations) {
        this.staticLocations = staticLocations;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public Integer getMaxFileSize() {
        return maxFileSize;
    }

    public void setMaxFileSize(Integer maxFileSize) {
        this.maxFileSize = maxFileSize;
    }
}
