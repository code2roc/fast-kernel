package com.code2roc.fastboot.framework.template;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.TableField;

import java.util.List;

public class BaseModel extends BaseSystemObject {
    @TableField(exist = false)
    @JSONField(serialize = false)
    private List<String> updateFieldList;

    public List<String> getUpdateFieldList() {
        return updateFieldList;
    }

    public void setUpdateFieldList(List<String> updateFieldList) {
        this.updateFieldList = updateFieldList;
    }
}
