package com.code2roc.fastboot.framework.template;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.code2roc.fastboot.framework.cache.CacheUtil;
import com.code2roc.fastboot.framework.database.CommonDTO;
import com.code2roc.fastboot.framework.database.CommonWrapper;
import com.code2roc.fastboot.framework.database.MapperBeanManage;
import com.code2roc.fastboot.framework.datasource.TargetDataSource;
import com.code2roc.fastboot.framework.util.ReflectUtil;
import com.code2roc.fastboot.framework.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@Component
@TargetDataSource
public class BaseCacheServiceImpl<T extends BaseModel> implements BaseCacheService<T> {
    @Autowired
    public CommonDTO commonDTO;
    @Autowired
    public CommonWrapper commonWrapper;
    public String tableName;
    public String tableKeyName;
    public Class entityClass;
    public BaseMapper mapperBean;
    @Autowired
    public MapperBeanManage mapperBeanManage;
    @Autowired
    public CacheUtil cacheUtil;

    public BaseCacheServiceImpl() {
        entityClass = getEntityClass();
        tableName = getTableName(entityClass);
        tableKeyName = getTableKeyName(entityClass);
    }

    public void insert(T entity) {
        getMapper().insert(entity);
        cacheUtil.insert(entity);
    }

    public void delete(String id) {
        UpdateWrapper wrapper = commonWrapper.getDeleteWrapperFillKey(entityClass, id);
        getMapper().delete(wrapper);
        cacheUtil.delete(entityClass, id);
    }

    public void batchDelte(List<String> idList) {
        for (String id : idList) {
            delete(id);
        }
    }

    public void update(T entity) {
        UpdateWrapper wrapper = commonWrapper.getUpdateWrapperFillKey(entity);
        getMapper().update(entity, wrapper);
        cacheUtil.update(entity);
    }

    public void batchUpdate(List<T> entityList) {
        for (T entity : entityList) {
            update(entity);
        }
    }

    public T selectOne(String unitguid) {
        return (T) cacheUtil.selectOne(entityClass, unitguid);
    }

    public List<T> selectAllList() {
        return cacheUtil.selectAllList(entityClass);
    }

    public void clear() {
        String sql = "delete from " + tableName;
        commonDTO.executeSQL(sql, new HashMap<>());
        cacheUtil.clear(entityClass);
    }


    //获取泛型具体对应class
    public Class getEntityClass() {
        return ReflectUtil.getSuperClassGenricType(this.getClass());
    }

    //获取表名
    public String getTableName(Class<T> clazz) {
        String tabaleName = "";
        TableName tableNameAnnotaion = (TableName) clazz.getAnnotation(TableName.class);
        if (tableNameAnnotaion != null) {
            tabaleName = tableNameAnnotaion.value();
        }
        if ("".equals(tabaleName)) {
            tabaleName = clazz.getSimpleName();
        }
        return tabaleName;
    }

    //获取主键名称
    public String getTableKeyName(Class<T> clazz) {
        String tableKeyName = "";
        List<Field> fs = Arrays.asList(clazz.getDeclaredFields());
        for (Field field : fs) {
            field.setAccessible(true);
            //TableId修饰主键
            TableId tableIDAnnotaion = field.getAnnotation(TableId.class);
            if (tableIDAnnotaion != null) {
                tableKeyName = tableIDAnnotaion.value();
                break;
            }
        }
        if ("".equals(tableKeyName)) {
            tableKeyName = "id";
        }
        return tableKeyName;
    }

    //获取Dao对应的bean
    public BaseMapper getMapper() {
        if (null == mapperBean) {
            String tableNameValue = StringUtil.replaceLast(entityClass.getSimpleName().toLowerCase(),"do","");
            mapperBean = mapperBeanManage.getMapper(tableNameValue);
        }
        return mapperBean;
    }
}
