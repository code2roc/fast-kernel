package com.code2roc.fastboot.framework.datadic;

import com.code2roc.fastboot.FastBootApplication;
import com.code2roc.fastboot.framework.util.ConvertOp;
import com.code2roc.fastboot.framework.util.ScanUtil;
import com.code2roc.fastboot.framework.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@Component
public class DataDicManage {
    private static HashMap<String, List<CodeItem>> codeDic = new HashMap<>();
    private static List<CodeMain> codeMainList = new ArrayList<>();
    @Autowired
    private Environment env;
    @Autowired
    private ApplicationContext applicationContext;

    public DataDicManage(){
    }

    public void initCodeItem(Class runtimeClass) {
        //扫描枚举代码项
        //枚举转换为codeitem代码项对象
        List<Class> filterClassList = new ArrayList<>();
        filterClassList.add(IConvertEnumToCodeItem.class);
        //扫描运行springbootclass所在package
        List<Class<?>> scanClassList = ScanUtil.getAllClassByPackageName(runtimeClass.getPackage(), filterClassList);

        if(runtimeClass.getPackage().getName()!= FastBootApplication.class.getPackage().getName()){
            scanClassList.addAll(ScanUtil.getAllClassByPackageName(FastBootApplication.class.getPackage(), filterClassList));
        }

        try {
            for (Class clazz : scanClassList) {
                if (clazz.isEnum()) {
                    List<CodeItem> codeItemList = new ArrayList<>();
                    Object[] oo = clazz.getEnumConstants();
                    for (Object obj : oo) {
                        String itemText = "";
                        Object itemValue = null;
                        List<Field> fs = Arrays.asList(clazz.getDeclaredFields());
                        for (Field field : fs) {
                            field.setAccessible(true);
                            if (field.getName().equals("_name")) {
                                itemText = ConvertOp.convert2String(field.get(obj));
                            } else if (field.getName().equals("_value")) {
                                itemValue = field.get(obj);
                            }
                        }
                        if (!StringUtil.isEmpty(itemText) && null != itemValue) {
                            CodeItem codeItem = new CodeItem(itemText, itemValue);
                            codeItemList.add(codeItem);
                        }
                    }
                    Method getCodeNameMethod = clazz.getDeclaredMethod("getCodeName");
                    String codeName = ConvertOp.convert2String(getCodeNameMethod.invoke(oo[0]));
                    if (codeDic.containsKey(codeName)) {
                        throw new Exception("存在同样的枚举代码项，名称：" + codeName);
                    }
                    codeDic.put(codeName, codeItemList);
                    CodeMain codeMain = new CodeMain();
                    codeMain.setCodeName(codeName);
                    codeMain.setCodeClassName(clazz.getSimpleName());
                    codeMain.setCodePackageName(clazz.getPackage().getName());
                    codeMain.setCodeItemList(codeItemList);
                    codeMainList.add(codeMain);
                }
            }
            if (codeDic.keySet().size() > 0) {
                System.out.println("扫描并初始化枚举代码项成功，代码项数量：" + codeDic.keySet().size() + "个");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //根据代码项名称获取具体项
    public static List<CodeItem> getCodeList(String codeName) {
        if (codeDic.containsKey(codeName)) {
            return codeDic.get(codeName);
        } else {
            return null;
        }
    }

    public static List<CodeMain> getCodeMainList() {
        return codeMainList;
    }
}
