package com.code2roc.fastboot.framework.mock;

import com.code2roc.fastboot.framework.auth.TokenModel;
import com.code2roc.fastboot.framework.auth.TokenUtil;
import com.code2roc.fastboot.framework.util.StringUtil;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Map;

@Aspect
@Component
@Order(value = 80)
public class MockIdentifyAspect {
    private static Logger logger = LoggerFactory.getLogger(MockIdentifyAspect.class);
    @Autowired
    private TokenUtil tokenUtil;
    @Autowired
    private ApplicationContext applicationContext;

    /**
     * 切面点 指定注解
     */
    @Pointcut("@annotation(com.code2roc.fastboot.framework.mock.MockIdentify) " +
            "|| @within(com.code2roc.fastboot.framework.mock.MockIdentify)" +
            "|| @annotation(com.code2roc.fastboot.framework.quartz.QuartzTaskJob)" +
            "|| @within(com.code2roc.fastboot.framework.quartz.QuartzTaskJob)")
    public void mockIdentifyAspect() {

    }


    @Around("mockIdentifyAspect()")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        MethodSignature signature = (MethodSignature) point.getSignature();
        Method method = signature.getMethod();
        MockIdentify mockIdentify = method.getAnnotation(MockIdentify.class);
        String token = "";
        if (mockIdentify != null) {
            String loginName = mockIdentify.loginName();
            if (!StringUtil.isEmpty(loginName)) {
                Map<String, IMockIdentify> res = applicationContext.getBeansOfType(IMockIdentify.class);
                for (Map.Entry en :res.entrySet()) {
                    IMockIdentify service = (IMockIdentify)en.getValue();
                    TokenModel tokenModel = service.getMockTokenModel(loginName);
                    MockIdentifyContextHolder.setToken(token);
                    logger.info("身份模拟成功，模拟至{}", "【" + tokenModel.getLoginName() + "-" + tokenModel.getUserName() + "】");
                }
            }
        }
        try {
            return point.proceed();
        } finally {
            if (!StringUtil.isEmpty(token)) {
                tokenUtil.removeToken(token);
                MockIdentifyContextHolder.clearToken();
                logger.info("清除身份模拟");
            }
        }
    }
}
