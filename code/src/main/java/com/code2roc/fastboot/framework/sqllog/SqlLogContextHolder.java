package com.code2roc.fastboot.framework.sqllog;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SqlLogContextHolder {
    private static Logger log = LoggerFactory.getLogger(SqlLogContextHolder.class);
    private static final ThreadLocal<String> contextHolder = new ThreadLocal<String>();

    // 调用此方法，切换数据源
    public static void setTag(String methodClassName) {
        contextHolder.set(methodClassName);
    }

    // 获取数据源
    public static String getTag() {
        return contextHolder.get();
    }

    // 删除数据源
    public static void cleatTag() {
        contextHolder.remove();
    }
}
