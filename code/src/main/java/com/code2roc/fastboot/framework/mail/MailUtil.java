package com.code2roc.fastboot.framework.mail;

import com.code2roc.fastboot.framework.util.BeanUtil;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.internet.MimeMessage;
import java.io.File;
import java.util.Date;
import java.util.List;

public class MailUtil {

    public static void sendSimpleMail(String subject, String content, List<String> receiverList) {
        JavaMailSenderImpl mailSender = getSender();
        MailConfig mailConfig = BeanUtil.getBean(MailConfig.class);

        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(mailConfig.getUserName());
        message.setTo(receiverList.toArray(new String[receiverList.size()]));
        message.setSubject(subject);
        message.setText(content);
        message.setSentDate(new Date());
        mailSender.send(message);
    }

    public static void sendMimeMail(String subject, String content,List<String> receiverList) throws Exception {
        JavaMailSenderImpl mailSender = getSender();
        MailConfig mailConfig = BeanUtil.getBean(MailConfig.class);
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);
        helper.setFrom(mailConfig.getUserName());
        helper.setTo(receiverList.toArray(new String[receiverList.size()]));
        helper.setSubject(subject);
        helper.setText(content,true);
        helper.setSentDate(new Date());
        mailSender.send(message);
    }

    public static void sendMimeMail(String subject, String content, List<String> receiverList, List<MailAttachModel> attachList) throws Exception {
        JavaMailSenderImpl mailSender = getSender();
        MailConfig mailConfig = BeanUtil.getBean(MailConfig.class);
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = null;
        if(attachList.size()>0){
            helper = new MimeMessageHelper(message,true);
        }else{
            helper = new MimeMessageHelper(message);
        }
        helper.setFrom(mailConfig.getUserName());
        helper.setTo(receiverList.toArray(new String[receiverList.size()]));
        helper.setSubject(subject);
        helper.setText(content,true);
        helper.setSentDate(new Date());
        for (MailAttachModel mailAttachModel :attachList) {
            helper.addAttachment(mailAttachModel.getAttachName(),new File(mailAttachModel.getFilePath()));
        }
        mailSender.send(message);
    }

    private static JavaMailSenderImpl getSender(){
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        MailConfig mailConfig = BeanUtil.getBean(MailConfig.class);
        mailSender.setUsername(mailConfig.getUserName());
        mailSender.setPassword(mailConfig.getPassword());
        mailSender.setHost(mailConfig.getHost());
        mailSender.setPort(mailConfig.getPort());
        mailSender.setDefaultEncoding("utf-8");
        return mailSender;
    }

}
