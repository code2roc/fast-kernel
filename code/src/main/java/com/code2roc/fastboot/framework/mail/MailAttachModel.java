package com.code2roc.fastboot.framework.mail;

import java.io.Serializable;

public class MailAttachModel implements Serializable {
    private String attachName;
    private String filePath;

    public String getAttachName() {
        return attachName;
    }

    public void setAttachName(String attachName) {
        this.attachName = attachName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
}
