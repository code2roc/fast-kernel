package com.code2roc.fastboot.framework.validatecode;

import com.code2roc.fastboot.framework.global.GlobalEnum;
import com.code2roc.fastboot.framework.redis.RedisUtil;
import com.code2roc.fastboot.framework.security.SecurityConfig;
import com.code2roc.fastboot.framework.util.ConvertOp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.util.Base64;
import java.util.Random;
import java.util.concurrent.TimeUnit;

@Component
public class ValidateCodeUtil {
    private static Random random = new Random();
    private int width = 165; //验证码的宽
    private int height = 45; //验证码的高
    private int lineSize = 30; //验证码中夹杂的干扰线数量
    private int randomStrNum = 4; //验证码字符个数

    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private SecurityConfig securityConfig;

    private String randomString = "0123456789";
    private final String sessionKey = "HaopanValidateCode";

    private int validDBIndex = GlobalEnum.RedisDBNum.ValidateCode.get_value();


    public String getBase64ValidateImage(String key) {
        // BufferedImage类是具有缓冲区的Image类,Image类是用于描述图像信息的类
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_BGR);
        Graphics g = image.getGraphics();
        g.fillRect(0, 0, width, height);
        g.setColor(getRandomColor(105, 189));
        g.setFont(getFont());
        //干扰线
        for (int i = 0; i < lineSize; i++) {
            drawLine(g);
        }

        //随机字符
        String randomStr = "";
        for (int i = 0; i < randomStrNum; i++) {
            randomStr = drawString(g, randomStr, i);
        }
        g.dispose();
        redisUtil.redisTemplateSetForList(key,sessionKey,randomStr,validDBIndex);
        redisUtil.setExpire(key, securityConfig.getValidateCodeExpire(), TimeUnit.SECONDS,validDBIndex);
        String base64String = "";
        try {
            //  直接返回图片
            //  ImageIO.write(image, "PNG", response.getOutputStream());
            //返回 base64
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ImageIO.write(image, "PNG", bos);
            byte[] bytes = bos.toByteArray();
            Base64.Encoder encoder = Base64.getEncoder();
            base64String = encoder.encodeToString(bytes);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return base64String;
    }

    public String checkValidate(String key,String code){
        String errorMessage = "";
        if(redisUtil.isValid(key,validDBIndex)){
            String sessionCode = ConvertOp.convert2String(redisUtil.redisTemplateGetForList(key,sessionKey,validDBIndex));
            if(!code.toLowerCase().equals(sessionCode)){
                errorMessage = "验证码不正确";
            }
        }else{
            errorMessage = "验证码已过期";
        }
        return errorMessage;
    }

    //颜色的设置
    private  Color getRandomColor(int fc, int bc) {
        fc = Math.min(fc, 255);
        bc = Math.min(bc, 255);
        int r = fc + random.nextInt(bc - fc - 16);
        int g = fc + random.nextInt(bc - fc - 14);
        int b = fc + random.nextInt(bc - fc - 12);

        return new Color(r, g, b);
    }

    //字体的设置
    private Font getFont() {
        return new Font("Times New Roman", Font.ROMAN_BASELINE, 40);
    }

    //干扰线的绘制
    private void drawLine(Graphics g) {
        int x = random.nextInt(width);
        int y = random.nextInt(height);
        int xl = random.nextInt(20);
        int yl = random.nextInt(10);
        g.drawLine(x, y, x + xl, y + yl);

    }
    //随机字符的获取
    private  String getRandomString(int num){
        num = num > 0 ? num : randomString.length();
        return String.valueOf(randomString.charAt(random.nextInt(num)));
    }
    //字符串的绘制
    private String drawString(Graphics g, String randomStr, int i) {
        g.setFont(getFont());
        g.setColor(getRandomColor(108, 190));
        //System.out.println(random.nextInt(randomString.length()));
        String rand = getRandomString(random.nextInt(randomString.length()));
        randomStr += rand;
        g.translate(random.nextInt(3), random.nextInt(6));
        g.drawString(rand, 40 * i + 10, 25);
        return randomStr;
    }
}
