package com.code2roc.fastboot.framework.submitlock;

import java.lang.annotation.*;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface SubmitLock {
    int expire() default 2;
}
