package com.code2roc.fastboot.framework.auth;

import com.code2roc.fastboot.framework.security.SecurityConfig;
import com.code2roc.fastboot.framework.util.ConvertOp;
import com.code2roc.fastboot.framework.util.ReflectUtil;
import com.code2roc.fastboot.framework.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class ExtendAuthInterceptor implements HandlerInterceptor {
    @Autowired
    private SecurityConfig securityConfig;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HandlerMethod handlerMethod = null;
        if (handler.getClass() == HandlerMethod.class) {
            handlerMethod = (HandlerMethod) handler;
        }

        ///region Basic认证
        boolean needCheckBasic = false;
        if (handlerMethod != null) {
            if (ReflectUtil.checkClassAnnotationPresent(handlerMethod.getBeanType(), CheckBasic.class)) {
                needCheckBasic = true;
            }
            if (ReflectUtil.checkMethodAnnotationPresent(handlerMethod.getMethod(), CheckBasic.class)) {
                needCheckBasic = true;
            }
        }
        if (needCheckBasic) {
            String authorization = ConvertOp.convert2String(request.getHeader("Authorization"));
            if(StringUtil.isEmpty(authorization)){
                authorization = ConvertOp.convert2String(request.getParameter("Authorization"));
            }
            if (StringUtil.isEmpty(authorization)) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                response.setHeader("WWW-Authenticate", "Basic realm=\"Realm\"");
                return false;
            } else {
                String credentials = authorization.substring("Basic ".length());
                byte[] decodedCredentials = Base64Utils.decode(credentials.getBytes("UTF-8"));
                String realcredentials = new String(decodedCredentials);
                CheckBasic checkBasic = null;
                CheckBasic classBasic = (CheckBasic) ReflectUtil.getClassAnnotation(handlerMethod.getBeanType(), CheckBasic.class);
                CheckBasic methodBasic = (CheckBasic) ReflectUtil.getMethodAnnotation(handlerMethod.getMethod(), CheckBasic.class);
                if (methodBasic != null) {
                    checkBasic = methodBasic;
                } else {
                    checkBasic = classBasic;
                }
                if (!StringUtil.isEmpty(checkBasic.value())) {
                    if (!checkBasic.value().equals(realcredentials)) {
                        response.setStatus(HttpStatus.UNAUTHORIZED.value());
                        response.setHeader("WWW-Authenticate", "Basic realm=\"Realm\"");
                        return false;
                    }
                } else {
                    String settingAuth = securityConfig.getBasicUserName() + ":" + securityConfig.getBasicPassword();
                    if (!settingAuth.equals(realcredentials)) {
                        response.setStatus(HttpStatus.UNAUTHORIZED.value());
                        response.setHeader("WWW-Authenticate", "Basic realm=\"Realm\"");
                        return false;
                    }
                }
            }
        }
        ///endregion

        return true;
    }
}