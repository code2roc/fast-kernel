package com.code2roc.fastboot.framework.route;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import com.code2roc.fastboot.framework.util.ConvertOp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Set;

public class CustomRequestMappingHandlerMapping extends RequestMappingHandlerMapping {
    public CustomRequestMappingHandlerMapping(){
        LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
        Logger logEntity = loggerContext.getLogger("com.code2roc.fastboot.framework.route.CustomRequestMappingHandlerMapping");
        ((ch.qos.logback.classic.Logger) logEntity).setLevel(Level.OFF);
    }


    @Override
    protected HandlerMethod lookupHandlerMethod(String lookupPath, HttpServletRequest request) throws Exception {
        if(ConverRouteUtil.checkExistCover(lookupPath)){
            String redirectRoute = ConverRouteUtil.getRedirectRoute(lookupPath);
            request.setAttribute("redirectTag","1");
            request.setAttribute("redirectRoute",redirectRoute);
            request.setAttribute("lookupPath",lookupPath);
            lookupPath = redirectRoute;
        }else{
            request.setAttribute("redirectTag","0");
        }
        return super.lookupHandlerMethod(lookupPath, request);
    }

    @Override
    protected RequestMappingInfo getMatchingMapping(RequestMappingInfo info, HttpServletRequest request) {
        String redirectTag = ConvertOp.convert2String(request.getAttribute("redirectTag"));
        if(redirectTag.equals("1")){
            String redirectRoute = ConvertOp.convert2String(request.getAttribute("redirectRoute"));
            boolean check = false;
            if( info.getPatternsCondition()!=null){
                Set<String> set =  info.getPatternsCondition().getPatterns();
                if(set.size()>0){
                    String[] array = new String[set.size()];
                    array = set.toArray(array);
                    String pattern = array[0];
                    if(pattern.equals(redirectRoute)){
                        check = true;
                    }
                }
            }
            if(check){
                return info;
            }else{
                return super.getMatchingMapping(info, request);
            }
        }else{
            return super.getMatchingMapping(info, request);
        }
    }
}
