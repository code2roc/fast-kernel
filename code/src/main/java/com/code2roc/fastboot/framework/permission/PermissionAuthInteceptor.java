package com.code2roc.fastboot.framework.permission;

import com.code2roc.fastboot.framework.auth.AnonymousAccess;
import com.code2roc.fastboot.framework.auth.TokenUtil;
import com.code2roc.fastboot.framework.security.SecurityConfig;
import com.code2roc.fastboot.framework.util.BeanUtil;
import com.code2roc.fastboot.framework.util.ReflectUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Component
public class PermissionAuthInteceptor implements HandlerInterceptor {
    @Autowired
    private TokenUtil tokenUtil;
    @Autowired
    private SecurityConfig securityConfig;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //验证配置项
        List<String> anonymousAccessList = securityConfig.getAllowAnonymousAccess();
        String url = request.getRequestURL().toString();
        for (String item:anonymousAccessList) {
            if(item.endsWith("*")){
                if(url.contains(item.replace("*",""))){
                    return true;
                }
            }else{
                if(url.endsWith(item)){
                    return true;
                }
            }
        }
        //验证注解
        if(handler.getClass()== HandlerMethod.class){
            HandlerMethod handlerMethod = (HandlerMethod)handler;
            if(ReflectUtil.checkClassAnnotationPresent(handlerMethod.getBeanType(), AnonymousAccess.class)){
                return true;
            }
            if(ReflectUtil.checkMethodAnnotationPresent(handlerMethod.getMethod(),AnonymousAccess.class)){
                return true;
            }

            boolean checkNeedAuth = false;
            if (ReflectUtil.checkClassAnnotationPresent(handlerMethod.getBeanType(), PermissionAuth.class)) {
                checkNeedAuth = true;
            }
            if (ReflectUtil.checkMethodAnnotationPresent(handlerMethod.getMethod(), PermissionAuth.class)) {
                checkNeedAuth = true;
            }
            if (checkNeedAuth) {
                PermissionAuth classRoleAuth = (PermissionAuth) ReflectUtil.getClassAnnotation(handlerMethod.getBeanType(), PermissionAuth.class);
                PermissionAuth methodRoleAuth = (PermissionAuth) ReflectUtil.getMethodAnnotation(handlerMethod.getMethod(), PermissionAuth.class);
                if (classRoleAuth != null || methodRoleAuth != null) {
                    String value;
                    Class<? extends IPermissionAuth> policy;
                    if (methodRoleAuth != null) {
                        policy = methodRoleAuth.policy();
                    } else {
                        policy = classRoleAuth.policy();
                    }
                    if (policy != null && !policy.equals(IPermissionAuth.class)) {
                        IPermissionAuth userRoleAuth = BeanUtil.getBean(policy);
                        if (!userRoleAuth.checkDataAccess()) {
                            response.sendRedirect(request.getContextPath() + "/frame/error/forbiddenVisitError");
                            return false;
                        }
                    }
                }
            }
        }

        return true;
    }
}
