package com.code2roc.fastboot.framework.database;

public class PartitionItemDetail {
    private String partitionName;
    private int tableRows;

    public String getPartitionName() {
        return partitionName;
    }

    public void setPartitionName(String partitionName) {
        this.partitionName = partitionName;
    }

    public int getTableRows() {
        return tableRows;
    }

    public void setTableRows(int tableRows) {
        this.tableRows = tableRows;
    }
}
