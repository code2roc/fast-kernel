package com.code2roc.fastboot.framework.datasource;

public class DataSourceConfigException extends RuntimeException {
    public DataSourceConfigException(String message)
    {
        super(message);
    }
    public DataSourceConfigException(){

    }
}
