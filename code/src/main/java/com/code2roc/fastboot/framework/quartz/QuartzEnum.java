package com.code2roc.fastboot.framework.quartz;

public class QuartzEnum {
    public enum ExecuteUnit{
        Second(10, "秒"), Minute(20, "分"), Hour(30, "时");
        private int _value;
        private String _name;

        private ExecuteUnit(int value, String name) {
            set_value(value);
            set_name((name));
        }

        public int get_value() {
            return _value;
        }

        public void set_value(int _value) {
            this._value = _value;
        }

        public String get_name() {
            return _name;
        }

        public void set_name(String _name) {
            this._name = _name;
        }

        @Override
        public String toString() {
            return _name;
        }
    }
}
