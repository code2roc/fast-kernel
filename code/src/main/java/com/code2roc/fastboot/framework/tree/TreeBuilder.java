package com.code2roc.fastboot.framework.tree;

import com.alibaba.fastjson.JSON;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TreeBuilder {
    public static List<TreeNode> BuildTree(List<TreeNode> treeNodeList){
        List<TreeNode> result = new ArrayList<TreeNode>();
        //获取顶级节点
        List<TreeNode> topNodeList = treeNodeList.stream().filter((TreeNode t)->t.isTopNode()).collect(Collectors.toList());
        topNodeList.sort(((h1, h2) -> h2.getSotNum().compareTo(h1.getSotNum())));
        for (TreeNode treeNode:topNodeList) {
            TreeNode copyNode = JSON.parseObject(JSON.toJSONString(treeNode),TreeNode.class);
            result.add(copyNode);
            BuildSubTree(treeNodeList,copyNode);
        }
        return result;
    }

    private static void BuildSubTree(List<TreeNode> treeNodeList,TreeNode parentNode){
        //查询父节点下是否有子节点
        List<TreeNode> childList = treeNodeList.stream().filter((TreeNode t)->t.getParentId().equals(parentNode.getId())).collect(Collectors.toList());
        childList.sort(((h1, h2) -> h2.getSotNum().compareTo(h1.getSotNum())));
        if(childList.size()>0){
            //设置父节点有子节点
            parentNode.setHasChildren(true);
            for (int i=0;i<childList.size();i++){
                TreeNode t = childList.get(i);
                //设置子节点有父节点
                t.setHasParent(true);
                //将子节点添加到父节点的children数组中
                TreeNode copyNode = JSON.parseObject(JSON.toJSONString(t),TreeNode.class);
                parentNode.addChildNode(copyNode);
                //对子节点进行递归
                BuildSubTree(treeNodeList,copyNode);
            }
        }else{
            parentNode.setHasChildren(false);
        }
    }
}
