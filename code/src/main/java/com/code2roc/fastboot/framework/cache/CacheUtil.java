package com.code2roc.fastboot.framework.cache;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.code2roc.fastboot.FastBootApplication;
import com.code2roc.fastboot.framework.database.CommonDTO;
import com.code2roc.fastboot.framework.message.GlobalCacheMessageReceiver;
import com.code2roc.fastboot.framework.message.MessageUtil;
import com.code2roc.fastboot.framework.template.BaseModel;
import com.code2roc.fastboot.framework.template.BaseSystemObject;
import com.code2roc.fastboot.framework.util.BeanUtil;
import com.code2roc.fastboot.framework.util.ConvertOp;
import com.code2roc.fastboot.framework.util.ScanUtil;
import com.code2roc.fastboot.framework.util.StringUtil;
import org.ehcache.Cache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.*;

@Component
public class CacheUtil {
    @Autowired
    private CommonDTO commonDTO;
    @Autowired
    private ApplicationContext context;
    @Autowired
    private CacheExecuteUtil cacheExecuteUtil;
    @Autowired
    private MessageUtil messageUtil;
    @Autowired
    private EhcacheConfig ehcacheConfig;
    @Autowired
    private CacheConfig cacheConfig;
    private List<CacheModel> cacheList;

    public void initCache(Class runtimeClass) {
        cacheList = new ArrayList<>();
        List<Class<?>> scanClassList = new ArrayList<>();
        scanClassList.addAll(ScanUtil.getAllClassByPackageName_Annotation(runtimeClass.getPackage(), CacheTable.class));

        if(runtimeClass.getPackage().getName()!= FastBootApplication.class.getPackage().getName()){
            scanClassList.addAll(ScanUtil.getAllClassByPackageName_Annotation(FastBootApplication.class.getPackage(), CacheTable.class));
        }

        for (Class clazz : scanClassList) {
            TableName tableName = (TableName) clazz.getAnnotation(TableName.class);
            SyncInit syncTask = (SyncInit) clazz.getAnnotation(SyncInit.class);
            CacheTable cacheTable = (CacheTable) clazz.getAnnotation(CacheTable.class);
            if (syncTask == null) {
                executeAsyncSingleTableCacheInit(clazz);
            } else {
                initFormCache(clazz);
                System.out.println("【Sync】初始化单表缓存" + clazz.getName() + "完成");
            }
            CacheModel cacheModel = new CacheModel();
            cacheModel.setCacheName(clazz.getName());
            cacheModel.setCacheType("Form");
            cacheModel.setClazz(clazz);
            int heap = cacheConfig.getHeap();
            int offHeap = cacheConfig.getOffheap();
            int disk = cacheConfig.getDisk();
            if (!StringUtil.isEmpty(cacheTable.heap())) {
                heap = ConvertOp.convert2Int(cacheTable.heap());
            }
            if (!StringUtil.isEmpty(cacheTable.offheap())) {
                offHeap = ConvertOp.convert2Int(cacheTable.offheap());
            }
            if (!StringUtil.isEmpty(cacheTable.disk())) {
                disk = ConvertOp.convert2Int(cacheTable.disk());
            }
            cacheModel.setHeap(heap);
            cacheModel.setOffHeap(offHeap);
            cacheModel.setDisk(disk);
            cacheList.add(cacheModel);
        }

        //自定义缓存
        Map<String, ICacheInitHandler> res = context.getBeansOfType(ICacheInitHandler.class);
        if (res.size() > 0) {
            System.out.println("自定义缓存实现类" + res.size() + "个");
        }
        for (Map.Entry en : res.entrySet()) {
            ICacheInitHandler service = (ICacheInitHandler) en.getValue();
            Class clazz = service.getClass();
            SyncInit syncTask = (SyncInit) clazz.getAnnotation(SyncInit.class);
            if (syncTask == null) {
                executeAsyncServiceCacheInit(service);
            } else {
                service.initCache();
                System.out.println("【Sync】初始化自定义缓存" + en.getKey() + "完成");
            }
            CacheModel cacheModel = new CacheModel();
            cacheModel.setCacheName(service.getCacheName());
            cacheModel.setCacheType("Custom");
            cacheModel.setClazz(service.getClass());
            int heap = cacheConfig.getHeap();
            int offHeap = cacheConfig.getOffheap();
            int disk = cacheConfig.getDisk();
            if (!StringUtil.isEmpty(service.getHeap())) {
                heap = ConvertOp.convert2Int(service.getHeap());
            }
            if (!StringUtil.isEmpty(service.getOffheap())) {
                offHeap = ConvertOp.convert2Int(service.getOffheap());
            }
            if (!StringUtil.isEmpty(service.getDisk())) {
                disk = ConvertOp.convert2Int(service.getDisk());
            }
            cacheModel.setHeap(heap);
            cacheModel.setOffHeap(offHeap);
            cacheModel.setDisk(disk);
            cacheList.add(cacheModel);
        }

        System.out.println("缓存初始化完毕");
    }

    @Async
    public void executeAsyncSingleTableCacheInit(Class clazz) {
        initFormCache(clazz);
        System.out.println("【Async】初始化单表缓存" + clazz.getName() + "完成");
    }

    @Async
    public void executeAsyncServiceCacheInit(ICacheInitHandler cacheInitService) {
        cacheInitService.initCache();
        System.out.println("【Async】初始化自定义缓存" + cacheInitService.getCacheName() + "完成");
    }

    public <T extends BaseModel> void insert(T entity) {
        String key = getModelKey(entity);
        if(!StringUtil.isEmpty(key)){
            putCache(entity.getClass().getName(), key, entity);
        }
    }

    public <T extends BaseModel> void delete(Class<T> clazz, String unitguid) {
        deleteCache(clazz.getName(), unitguid);
    }

    public <T extends BaseModel> void clear(Class<T> clazz) {
        clearCache(clazz.getName());
    }

    public <T extends BaseModel> void update(T entity) {
        String key = getModelKey(entity);
        if(!StringUtil.isEmpty(key)){
            putCache(entity.getClass().getName(), key, entity);
        }
    }

    public <T extends BaseModel> T selectOne(Class<T> clazz, String unitguid) {
        return (T) getCache(clazz.getName(), unitguid);
    }

    public <T extends BaseModel> List<T> selectAllList(Class<T> clazz) {
        List<T> result = new ArrayList<>();
        List<Object> cacheResult = getAllCache(clazz.getName());
        for (Object obj : cacheResult) {
            result.add((T) obj);
        }
        return result;
    }

    public <T extends BaseModel> String getModelKey(T entity){
        String key = "";
        Class clazz = entity.getClass();
        List<Field> fs = Arrays.asList(clazz.getDeclaredFields());
        for (Field field:fs) {
            field.setAccessible(true);
            //TableId修饰主键
            if(null!=field.getAnnotation(TableId.class)){
                try{
                    key = ConvertOp.convert2String( field.get(entity));
                }catch (Exception e){

                }
            }
        }
        return key;
    }

    public Object getCache(String cacheName, String key) {
        Cache cache = ehcacheConfig.getInstance().getCache(cacheName, String.class, BaseSystemObject.class);
        return cache.get(key);
    }

    public boolean existCache(String cacheName, String key) {
        Cache cache = ehcacheConfig.getInstance().getCache(cacheName, String.class, BaseSystemObject.class);
        return cache.containsKey(key);
    }

    public List<Object> getAllCache(String cacheName) {
        List result = new ArrayList<>();
        Cache cache = ehcacheConfig.getInstance().getCache(cacheName, String.class, BaseSystemObject.class);
        Iterator iter = cache.iterator();
        while (iter.hasNext()) {
            Cache.Entry entry = (Cache.Entry) iter.next();
            result.add(entry.getValue());
        }
        return result;
    }

    public List<String> getAllCacheKey(String cacheName) {
        List<String> result = new ArrayList<String>();
        Cache cache = ehcacheConfig.getInstance().getCache(cacheName, String.class, BaseSystemObject.class);
        Iterator iter = cache.iterator();
        while (iter.hasNext()) {
            Cache.Entry entry = (Cache.Entry) iter.next();
            result.add(entry.getKey().toString());
        }
        return result;
    }

    public void initFormCache(Class clazz) {
        TableName tableName = (TableName) clazz.getAnnotation(TableName.class);
        List<LinkedHashMap<String, Object>> resultList = commonDTO.selectList(tableName.value(), "*", "1=1", "", new HashMap<>());
        for (LinkedHashMap<String, Object> map : resultList) {
            Cache cache = ehcacheConfig.getInstance().getCache(clazz.getName(), String.class, BaseSystemObject.class);
            String unitguid = ConvertOp.convert2String(map.get("UnitGuid"));
            try {
                Object obj = clazz.newInstance();
                obj = ConvertOp.convertLinkHashMapToBean(map, obj);
                cache.put(unitguid, obj);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void initFormKeyCache(Class clazz, String key) {
        TableName tableName = (TableName) clazz.getAnnotation(TableName.class);
        String sql = "UnitGuid = #{UnitGuid}";
        HashMap paramMap = new HashMap();
        paramMap.put("UnitGuid", key);
        List<LinkedHashMap<String, Object>> resultList = commonDTO.selectList(tableName.value(), "*", "1=1", "", new HashMap<>());
        for (LinkedHashMap<String, Object> map : resultList) {
            Cache cache = ehcacheConfig.getInstance().getCache(clazz.getName(), String.class, BaseSystemObject.class);
            try {
                Object obj = clazz.newInstance();
                obj = ConvertOp.convertLinkHashMapToBean(map, obj);
                cache.put(key, obj);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public List<CacheModel> getCacheList() {
        return cacheList;
    }

    public void putCache(String cacheName, String key, BaseSystemObject value) {
        putGlobalCache(cacheName, key, value, true);
    }

    public void deleteCache(String cacheName, String key) {
        deleteGlobalCache(cacheName, key, true);
    }

    public void clearCache(String cacheName) {
        clearGlobalCache(cacheName, true);
    }

    public void refreshCache(String cacheName) {
        refreshGlobalCache(cacheName, true);
    }

    public void refreshCache(String cacheName, String key) {
        refreshGlobalSingleCache(cacheName, key, true);
    }

    @Async
    public void refreshCacheSync(String cacheName) {
        refreshGlobalCacheSync(cacheName, true);
    }

    @Async
    public void refreshCacheSync(String cacheName, String key) {
        refreshGlobalSingleCacheSync(cacheName, key, true);
    }

    //======================================================================================================

    public void putGlobalCache(String cacheName, String key, BaseSystemObject value, Boolean noticeGlobal) {
        cacheExecuteUtil.putCache(cacheName, key, value);
        if (noticeGlobal) {
            try {
                Object[] paramList = new Object[]{cacheName, key, value, false};
                Class[] paramClassList = new Class[]{String.class, String.class, BaseSystemObject.class, Boolean.class};
                GlobalCacheInfo globalCacheInfo = new GlobalCacheInfo("putGlobalCache", paramClassList, paramList);
                globalCacheInfo.setClassName(value.getClass().getName());
                sendCacheChangeNoticeToGlobal(globalCacheInfo);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void deleteGlobalCache(String cacheName, String key, Boolean noticeGlobal) {
        cacheExecuteUtil.deleteCache(cacheName, key);
        if (noticeGlobal) {
            try {
                Object[] paramList = new Object[]{cacheName, key, false};
                Class[] paramClassList = new Class[]{String.class, String.class, Boolean.class};
                GlobalCacheInfo globalCacheInfo = new GlobalCacheInfo("deleteGlobalCache", paramClassList, paramList);
                sendCacheChangeNoticeToGlobal(globalCacheInfo);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void clearGlobalCache(String cacheName, Boolean noticeGlobal) {
        cacheExecuteUtil.clearCache(cacheName);
        if (noticeGlobal) {
            try {
                Object[] paramList = new Object[]{cacheName, false};
                Class[] paramClassList = new Class[]{String.class, Boolean.class};
                GlobalCacheInfo globalCacheInfo = new GlobalCacheInfo("clearGlobalCache", paramClassList, paramList);
                sendCacheChangeNoticeToGlobal(globalCacheInfo);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void refreshGlobalCache(String cacheName, Boolean noticeGlobal) {
        for (CacheModel cacheModel : cacheList) {
            if (cacheModel.getCacheName().equals(cacheName)) {
                Cache cache = ehcacheConfig.getInstance().getCache(cacheName, String.class, BaseSystemObject.class);
                cache.clear();
                if (cacheModel.getCacheType().equals("Form")) {
                    initFormCache(cacheModel.getClazz());
                } else {
                    ICacheInitHandler service = (ICacheInitHandler) BeanUtil.getBean(cacheModel.getClazz());
                    service.initCache();
                }
                System.out.println("手动刷新缓存" + cacheName + "成功");
                break;
            }
        }
        if (noticeGlobal) {
            try {
                Object[] paramList = new Object[]{cacheName, false};
                Class[] paramClassList = new Class[]{String.class, Boolean.class};
                GlobalCacheInfo globalCacheInfo = new GlobalCacheInfo("refreshGlobalCache", paramClassList, paramList);
                sendCacheChangeNoticeToGlobal(globalCacheInfo);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void refreshGlobalSingleCache(String cacheName, String key, Boolean noticeGlobal) {
        for (CacheModel cacheModel : cacheList) {
            if (cacheModel.getCacheName().equals(cacheName)) {
                if (cacheModel.getCacheType().equals("Form")) {
                    initFormKeyCache(cacheModel.getClazz(), key);
                } else {
                    ICacheInitHandler service = (ICacheInitHandler) BeanUtil.getBean(cacheModel.getClazz());
                    service.initKeyCache(key);
                }
                System.out.println("手动刷新缓存" + cacheName + "-" + key + "成功");
                break;
            }
        }
        if (noticeGlobal) {
            try {
                Object[] paramList = new Object[]{cacheName, key, false};
                Class[] paramClassList = new Class[]{String.class,String.class, Boolean.class};
                GlobalCacheInfo globalCacheInfo = new GlobalCacheInfo("refreshGlobalSingleCache", paramClassList, paramList);
                sendCacheChangeNoticeToGlobal(globalCacheInfo);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Async
    public void refreshGlobalCacheSync(String cacheName, Boolean noticeGlobal) {
        refreshGlobalCache(cacheName, noticeGlobal);
        if (noticeGlobal) {
            try {
                Object[] paramList = new Object[]{cacheName, false};
                Class[] paramClassList = new Class[]{String.class, Boolean.class};
                GlobalCacheInfo globalCacheInfo = new GlobalCacheInfo("refreshGlobalCacheSync", paramClassList, paramList);
                sendCacheChangeNoticeToGlobal(globalCacheInfo);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Async
    public void refreshGlobalSingleCacheSync(String cacheName, String key, Boolean noticeGlobal) {
        refreshGlobalSingleCache(cacheName, key, noticeGlobal);
        if (noticeGlobal) {
            try {
                Object[] paramList = new Object[]{cacheName, key, false};
                Class[] paramClassList = new Class[]{String.class, String.class, Boolean.class};
                GlobalCacheInfo globalCacheInfo = new GlobalCacheInfo("refreshGlobalSingleCacheSync", paramClassList, paramList);
                sendCacheChangeNoticeToGlobal(globalCacheInfo);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void sendCacheChangeNoticeToGlobal(GlobalCacheInfo globalCacheInfo) {
        messageUtil.sendGlobalMessage(globalCacheInfo, GlobalCacheMessageReceiver.class, new HashMap());
    }
}
