package com.code2roc.fastboot.framework.datadic;

import com.code2roc.fastboot.framework.template.BaseSystemObject;

public class CodeItem  extends BaseSystemObject {
    private String _itemText;
    private Object _itemValue;

    public CodeItem(){

    }

    public CodeItem(String itemText, Object itemValue){
        _itemText = itemText;
        _itemValue = itemValue;
    }

    public String get_itemText() {
        return _itemText;
    }

    public void set_itemText(String _itemText) {
        this._itemText = _itemText;
    }

    public Object get_itemValue() {
        return _itemValue;
    }

    public void set_itemValue(Object _itemValue) {
        this._itemValue = _itemValue;
    }
}
