package com.code2roc.fastboot.framework.cache;

import com.code2roc.fastboot.FastBootApplication;
import com.code2roc.fastboot.framework.template.BaseSystemObject;
import com.code2roc.fastboot.framework.util.ConvertOp;
import com.code2roc.fastboot.framework.util.ScanUtil;
import com.code2roc.fastboot.framework.util.StringUtil;
import org.ehcache.CacheManager;
import org.ehcache.config.CacheConfiguration;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ExpiryPolicyBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.config.units.EntryUnit;
import org.ehcache.config.units.MemoryUnit;
import org.ehcache.expiry.ExpiryPolicy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Configuration
@EnableCaching
public class EhcacheConfig {
    @Autowired
    private CacheConfig cacheConfig;
    @Autowired
    private ApplicationContext context;
    private CacheManager cacheManager;

    public CacheManager getInstance() {
        if (cacheManager == null) {
            CacheManagerBuilder cacheManagerBuilder = CacheManagerBuilder.newCacheManagerBuilder()
                    .with(CacheManagerBuilder.persistence(cacheConfig.getDiskDir()));


            Map<String, Object> annotatedBeans = context.getBeansWithAnnotation(SpringBootApplication.class);
            Class runtimeClass = annotatedBeans.values().toArray()[0].getClass();
            //do，dao扫描
            List<Class<?>> scanClassList = new ArrayList<>();
            scanClassList.addAll(ScanUtil.getAllClassByPackageName_Annotation(runtimeClass.getPackage(), CacheTable.class));

            if(runtimeClass.getPackage().getName()!= FastBootApplication.class.getPackage().getName()){
                scanClassList.addAll(ScanUtil.getAllClassByPackageName_Annotation(FastBootApplication.class.getPackage(), CacheTable.class));
            }

            for (Class clazz : scanClassList) {
                cacheManagerBuilder =cacheManagerBuilder.withCache(clazz.getName(), getFormConfig((CacheTable)clazz.getAnnotation(CacheTable.class)));
            }

            //自定义缓存
            String[] beanNameList = context.getBeanNamesForType(ICacheInitHandler.class);
            for (String beanName : beanNameList) {
                ICacheInitHandler service = (ICacheInitHandler) context.getBean(beanName);
                cacheManagerBuilder = cacheManagerBuilder.withCache(service.getCacheName(), getServicemConfig(service));
            }
            cacheManager = cacheManagerBuilder.build(true);
        }
        return cacheManager;
    }

    public CacheConfiguration getFormConfig(CacheTable hpCache){
        int heap = cacheConfig.getHeap();
        int offHeap = cacheConfig.getOffheap();
        int disk = cacheConfig.getDisk();
        if(!StringUtil.isEmpty(hpCache.heap())){
            heap = ConvertOp.convert2Int(hpCache.heap());
        }
        if(!StringUtil.isEmpty(hpCache.offheap())){
            offHeap = ConvertOp.convert2Int(hpCache.offheap());
        }
        if(!StringUtil.isEmpty(hpCache.disk())){
            disk = ConvertOp.convert2Int(hpCache.disk());
        }

        //资源池生成器配置持久化
        ResourcePoolsBuilder resourcePoolsBuilder = ResourcePoolsBuilder.newResourcePoolsBuilder().
                // 堆内缓存大小
                        heap(heap, EntryUnit.ENTRIES)
                // 堆外缓存大小
                .offheap(offHeap, MemoryUnit.MB)
                // 文件缓存大小
                .disk(disk, MemoryUnit.MB);
        //生成配置
        ExpiryPolicy expiryPolicy = ExpiryPolicyBuilder.noExpiration();
        CacheConfiguration config = CacheConfigurationBuilder.newCacheConfigurationBuilder(String.class, BaseSystemObject.class, resourcePoolsBuilder)
                //设置永不过期
                .withExpiry(expiryPolicy)
                .build();
        return config;
    }

    public CacheConfiguration getServicemConfig(ICacheInitHandler cacheInitService){
        int heap = cacheConfig.getHeap();
        int offHeap = cacheConfig.getOffheap();
        int disk = cacheConfig.getDisk();
        if(!StringUtil.isEmpty(cacheInitService.getHeap())){
            heap = ConvertOp.convert2Int(cacheInitService.getHeap());
        }
        if(!StringUtil.isEmpty(cacheInitService.getOffheap())){
            offHeap = ConvertOp.convert2Int(cacheInitService.getOffheap());
        }
        if(!StringUtil.isEmpty(cacheInitService.getDisk())){
            disk = ConvertOp.convert2Int(cacheInitService.getDisk());
        }

        //资源池生成器配置持久化
        ResourcePoolsBuilder resourcePoolsBuilder = ResourcePoolsBuilder.newResourcePoolsBuilder().
                // 堆内缓存大小
                        heap(heap, EntryUnit.ENTRIES)
                // 堆外缓存大小
                .offheap(offHeap, MemoryUnit.MB)
                // 文件缓存大小
                .disk(disk, MemoryUnit.MB);
        //生成配置
        ExpiryPolicy expiryPolicy = ExpiryPolicyBuilder.noExpiration();
        CacheConfiguration config = CacheConfigurationBuilder.newCacheConfigurationBuilder(String.class, BaseSystemObject.class, resourcePoolsBuilder)
                //设置永不过期
                .withExpiry(expiryPolicy)
                .build();
        return config;
    }
}
