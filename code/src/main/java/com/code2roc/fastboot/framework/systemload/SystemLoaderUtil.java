package com.code2roc.fastboot.framework.systemload;

import com.code2roc.fastboot.framework.global.SystemConfig;
import com.code2roc.fastboot.framework.util.BeanUtil;
import com.code2roc.fastboot.framework.util.ClassPathFileUtil;
import com.code2roc.fastboot.framework.util.FileUtil;

import java.io.File;


public class SystemLoaderUtil {
    public static void clearLoaderTag() {
        SystemConfig systemConfig = BeanUtil.getBean(SystemConfig.class);
        File tagFile = new File(systemConfig.getTemplatePath() + "systemloader.txt");
        if (tagFile.exists()) {
            tagFile.delete();
        }
    }

    public static void addLoaderTag() {
        SystemConfig systemConfig = BeanUtil.getBean(SystemConfig.class);
        File tagFile = new File(systemConfig.getTemplatePath() + "systemloader.txt");
        try {
            tagFile.createNewFile();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean checkLoaderTag() {
        SystemConfig systemConfig = BeanUtil.getBean(SystemConfig.class);
        File tagFile = new File(systemConfig.getTemplatePath() + "systemloader.txt");
        return tagFile.exists();
    }

    public static String getFrameVersion() {
        String fileName = ClassPathFileUtil.getFilePath("version.txt");
        String version = FileUtil.readFileFirstLine(fileName).trim().replace("当前版本：", "");
        return version;
    }

    public static boolean checkFrameVersion() {
        boolean checkResult = false;
        SystemConfig systemConfig = BeanUtil.getBean(SystemConfig.class);
        String fileName = systemConfig.getTemplatePath() + "systemversion.txt";
        File tagFile = new File(fileName);
        if (tagFile.exists()) {
            String fileReadVersion = FileUtil.readFileContent(fileName);
            if (getFrameVersion().equals(fileReadVersion)) {
                checkResult = true;
            }
        }
        return checkResult;
    }

    public static void updateFrameVersion() {
        SystemConfig systemConfig = BeanUtil.getBean(SystemConfig.class);
        String fileName = systemConfig.getTemplatePath() + "systemversion.txt";
        FileUtil.writeFile(fileName, getFrameVersion(), false);
    }
}
