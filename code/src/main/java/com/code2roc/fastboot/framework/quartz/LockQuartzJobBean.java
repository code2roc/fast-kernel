package com.code2roc.fastboot.framework.quartz;

import com.code2roc.fastboot.framework.redis.RedisLockUtil;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

@Component
public class LockQuartzJobBean extends QuartzJobBean {
    @Autowired
    private RedisLockUtil redisLockUtil;

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        String jobName = context.getJobDetail().getKey().getName();
        try {
            if (!redisLockUtil.lock(jobName)) {
                executeCustomerInternal(context);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            redisLockUtil.unLock(jobName);
        }
    }

    public void executeCustomerInternal(JobExecutionContext context) throws JobExecutionException {

    }
}

