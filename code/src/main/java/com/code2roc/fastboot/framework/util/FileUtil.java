package com.code2roc.fastboot.framework.util;

import java.io.*;

public class FileUtil {
    public static byte[] getBytes(String filePath) {
        byte[] buffer = null;
        try {
            File file = new File(filePath);
            FileInputStream fis = new FileInputStream(file);
            ByteArrayOutputStream bos = new ByteArrayOutputStream(1000);
            byte[] b = new byte[1000];
            int n;
            while ((n = fis.read(b)) != -1) {
                bos.write(b, 0, n);
            }
            fis.close();
            bos.close();
            buffer = bos.toByteArray();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return buffer;
    }

    private static void copy(String f1, String f2) throws IOException {
        File file1 = new File(f1);
        /*     File file2=new File(f2);*/

        File[] flist = file1.listFiles();
        for (File f : flist) {
            if (f.isFile()) {
                copyFile(f.getPath(), f2 + "/" + f.getName()); //调用复制文件的方法
                //System.out.println("原路径["+f.getPath()+"] 被复制路径["+f2+"/"+f.getName()+"]");
            } else if (f.isDirectory()) {
                copyFileFolder(f.getPath(), f2 + "/" + f.getName()); //调用复制文件夹的方法
                //System.out.println("原路径["+f.getPath()+"] 被复制路径["+f2+"/"+f.getName()+"]");
            }
        }
    }

    public static void copyFileFolder(String sourceFolderPath, String targetFolderPath) throws IOException {
        //创建文件夹
        File file = new File(targetFolderPath);
        if (!file.exists()) {
            file.mkdirs();
        }
        copy(sourceFolderPath, targetFolderPath);
    }

    public static void copyFile(String sourceFilePath, String tagretFilePath) throws IOException {
        try {
            File file = new File(tagretFilePath);
            if (file.exists()) {
                file.delete();
            }
            InputStream in = new FileInputStream(sourceFilePath);
            OutputStream out = new FileOutputStream(tagretFilePath);
            byte[] buffer = new byte[2048];
            int nBytes = 0;
            while ((nBytes = in.read(buffer)) > 0) {
                out.write(buffer, 0, nBytes);
            }
            out.flush();
            out.close();
            in.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static boolean delete(String fileName) {
        File file = new File(fileName);
        if (!file.exists()) {
            //System.out.println("删除文件失败:" + fileName +"不存在！");
            return false;
        } else {
            if (file.isFile())
                return deleteFile(fileName);
            else
                return deleteFileFolder(fileName);
        }
    }

    public static boolean deleteFile(String fileName) {
        File file = new File(fileName);
        // 如果文件路径所对应的文件存在，并且是一个文件，则直接删除
        if (file.exists() && file.isFile()) {
            if (file.delete()) {
                //System.out.println("删除单个文件" + fileName +"成功！");
                return true;
            } else {
                //System.out.println("删除单个文件" + fileName +"失败！");
                return false;
            }
        } else {
            //System.out.println("删除单个文件失败：" + fileName +"不存在！");
            return false;
        }
    }

    public static boolean deleteFileFolder(String dir) {
        // 如果dir不以文件分隔符结尾，自动添加文件分隔符
        if (!dir.endsWith(File.separator))
            dir = dir + File.separator;
        File dirFile = new File(dir);
        // 如果dir对应的文件不存在，或者不是一个目录，则退出
        if ((!dirFile.exists()) || (!dirFile.isDirectory())) {
            System.out.println("删除目录失败：" + dir + "不存在！");
            return false;
        }
        boolean flag = true;
        // 删除文件夹中的所有文件包括子目录
        File[] files = dirFile.listFiles();
        for (int i = 0; i < files.length; i++) {
            // 删除子文件
            if (files[i].isFile()) {
                flag = deleteFile(files[i].getAbsolutePath());
                if (!flag)
                    break;
            }
            // 删除子目录
            else if (files[i].isDirectory()) {
                flag = deleteFileFolder(files[i].getAbsolutePath());
                if (!flag)
                    break;
            }
        }
        if (!flag) {
            //System.out.println("删除目录失败！");
            return false;
        }
        // 删除当前目录
        if (dirFile.delete()) {
            //System.out.println("删除目录" + dir +"成功！");
            return true;
        } else {
            return false;
        }
    }

    public static String readFileContent(String fileName) {
        String fileContent = "";
        FileInputStream fis = null;
        InputStreamReader isr = null;
        try{
            fis = new FileInputStream(fileName);
            isr = new InputStreamReader(fis, "UTF-8");
            BufferedReader br = new BufferedReader(isr);
            String line = null;
            while ((line = br.readLine()) != null) {
                fileContent += line;
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try{
                if(isr!=null){
                    isr.close();
                }
                if(fis!=null){
                    fis.close();
                }
            }catch (Exception e){

            }
        }
        return fileContent;
    }

    public static String readFileFirstLine(String fileName) {
        String fileContent = "";
        FileInputStream fis = null;
        InputStreamReader isr = null;
        try{
            fis = new FileInputStream(fileName);
            isr = new InputStreamReader(fis, "UTF-8");
            BufferedReader br = new BufferedReader(isr);
            fileContent = br.readLine();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try{
                if(isr!=null){
                    isr.close();
                }
                if(fis!=null){
                    fis.close();
                }
            }catch (Exception e){

            }
        }
        return fileContent;
    }

    public static String getFileContent(String fileName) {
        String fileContent = "";
        try {
            String encoding = "UTF-8";
            File file = new File(fileName);
            Long filelength = file.length();
            byte[] filecontent = new byte[filelength.intValue()];
            FileInputStream in = new FileInputStream(file);
            in.read(filecontent);
            fileContent =  new String(filecontent, encoding);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fileContent;
    }

    public static void writeFile(String path, String fileName, String content) throws IOException {
        FileOutputStream fos = null;
        OutputStreamWriter osw = null;
        try {
            File f = new File(path);
            if (!f.exists()) {
                f.mkdirs();
            }
            fos = new FileOutputStream(path + fileName);
            osw = new OutputStreamWriter(fos, "UTF-8");
            osw.write(content);
            osw.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }finally {
            try{
                if(osw!=null){
                    osw.close();
                }
                if(fos!=null){
                    fos.close();
                }
            }catch (Exception e){

            }
        }
    }

    public static void writeFile(String fileName, String content,boolean append) {
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(fileName,append);
            fos.write(content.getBytes("UTF-8"));
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try{
                if(fos!=null){
                    fos.close();
                }
            }catch (Exception e){

            }
        }
    }

    public static void writeFile(String fileName, byte[] fileContent) throws Exception {
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(fileName);
            fos.write(fileContent);
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try{
                if(fos!=null){
                    fos.close();
                }
            }catch (Exception e){

            }
        }
    }

    public static void initfloderPath(String floderName){
        if (!StringUtil.isEmpty(floderName)) {
            String[] pathList = floderName.split("/");
            String temp = "";
            for (int i = 0; i < pathList.length; i++) {
                temp += pathList[i] + "/";
                File logFolder = new File(temp);
                if (!logFolder.exists()) {
                    logFolder.mkdirs();
                }
            }
        }
    }

}
