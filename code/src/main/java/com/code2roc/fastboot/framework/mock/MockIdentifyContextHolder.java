package com.code2roc.fastboot.framework.mock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MockIdentifyContextHolder {
    private static Logger log = LoggerFactory.getLogger(MockIdentifyContextHolder.class);
    // 对当前线程的操作-线程安全的
    private static final ThreadLocal<String> contextHolder = new ThreadLocal<String>();

    // 调用此方法，切换数据源
    public static void setToken(String token) {
        contextHolder.set(token);
        log.debug("已切换身份模拟:{}",token);
    }

    // 获取数据源
    public static String getToken() {
        return contextHolder.get();
    }

    // 删除数据源
    public static void clearToken() {
        contextHolder.remove();
        log.debug("已移除身份模拟");
    }
}
