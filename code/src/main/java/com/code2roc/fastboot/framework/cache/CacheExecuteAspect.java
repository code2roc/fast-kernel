package com.code2roc.fastboot.framework.cache;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

@Aspect
@Component
@Order(value = 101)
public class CacheExecuteAspect {
    @Autowired
    private CacheExecuteUtil cacheExecuteUtil;


    /**
     * 切面点 指定注解
     */
    @Pointcut("@annotation(com.code2roc.fastboot.framework.cache.CacheTransactional) " +
            "|| @within(com.code2roc.fastboot.framework.cache.CacheTransactional)")
    public void cacheExecuteAspect() {

    }

    /**
     * 拦截方法指定为 repeatSubmitAspect
     */
    @Around("cacheExecuteAspect()")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        MethodSignature signature = (MethodSignature) point.getSignature();
        Method method = signature.getMethod();
        CacheTransactional cacheTransactional = method.getAnnotation(CacheTransactional.class);
        if (cacheTransactional != null) {
            cacheExecuteUtil.putCacheIntoTransition();
            try{
                Object obj = point.proceed();
                cacheExecuteUtil.executeOperation();
                return obj;
            }catch (Exception e){
                e.printStackTrace();
                throw  e;
            }
        } else {
            return point.proceed();
        }
    }
}
