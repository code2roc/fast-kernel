package com.code2roc.fastboot.framework.util;

import com.alibaba.fastjson.annotation.JSONField;
import com.code2roc.fastboot.FastBootApplication;
import com.code2roc.fastboot.framework.template.BaseSystemObject;

import java.lang.reflect.Field;
import java.util.*;

public class FastJsonUtil {
    private static HashMap<String, String> dateMap = new HashMap();

    public static void scanDate2Json(Class runtimeClass) {
        List<Class> filterClassList = new ArrayList<>();
        filterClassList.add(BaseSystemObject.class);
        List<Class<?>> scanClassList = new ArrayList<>();
        scanClassList.addAll(ScanUtil.getAllClassByPackageName(runtimeClass.getPackage(), filterClassList));

        if(runtimeClass.getPackage().getName()!= FastBootApplication.class.getPackage().getName()){
            scanClassList.addAll(ScanUtil.getAllClassByPackageName(FastBootApplication.class.getPackage(), filterClassList));
        }

        for (Class clazz : scanClassList) {
            List<Field> fs = Arrays.asList(clazz.getDeclaredFields());
            for (Field field : fs) {
                field.setAccessible(true);
                if (field.getType() == Date.class) {
                    JSONField jsonField = field.getAnnotation(JSONField.class);
                    if (jsonField != null && !StringUtil.isEmpty(jsonField.format())) {
                        dateMap.put(clazz.getName() + "|" + field.getName(), jsonField.format());
                    }
                }
            }
        }
    }

    public static boolean checkDate2Json(String key){
        return dateMap.containsKey(key);
    }

    public static String getDate2JsonFormat(String key){
        return dateMap.get(key);
    }
}
