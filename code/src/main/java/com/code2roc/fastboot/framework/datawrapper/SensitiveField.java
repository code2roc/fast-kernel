package com.code2roc.fastboot.framework.datawrapper;

import java.lang.annotation.*;

@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface SensitiveField {
    Class<? extends BaseSensitive> value();
}
