package com.code2roc.fastboot.framework.message;

import java.util.HashMap;

public interface IMessageReceiver {
    void handleMessage(Object bodyObject, HashMap extraParam);
}
