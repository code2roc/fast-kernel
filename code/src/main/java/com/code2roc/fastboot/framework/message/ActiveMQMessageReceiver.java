package com.code2roc.fastboot.framework.message;

import com.alibaba.fastjson.JSON;
import com.code2roc.fastboot.framework.util.BeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
@ConditionalOnProperty(name = "system.messageChannel", havingValue = "activemq", matchIfMissing = false)
public class ActiveMQMessageReceiver {
    @Autowired
    private ChannelConfig channelConfig;

    @JmsListener(destination = "#{@channelConfig.channelName}", containerFactory = "customQueueListener")
    public void handleSystemMessage(String message) {
        MessageModel messageModel = JSON.parseObject(message, MessageModel.class);
        IMessageReceiver receiver = BeanUtil.getBean(messageModel.getHandleClazz());
        receiver.handleMessage(JSON.parseObject(messageModel.getBodyContent(), messageModel.getBodyClass()), messageModel.getExtraParam());
    }

    @JmsListener(destination = "#{@channelConfig.globalChannelName}", containerFactory = "customQueueListener")
    public void handleGlobalMessage(String message) {
        MessageModel messageModel = JSON.parseObject(message, MessageModel.class);
        //全局消息处理排除消息发送者
        if(!channelConfig.getChannelName().equals(messageModel.getSendFromSystemChannel())){
            IMessageReceiver receiver = BeanUtil.getBean(messageModel.getHandleClazz());
            receiver.handleMessage(JSON.parseObject(messageModel.getBodyContent(), messageModel.getBodyClass()), messageModel.getExtraParam());
        }
    }
}
