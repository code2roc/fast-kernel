package com.code2roc.fastboot.framework.message;

import com.alibaba.fastjson.JSON;
import com.code2roc.fastboot.framework.global.SystemConfig;
import com.code2roc.fastboot.framework.redis.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
public class MessageUtil {
    @Autowired
    private SystemConfig systemConfig;
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;
    @Autowired
    private ChannelConfig channelConfig;

    public void sendMessage(Object messageBody, Class<? extends IMessageReceiver> handleClass, HashMap<String, Object> extraParam) {
        MessageModel messageModel = new MessageModel();
        messageModel.setHandleClazz(handleClass);
        messageModel.setBodyClass(messageBody.getClass());
        messageModel.setBodyContent(JSON.toJSONString(messageBody));
        messageModel.setSendFromSystemChannel(channelConfig.getChannelName());
        if (extraParam != null) {
            for (String key : extraParam.keySet()) {
                messageModel.getExtraParam().put(key, extraParam.get(key));
            }
        }
        if (systemConfig.getMessageChannel().equals("redis")) {
            redisUtil.sendMessage(channelConfig.getChannelName(),JSON.toJSON(messageModel));
        } else {
            jmsMessagingTemplate.convertAndSend(channelConfig.getChannelName(), JSON.toJSONString(messageModel));
        }
    }

    public void sendGlobalMessage(Object messageBody, Class<? extends IMessageReceiver> handleClass, HashMap<String, Object> extraParam) {
        MessageModel messageModel = new MessageModel();
        messageModel.setHandleClazz(handleClass);
        messageModel.setBodyClass(messageBody.getClass());
        messageModel.setBodyContent(JSON.toJSONString(messageBody));
        messageModel.setSendFromSystemChannel(channelConfig.getChannelName());
        messageModel.setGlobalMessage(true);
        if (extraParam != null) {
            for (String key : extraParam.keySet()) {
                messageModel.getExtraParam().put(key, extraParam.get(key));
            }
        }
        if (systemConfig.getMessageChannel().equals("redis")) {
            redisUtil.sendMessage(channelConfig.getGlobalChannelName(),JSON.toJSON(messageModel));
        } else {
            jmsMessagingTemplate.convertAndSend(channelConfig.getGlobalChannelName(), JSON.toJSONString(messageModel));
        }
    }
}
