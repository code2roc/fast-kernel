package com.code2roc.fastboot.framework.global;

public class SystemErrorCodeEnum {
    public enum ErrorCode {
        SystemUnLoader(-1999, "System in Loading"),
        TokenAuthError(-999, "Token无效"), SystemExecuteException(-998, "系统执行发生错误"), SystemRunningException(-997, "系统运行发生错误"),
        NotFoundError(-996, "请求路径未找到"), RequestParamTypeError(-981, "请求参数格式错误"), RepeatSubmitError(-980, "重复请求，请稍后再试"),
        SqlInjectionError(-975, "提交信息存在SQL注入风险"), NoAuthAccess(-850, "无权限访问"),DataSourceNotExist(-840, "数据源配置不正确");

        private int _value;
        private String _name;

        private ErrorCode(int value, String name) {
            set_value(value);
            set_name((name));
        }

        public int get_value() {
            return _value;
        }

        public void set_value(int _value) {
            this._value = _value;
        }

        public String get_name() {
            return _name;
        }

        public void set_name(String _name) {
            this._name = _name;
        }

        @Override
        public String toString() {
            return _name;
        }
    }
}
