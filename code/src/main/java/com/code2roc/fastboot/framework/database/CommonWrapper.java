package com.code2roc.fastboot.framework.database;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.code2roc.fastboot.framework.template.BaseModel;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

@Component
public class CommonWrapper {
    public CommonWrapper(){

    }

    private  <T extends BaseModel> UpdateWrapper getCommonWrapper(T entity){
        return getCommonWrapper(entity,false);
    }

    private  <T extends BaseModel> UpdateWrapper getCommonWrapper(T entity,boolean autoFillKeyCondition){
        UpdateWrapper<T> wrapper = new UpdateWrapper<T>();
        if(null!=entity && autoFillKeyCondition){
            try{
                Class clazz = entity.getClass();
                List<Field> fs = Arrays.asList(clazz.getDeclaredFields());
                for (Field field:fs) {
                    field.setAccessible(true);
                    //TableId修饰主键
                    if(null!=field.getAnnotation(TableId.class)){
                        wrapper.eq(field.getName(),field.get(entity));
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return wrapper;
    }

    public <T extends BaseModel>  UpdateWrapper getUpdateWrapper(T entity,boolean autoFillKeyCondition){
        UpdateWrapper<T> updateWrapper = getCommonWrapper(entity,autoFillKeyCondition);
        try{
            Class clazz = entity.getClass();
            List<Field> fs = Arrays.asList(clazz.getDeclaredFields());
            List<String> updateFieldList = entity.getUpdateFieldList();
            if(updateFieldList!=null){
                for (String updateFiled:updateFieldList ) {
                    for (Field field:fs) {
                        field.setAccessible(true);
                        if(field.getName().toLowerCase().equals(updateFiled)){
                            Object fieldValue = field.get(entity);
                            updateWrapper.set(fieldValue==null,field.getName(),null);
                            continue;
                        }
                    }
                }
            }
            return updateWrapper;
        }catch (Exception e){
            e.printStackTrace();
        }
        return updateWrapper;
    }

    public <T extends BaseModel>  UpdateWrapper getUpdateWrapperFillKey(T entity){
        return getUpdateWrapper(entity,true);
    }

    public <T extends BaseModel>  UpdateWrapper getUpdateWrapperCustomKey(T entity,String fieldName,Object fieldValue){
        UpdateWrapper wrapper =  getUpdateWrapper(entity,false);
        wrapper.eq(fieldName,fieldValue);
        return wrapper;
    }

    public UpdateWrapper getDeleteWrapper(){
        return getCommonWrapper(null,false);
    }

    public  UpdateWrapper getDeleteWrapperFillKey(Class clazz,String unitguid){
        UpdateWrapper wrapper =  getCommonWrapper(null,false);
        try{
            List<Field> fs = Arrays.asList(clazz.getDeclaredFields());
            for (Field field:fs) {
                field.setAccessible(true);
                //TableId修饰主键
                TableId tableId = field.getAnnotation(TableId.class);
                if(null!=tableId){
                    wrapper.eq(tableId.value(),unitguid);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return wrapper;
    }

    public  UpdateWrapper getDeleteWrapperCustomKey(String fieldName,Object fieldValue){
        UpdateWrapper wrapper =  getCommonWrapper(null,false);
        wrapper.eq(fieldName,fieldValue);
        return wrapper;
    }
}
