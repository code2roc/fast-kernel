package com.code2roc.fastboot.framework.message;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.code2roc.fastboot.framework.cache.CacheUtil;
import com.code2roc.fastboot.framework.cache.GlobalCacheInfo;
import com.code2roc.fastboot.framework.util.ConvertOp;
import com.code2roc.fastboot.framework.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Method;
import java.util.HashMap;

@Service
public class GlobalCacheMessageReceiver implements IMessageReceiver {
    @Autowired
    private CacheUtil cacheUtil;

    @Override
    public void handleMessage(Object bodyObject, HashMap extraParam) {
        GlobalCacheInfo globalCacheInfo = (GlobalCacheInfo) bodyObject;
        try {
            String methodName = globalCacheInfo.getMethodName();

            Method method = CacheUtil.class.getMethod(methodName,globalCacheInfo.getParamClassList());
            for (int i = 0; i < globalCacheInfo.getParamClassList().length; i++) {
                Class clazz = globalCacheInfo.getParamClassList()[i];
                if (clazz == String.class) {
                    globalCacheInfo.getParamList()[i] = ConvertOp.convert2String(globalCacheInfo.getParamList()[i]);
                }
                else if (clazz == Boolean.class) {
                    globalCacheInfo.getParamList()[i] = ConvertOp.convert2Boolean(globalCacheInfo.getParamList()[i]);
                } else {
                    if (methodName.equals("putGlobalCache")) {
                        if(!StringUtil.isEmpty(globalCacheInfo.getClassName())){
                            JSONObject content = (JSONObject)globalCacheInfo.getParamList()[i];
                            globalCacheInfo.getParamList()[i] = JSON.parseObject(content.toJSONString(), Class.forName(globalCacheInfo.getClassName()));
                        }
                    } else {
                        JSONObject content = (JSONObject)globalCacheInfo.getParamList()[i];
                        globalCacheInfo.getParamList()[i] = JSON.parseObject(content.toJSONString(), clazz);
                    }
                }
            }

            method.invoke(cacheUtil, globalCacheInfo.getParamList());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
