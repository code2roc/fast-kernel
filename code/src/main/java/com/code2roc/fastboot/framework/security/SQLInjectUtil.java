package com.code2roc.fastboot.framework.security;

import com.code2roc.fastboot.framework.util.CommonUtil;
import com.code2roc.fastboot.framework.util.ConvertOp;
import com.code2roc.fastboot.framework.util.StringUtil;

import java.util.Arrays;
import java.util.List;

public class SQLInjectUtil {
    public static String keyWord = "select|update|delete|insert|truncate|declare|cast|xp_cmdshell|count|char|length|sleep|master|mid|and|or";

    public static boolean checkSQLInject(String value) {
        boolean flag = false;
        value = ConvertOp.convert2String(value).toLowerCase().trim();
        if (!StringUtil.isEmpty(value) && !StringUtil.checkIsOnlyContainCharAndNum(value)) {
            List<String> keyWordList = Arrays.asList(keyWord.split("\\|"));
            for (String ss : keyWordList) {
                if (value.contains(ss)) {
                    if (StringUtil.checkFlowChar(value, ss, " ", true) ||
                            StringUtil.checkFlowChar(value, ss, "(", true) ||
                            StringUtil.checkFlowChar(value, ss, CommonUtil.getNewLine(), true)) {
                        flag = true;
                        break;
                    }
                }
            }
        }
        return flag;
    }
}
