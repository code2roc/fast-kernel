package com.code2roc.fastboot.framework.tree;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TreeNode {
    /**
     * 节点ID
     */
    private String id;
    /**
     * 显示节点文本
     */
    private String label;
    /**
     * 父ID
     */
    private String parentId;
    /**
     * 节点属性
     */
    private Map<String, Object> attributes;
    /**
     * 是否有父节点
     */
    private boolean hasParent;
    /**
     * 是否有子节点
     */
    private boolean hasChildren;
    /**
     * 是否选中
     */
    private boolean checked;
    /**
     * 是否顶级节点
     */
    private boolean topNode;
    /**
     * 是否能够选择节点（单选/复选）
     */
    private boolean canSelect;
    /**
     * 节点顺序
     */
    private Integer sotNum = 0;

    private List<TreeNode> children;

    public TreeNode(){
        attributes = new HashMap<String, Object>();
        children = new ArrayList<>();
        hasParent = false;
        hasChildren = false;
        checked = false;
        topNode = false;
        canSelect = true;
    }

    public void addAttribute(String key,Object value){
        if(attributes.containsKey(key)){
            attributes.replace(key,value);
        }else{
            attributes.put(key,value);
        }
    }

    public void addChildNode(TreeNode node){
        children.add(node);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public boolean isHasParent() {
        return hasParent;
    }

    public void setHasParent(boolean hasParent) {
        this.hasParent = hasParent;
    }

    public boolean isHasChildren() {
        return hasChildren;
    }

    public void setHasChildren(boolean hasChildren) {
        this.hasChildren = hasChildren;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public boolean isTopNode() {
        return topNode;
    }

    public void setTopNode(boolean topNode) {
        this.topNode = topNode;
    }

    public Integer getSotNum() {
        return sotNum;
    }

    public void setSotNum(Integer sotNum) {
        this.sotNum = sotNum;
    }

    public Map<String, Object> getAttributes() {
        return attributes;
    }

    public void setAttributes(Map<String, Object> attributes) {
        this.attributes = attributes;
    }

    public List<TreeNode> getChildren() {
        return children;
    }

    public void setChildren(List<TreeNode> children) {
        this.children = children;
    }

    public boolean isCanSelect() {
        return canSelect;
    }

    public void setCanSelect(boolean canSelect) {
        this.canSelect = canSelect;
    }
}
