package com.code2roc.fastboot.framework.cancel;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Inherited
public @interface HandleCancel {
}
