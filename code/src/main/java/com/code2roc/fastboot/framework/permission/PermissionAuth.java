package com.code2roc.fastboot.framework.permission;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE, ElementType.PARAMETER})
public @interface PermissionAuth {
    Class<? extends  IPermissionAuth> policy() default IPermissionAuth.class;
}
