package com.code2roc.fastboot.framework.file;

public class LocalFileStoargeProperty extends FileStoargeProperty {
    private String localFilePath;

    public String getLocalFilePath() {
        return localFilePath;
    }

    public void setLocalFilePath(String localFilePath) {
        this.localFilePath = localFilePath;
    }

    public LocalFileStoargeProperty(){

    }

    public LocalFileStoargeProperty(String localFilePath){
        this.localFilePath = localFilePath;
    }
}
