package com.code2roc.fastboot.framework.systemload;

public interface ISystemStartExecute {
    void execute();
}
