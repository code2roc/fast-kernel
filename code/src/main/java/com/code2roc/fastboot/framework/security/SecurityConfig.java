package com.code2roc.fastboot.framework.security;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@ConfigurationProperties(prefix = "security")
public class SecurityConfig {
    private List<String> allowedOrigin;
    private List<String> allowAnonymousAccess;
    private boolean openXSS;
    private boolean openSQLInject;
    private int validateCodeExpire;
    private boolean openTokenAuth;
    private int tokenExpire;
    private boolean limitHTTPMethod;
    private boolean openSSL;
    private String sslStore;
    private String sslStorePassword;
    private String sslStoreType;
    private String sslalias;
    private int sslNeedRedirectPort;
    private String basicUserName;
    private String basicPassword;

    public SecurityConfig(){
        allowedOrigin = new ArrayList<>();
        allowAnonymousAccess = new ArrayList<>();
        openXSS = false;
        openSQLInject = false;
        validateCodeExpire = 60;
        openTokenAuth = true;
        tokenExpire = 180;
        limitHTTPMethod = false;
        openSSL = false;
        sslStore = "classpath:systemfile/frame.jks";
        sslStorePassword = "abcd@1234";
        sslStoreType = "JKS";
        sslalias = "tomcat";
        sslNeedRedirectPort = 0;
        basicUserName = "admin";
        basicPassword = "11111";
    }

    public List<String> getAllowedOrigin() {
        return allowedOrigin;
    }

    public void setAllowedOrigin(List<String> allowedOrigin) {
        this.allowedOrigin = allowedOrigin;
    }

    public List<String> getAllowAnonymousAccess() {
        return allowAnonymousAccess;
    }

    public void setAllowAnonymousAccess(List<String> allowAnonymousAccess) {
        this.allowAnonymousAccess = allowAnonymousAccess;
    }

    public boolean isOpenXSS() {
        return openXSS;
    }

    public void setOpenXSS(boolean openXSS) {
        this.openXSS = openXSS;
    }

    public boolean isOpenSQLInject() {
        return openSQLInject;
    }

    public void setOpenSQLInject(boolean openSQLInject) {
        this.openSQLInject = openSQLInject;
    }

    public int getValidateCodeExpire() {
        return validateCodeExpire;
    }

    public void setValidateCodeExpire(int validateCodeExpire) {
        this.validateCodeExpire = validateCodeExpire;
    }

    public int getTokenExpire() {
        return tokenExpire;
    }

    public void setTokenExpire(int tokenExpire) {
        this.tokenExpire = tokenExpire;
    }

    public boolean isOpenTokenAuth() {
        return openTokenAuth;
    }

    public void setOpenTokenAuth(boolean openTokenAuth) {
        this.openTokenAuth = openTokenAuth;
    }

    public boolean isOpenSSL() {
        return openSSL;
    }

    public void setOpenSSL(boolean openSSL) {
        this.openSSL = openSSL;
    }

    public String getSslStore() {
        return sslStore;
    }

    public void setSslStore(String sslStore) {
        this.sslStore = sslStore;
    }

    public String getSslStorePassword() {
        return sslStorePassword;
    }

    public void setSslStorePassword(String sslStorePassword) {
        this.sslStorePassword = sslStorePassword;
    }

    public String getSslStoreType() {
        return sslStoreType;
    }

    public void setSslStoreType(String sslStoreType) {
        this.sslStoreType = sslStoreType;
    }

    public String getSslalias() {
        return sslalias;
    }

    public void setSslalias(String sslalias) {
        this.sslalias = sslalias;
    }

    public boolean isLimitHTTPMethod() {
        return limitHTTPMethod;
    }

    public void setLimitHTTPMethod(boolean limitHTTPMethod) {
        this.limitHTTPMethod = limitHTTPMethod;
    }

    public int getSslNeedRedirectPort() {
        return sslNeedRedirectPort;
    }

    public void setSslNeedRedirectPort(int sslNeedRedirectPort) {
        this.sslNeedRedirectPort = sslNeedRedirectPort;
    }

    public String getBasicUserName() {
        return basicUserName;
    }

    public void setBasicUserName(String basicUserName) {
        this.basicUserName = basicUserName;
    }

    public String getBasicPassword() {
        return basicPassword;
    }

    public void setBasicPassword(String basicPassword) {
        this.basicPassword = basicPassword;
    }
}
