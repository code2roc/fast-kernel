package com.code2roc.fastboot.framework.sqllog;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE, ElementType.PARAMETER})
@Inherited
public @interface IgnoreSqlLog {

}
