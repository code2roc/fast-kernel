package com.code2roc.fastboot.framework.cache;

public interface ICacheInitHandler {
    String getCacheName();

    void initCache();

    void initKeyCache(String key);

    String getHeap();

    String getOffheap();

    String getDisk();
}
