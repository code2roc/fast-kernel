package com.code2roc.fastboot.framework.datadic;

import com.code2roc.fastboot.framework.auth.AnonymousAccess;
import com.code2roc.fastboot.framework.model.Result;
import com.code2roc.fastboot.framework.template.BaseController;
import com.code2roc.fastboot.framework.util.ConvertOp;
import com.code2roc.fastboot.framework.util.StringUtil;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/frame/enumcode")
public class EnumCodeController extends BaseController {

    @ResponseBody
    @PostMapping("/getCodeList")
    public Object getCodeList(@RequestBody Map<String, Object> params){
        String codeName = ConvertOp.convert2String(params.get("codeName"));
        int pageIndex = ConvertOp.convert2Int(params.get("pageIndex"));
        int pageSize = ConvertOp.convert2Int(params.get("pageSize"));
        List<CodeMain> codeMains = DataDicManage.getCodeMainList();
        if(!StringUtil.isEmpty(codeName)){
            codeMains = codeMains.stream().filter(a->a.getCodeName().contains(codeName)).collect(Collectors.toList());
        }
        int total = codeMains.size();
        List<CodeMain> result = new ArrayList<>();
        int currIdx = (pageIndex > 1 ? (pageIndex - 1) * pageSize : 0);
        for (int i = 0; i < pageSize && i < codeMains.size() - currIdx; i++) {
            CodeMain data = codeMains.get(currIdx + i);
            result.add(data);
        }
        return Result.okResult().add("rows", result).add("total", total);
    }

    @ResponseBody
    @PostMapping("/getCodeItemList")
    public Object getCodeItemList(@RequestBody Map<String, Object> params){
        String codeName = ConvertOp.convert2String(params.get("codeName"));
        String itemName = ConvertOp.convert2String(params.get("itemName"));
        String itemValue = ConvertOp.convert2String(params.get("itemValue"));
        int pageIndex = ConvertOp.convert2Int(params.get("pageIndex"));
        int pageSize = ConvertOp.convert2Int(params.get("pageSize"));
        List<CodeMain> codeMains = DataDicManage.getCodeMainList();
        if(!StringUtil.isEmpty(codeName)){
            codeMains = codeMains.stream().filter(a->a.getCodeName().equals(codeName)).collect(Collectors.toList());
        }
        if(codeMains.size()>0){
            CodeMain codeMain   = codeMains.get(0);
            List<CodeItem> codeItemList = codeMain.getCodeItemList();
            if(!StringUtil.isEmpty(itemName)){
                codeItemList = codeItemList.stream().filter(a->a.get_itemText().contains(itemName)).collect(Collectors.toList());
            }
            if(!StringUtil.isEmpty(itemValue)){
                codeItemList = codeItemList.stream().filter(a->a.get_itemValue().toString().contains(itemValue)).collect(Collectors.toList());
            }
            int total = codeItemList.size();
            List<CodeItem> result = new ArrayList<>();
            int currIdx = (pageIndex > 1 ? (pageIndex - 1) * pageSize : 0);
            for (int i = 0; i < pageSize && i < codeItemList.size() - currIdx; i++) {
                CodeItem data = codeItemList.get(currIdx + i);
                result.add(data);
            }
            return Result.okResult().add("rows", result).add("total", total);
        }else{
            return Result.errorResult().setMsg("代码项不存在");
        }
    }

    @ResponseBody
    @PostMapping("/getCodeDetail")
    @AnonymousAccess
    public Object getCodeDetail(@RequestBody Map<String, Object> params){
        String codeName = ConvertOp.convert2String(params.get("codeName"));
        if(StringUtil.isEmpty(codeName)){
            return Result.errorResult().add("msg","代码项名称不能为空");
        }
        List<CodeItem> codeItemList = DataDicManage.getCodeList(codeName);
        if(null==codeItemList){
            return Result.errorResult().add("msg","代码项不存在");
        }else{
            return Result.okResult().add("obj",codeItemList);
        }

    }

}
