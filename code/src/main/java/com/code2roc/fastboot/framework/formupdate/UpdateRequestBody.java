package com.code2roc.fastboot.framework.formupdate;

import java.lang.annotation.*;

@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface UpdateRequestBody {
}

