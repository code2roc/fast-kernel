package com.code2roc.fastboot.framework.cancel;

import com.code2roc.fastboot.framework.auth.TokenModel;
import com.code2roc.fastboot.framework.auth.TokenUtil;
import com.code2roc.fastboot.framework.model.Result;
import com.code2roc.fastboot.framework.template.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/frame/handlecancel")
public class HandleCancelController extends BaseController {
    @Autowired
    private TokenUtil tokenUtil;

    @PostMapping("/killUserHandleThread")
    @ResponseBody
    public Object killUserHandleThread(@RequestBody Map<String, Object> params) {
        Result result = Result.okResult();
        TokenModel userModel = tokenUtil.getTokenModel();
        List<String> threadNameList = userModel.getThreadList();

        ThreadGroup currentGroup = Thread.currentThread().getThreadGroup();
        int noThreads = currentGroup.activeCount();
        Thread[] lstThreads = new Thread[noThreads];
        currentGroup.enumerate(lstThreads);

        for (int i = 0; i < noThreads; i++) {
            String threadName = lstThreads[i].getName();
            if (threadNameList.contains(threadName)) {
                System.out.println("中断线程：" + threadName);
                lstThreads[i].interrupt();
                userModel.removeThread(threadName);
                tokenUtil.setTokenModel(tokenUtil.getTokenString(),userModel);
            }
        }
        return result;
    }
}
