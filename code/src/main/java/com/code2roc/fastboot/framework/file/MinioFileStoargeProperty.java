package com.code2roc.fastboot.framework.file;

public class MinioFileStoargeProperty extends FileStoargeProperty {
    private String endpoint;
    private String accessKey;
    private String secrectKey;

    public MinioFileStoargeProperty() {

    }

    public MinioFileStoargeProperty(String endpoint, String accessKey, String secrectKey) {
        this.endpoint = endpoint;
        this.accessKey = accessKey;
        this.secrectKey = secrectKey;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getSecrectKey() {
        return secrectKey;
    }

    public void setSecrectKey(String secrectKey) {
        this.secrectKey = secrectKey;
    }
}
