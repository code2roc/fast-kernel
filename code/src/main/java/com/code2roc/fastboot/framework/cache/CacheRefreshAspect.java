package com.code2roc.fastboot.framework.cache;

import com.code2roc.fastboot.framework.database.TransactionCallBackUtil;
import com.code2roc.fastboot.framework.util.StringUtil;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

@Aspect
@Component
@Order(value = 85)
public class CacheRefreshAspect {
    @Autowired
    private CacheUtil cacheUtil;
    @Autowired
    private TransactionCallBackUtil transactionCallBackUtil;

    /**
     * 切面点 指定注解
     */
    @Pointcut("@annotation(com.code2roc.fastboot.framework.cache.AutoRefreshCache) " +
            "|| @within(com.code2roc.fastboot.framework.cache.AutoRefreshCache)")
    public void cacheRefreshAspect() {

    }

    /**
     * 拦截方法指定为 repeatSubmitAspect
     */
    @Around("cacheRefreshAspect()")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        MethodSignature signature = (MethodSignature) point.getSignature();
        Method method = signature.getMethod();
        AutoRefreshCache autoRefreshCache = method.getAnnotation(AutoRefreshCache.class);
        if (autoRefreshCache != null && !StringUtil.isEmpty(autoRefreshCache.value())) {
            Object obj = point.proceed();
            transactionCallBackUtil.execute(() -> {
                cacheUtil.refreshCache(autoRefreshCache.value());
            });
            return obj;
        } else {
            return point.proceed();
        }
    }
}
