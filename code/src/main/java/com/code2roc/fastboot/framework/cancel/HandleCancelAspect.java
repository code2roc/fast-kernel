package com.code2roc.fastboot.framework.cancel;

import com.code2roc.fastboot.framework.auth.TokenModel;
import com.code2roc.fastboot.framework.auth.TokenUtil;
import com.code2roc.fastboot.framework.cache.CacheTransactional;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

@Aspect
@Component
@Order(value = 102)
public class HandleCancelAspect {
    @Autowired
    private TokenUtil tokenUtil;

    /**
     * 切面点 指定注解
     */
    @Pointcut("@annotation(com.code2roc.fastboot.framework.cancel.HandleCancel) " +
            "|| @within(com.code2roc.fastboot.framework.cancel.HandleCancel)")
    public void handleCancelAspect() {

    }

    @Around("handleCancelAspect()")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        MethodSignature signature = (MethodSignature) point.getSignature();
        Method method = signature.getMethod();
        HandleCancel handleCancel = method.getAnnotation(HandleCancel.class);
        if (handleCancel != null) {
            Object result = null;
            try{
                TokenModel userModel = tokenUtil.getTokenModel();
                userModel.addThread(Thread.currentThread().getName());
                tokenUtil.setTokenModel(tokenUtil.getTokenString(),userModel);
                result = point.proceed();
            }finally {
                TokenModel userModel = tokenUtil.getTokenModel();
                userModel.removeThread(Thread.currentThread().getName());
                tokenUtil.setTokenModel(tokenUtil.getTokenString(),userModel);
            }
            return result;
        } else {
            return point.proceed();
        }
    }
}
