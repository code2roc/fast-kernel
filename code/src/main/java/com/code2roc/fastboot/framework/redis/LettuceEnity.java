package com.code2roc.fastboot.framework.redis;

import org.springframework.boot.autoconfigure.data.redis.RedisProperties;

import java.time.Duration;

public class LettuceEnity extends RedisProperties.Lettuce {
    private PoolEntity poolEntity;

    public LettuceEnity(){
        poolEntity = new PoolEntity();
        setShutdownTimeout(Duration.ofMillis(100));
    }

    public PoolEntity getPoolEntity() {
        return poolEntity;
    }

    public void setPoolEntity(PoolEntity poolEntity) {
        this.poolEntity = poolEntity;
    }
}
