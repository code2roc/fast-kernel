function OpenDialog(title, url, width, height, callback, params) {
    layui.use('layer', function() {
        var $ = layui.jquery,
            layer = top.layui.layer;
        width = width + "px"
        height = height + "px";
        if (params) {
            top.window["layer_params"] = params;
        }
        layer.open({
            type: 2,
            title: title,
            area: [width, height],
            shade: 0.3,
            maxmin: false,
            content: GetRootPath() + url,
            zIndex: layer.zIndex,
            success: function(layero, index) {

            },
            end: function() {
                if (callback) {
                    var layer_return = top.window["layer_return"];
                    if (layer_return) {
                        callback(layer_return);
                    } else {
                        callback();
                    }
                }
                top.window["layer_return"] = null;
                top.window["layer_params"] = null;
            }
        });

    })
}

function CloseDialog(ReturnValue) {
    layui.use('layer', function() {
        if (ReturnValue) {
            top.window["layer_return"] = ReturnValue;
        }
        var $ = layui.jquery,
            layer = layui.layer;
        //当你在iframe页面关闭自身时
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    })
}

function GetParam() {
    if (top.window["layer_params"]) {
        var layer_params = top.window["layer_params"];
        return layer_params;
    }
}

function OpenAlert(title, callback) {
    layui.use('layer', function() {
        layer.alert(title, function(index) {
            if (callback) {
                callback();
            }
            layer.close(index);
        });
    })
}

function OpenSuccess(title, callback) {
    layui.use('layer', function() {
        layer.alert(title, { icon: 1 }, function(index) {
            if (callback) {
                callback();
            }
            layer.close(index);
        });
    })
}

function OpenWarning(title, callback) {
    layui.use('layer', function() {
        layer.alert(title, { icon: 7 }, function(index) {
            if (callback) {
                callback();
            }
            layer.close(index);
        });
    })
}

function OpenFail(title, callback) {
    layui.use('layer', function() {
        layer.alert(title, { icon: 2 }, function(index) {
            if (callback) {
                callback();
            }
            layer.close(index);
        });
    })
}

function OpenSuccessMessage(title) {
    layer.msg(title, { icon: 1 });
}

function OpenWarningMessage(title) {
    layer.msg(title, { icon: 7 });
}

function OpenFailMessage(title) {
    layer.msg(title, { icon: 2 });
}


function OpenConfirm(title, yes, cancel) {
    layui.use('layer', function() {
        layer.confirm(title, { icon: 3, title: '提示' }, function(index) {
            //do something
            if (yes) {
                yes();
            }
            layer.close(index);
        }, function(index) {
            if (cancel) {
                cancel();
            }
            layer.close(index);
        });
    })

}

function UseDialog() {
    layui.use('layer', function() {})
}

function StartLoading(title) {
    layer.load();
}

function CloseLoading() {
    layer.closeAll('loading');
}
