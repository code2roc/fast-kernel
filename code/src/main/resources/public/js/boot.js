var nowdate = new Date();
var version = ConvertDateToString(nowdate);

function LoadJS(url, isappendversion) {
    if (isappendversion) {
        url += "?version=" + version;
    }
    document.write('<script src= ' + url + '></script>');
}

function GetRootPath() {
    var loc = window.location,
        host = loc.hostname,
        protocol = loc.protocol,
        port = loc.port ? (':' + loc.port) : '';
    var path = location.pathname;

    if (path.indexOf('/') === 0) {
        path = path.substring(1);
    }

    var mypath = '/' + path.split('/')[0];
    path = (mypath != undefined ? mypath : ('/' + loc.pathname.split('/')[1])) + '/';

    var rootPath = protocol + '//' + host + port + path+"/";
    return rootPath;
}

function GetApiPath() {
    var loc = window.location,
        host = loc.hostname,
        protocol = loc.protocol,
        port = loc.port ? (':' + loc.port) : '';
    var path = location.pathname;

    if (path.indexOf('/') === 0) {
        path = path.substring(1);
    }

    var mypath = '/' + path.split('/')[0];
    path = (mypath != undefined ? mypath : ('/' + loc.pathname.split('/')[1])) + '/';

    var rootPath = protocol + '//' + host + port + "/";
    return rootPath;
}

function ConvertDateToString(now) {
    var year = now.getFullYear();
    var month = (now.getMonth() + 1).toString();
    var day = (now.getDate()).toString();
    var hour = (now.getHours()).toString();
    var minute = (now.getMinutes()).toString();
    var second = (now.getSeconds()).toString();
    if (month.length == 1) {
        month = "0" + month;
    }
    if (day.length == 1) {
        day = "0" + day;
    }
    if (hour.length == 1) {
        hour = "0" + hour;
    }
    if (minute.length == 1) {
        minute = "0" + minute;
    }
    if (second.length == 1) {
        second = "0" + second;
    }
    var dateTime = year + month + day + hour + minute + second;
    return dateTime;
}

LoadJS(GetRootPath() + 'plugins/jquery/jquery-1.10.2.min.js', false);
LoadJS(GetRootPath() + 'plugins/jquery/jquery.cookie.js', false);
LoadJS(GetRootPath() + 'js/config.js', true);
LoadJS(GetRootPath() + 'js/staticFileLoad.js', true);