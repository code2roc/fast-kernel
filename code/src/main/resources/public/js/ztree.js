var TreeDic = new Map();

function BindTree(treeid, url, params, treeNodeClick) {
    var defaultparams;
    var defaultsetting = {
        data: {
            simpleData: {
                enable: true, //true 、 false 分别表示 使用 、 不使用 简单数据模式 
                idKey: "id", //节点数据中保存唯一标识的属性名称
                pIdKey: "pId", //节点数据中保存其父节点唯一标识的属性名称 
                rootPId: -null //用于修正根节点父节点数据，即 pIdKey 指定的属性值
            },
            key: {
                name: "name" //zTree 节点数据保存节点名称的属性名称  默认值："name"
            },
        },
        callback: {
            onClick: treeNodeClick
        }
    };
    if (params && params != null) {
        defaultparams = params
    } else {
        defaultparams = {}
    }

    HttpPost(JSON.stringify(defaultparams), url, function(result, status) {
        var nodes = result.data.obj;
        var zTreeObj = $.fn.zTree.init($("#" + treeid), defaultsetting, nodes);
        TreeDic.set(treeid, zTreeObj);
    })
}

function BindTreeV2(treeid, url, params, requestCallBack, treeNodeClick, setting) {
    var defaultparams;
    var defaultsetting = {
        data: {
            simpleData: {
                enable: true, //true 、 false 分别表示 使用 、 不使用 简单数据模式 
                idKey: "id", //节点数据中保存唯一标识的属性名称
                pIdKey: "pId", //节点数据中保存其父节点唯一标识的属性名称 
                rootPId: -null //用于修正根节点父节点数据，即 pIdKey 指定的属性值
            },
            key: {
                name: "name" //zTree 节点数据保存节点名称的属性名称  默认值："name"
            },
        },
        callback: {
            onClick: treeNodeClick
        }
    };
    if (params && params != null) {
        defaultparams = params
    } else {
        defaultparams = {}
    }
    if (setting) {
        for (var key in setting) {
            defaultsetting[key] = setting[key];
        }
    }
    HttpPost(JSON.stringify(defaultparams), url, function(result, status) {
        var nodes = result.data.obj;
        var zTreeObj = $.fn.zTree.init($("#" + treeid), defaultsetting, nodes);
        TreeDic.set(treeid, zTreeObj);
        if (requestCallBack) {
            requestCallBack(result, status);
        }
    })
}

function GetTreeCheckedNodes(treeid) {
    var zTreeObj = TreeDic.get(treeid);
    return zTreeObj.getCheckedNodes(true)
}


function SetCheckedNodes(treeid, nodeIDArray) {
    var zTreeObj = TreeDic.get(treeid);
    for (var i = 0; i < nodeIDArray.length; i++) {
        var node = zTreeObj.getNodeByParam("id", nodeIDArray[i]);
        zTreeObj.selectNode(node);
        zTreeObj.checkNode(node, true, true);
        zTreeObj.updateNode(node);
    }
}