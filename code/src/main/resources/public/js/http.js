function GetHeadInfo() {
    var headinfo = {
        "Content-Type": "application/json;charset=uft-8",
        "token": GetTokenID()
    };
    return headinfo;
}

function GetFormHeadInfo() {
    var headinfo = {
        "token": GetTokenID()
    };
    return headinfo;
}

function HttpPost(JsonData, Url, SucessFunctionName) {
    if (Url.indexOf("?") < 0)
        Url = Url + "?t=" + Math.random();
    else
        Url = Url + "&t=" + Math.random();

    $.ajax({
        url: GetApiPath() + Url,
        type: "POST",
        headers: GetHeadInfo(),
        data: JsonData,
        dataType: 'json',
        complete: function() {},
        success: SucessFunctionName,
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            //var strLog = "详细错误：" + XMLHttpRequest.responseText;                //writeFile("c:\\qd.log", strLog);
            ///alert(errorThrown + ";详细错误：" + XMLHttpRequest.responseText);
        }
    });
}

function HttpPost_Sync(JsonData, Url, SucessFunctionName) {
    if (Url.indexOf("?") < 0)
        Url = Url + "?t=" + Math.random();
    else
        Url = Url + "&t=" + Math.random();

    $.ajax({
        async: false,
        url: GetApiPath() + Url,
        type: "POST",
        headers: GetHeadInfo(),
        data: JsonData,
        dataType: 'json',
        complete: function() {},
        success: SucessFunctionName,
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            //var strLog = "详细错误：" + XMLHttpRequest.responseText;                //writeFile("c:\\qd.log", strLog);
            ///alert(errorThrown + ";详细错误：" + XMLHttpRequest.responseText);
        }
    });
}

function HttpForm(FormData, Url, SucessFunctionName) {
    if (Url.indexOf("?") < 0)
        Url = Url + "?t=" + Math.random();
    else
        Url = Url + "&t=" + Math.random();

    $.ajax({
        url: GetApiPath() + Url,
        type: "POST",
        headers: GetFormHeadInfo(),
        data: FormData,
        dataType: 'json',
        cache: false, // 不缓存
        processData: false, // jQuery不要去处理发送的数据
        contentType: false, // jQuery不要去设置
        complete: function() {},
        success: SucessFunctionName,
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            //var strLog = "详细错误：" + XMLHttpRequest.responseText;                //writeFile("c:\\qd.log", strLog);
            ///alert(errorThrown + ";详细错误：" + XMLHttpRequest.responseText);
        }
    });
}