package ${packageName}.service;


import ${packageName}.model.${classModelName};
import com.code2roc.fastboot.template.BaseBootService;

public interface I${className}Service extends BaseBootService<${classModelName}> {

}