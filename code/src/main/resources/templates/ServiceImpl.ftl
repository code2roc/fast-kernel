package ${packageName}.service.impl;

import ${packageName}.model.${classModelName};
import ${packageName}.service.I${className}Service;
import com.code2roc.fastboot.template.BaseBootServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class ${className}ServiceImpl  extends BaseBootServiceImpl<${classModelName}> implements I${className}Service {

}