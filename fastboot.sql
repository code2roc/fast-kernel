/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50725
Source Host           : localhost:3306
Source Database       : fastboot

Target Server Type    : MYSQL
Target Server Version : 50725
File Encoding         : 65001

Date: 2023-04-20 09:16:12
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for system_attach
-- ----------------------------
DROP TABLE IF EXISTS `system_attach`;
CREATE TABLE `system_attach` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `row_id` varchar(50) DEFAULT NULL,
  `attach_name` varchar(200) DEFAULT NULL COMMENT '附件名称',
  `attach_type` varchar(50) DEFAULT NULL COMMENT '附件类型',
  `attach_size` int(11) DEFAULT NULL COMMENT '附件大小（字节）',
  `attach_storage_type` int(11) DEFAULT NULL COMMENT '附件存储方式',
  `attach_virtual_path` varchar(500) DEFAULT NULL COMMENT '附件虚拟路径',
  `group_guid` varchar(50) DEFAULT NULL COMMENT '分组guid',
  `group_type` varchar(100) DEFAULT NULL COMMENT '分组类型',
  `attach_status` int(11) DEFAULT NULL COMMENT '附件状态',
  `upload_user_id` varchar(50) DEFAULT NULL COMMENT '上传人id',
  `upload_user_name` varchar(50) DEFAULT NULL COMMENT '上传人姓名',
  `sort_num` int(11) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COMMENT='附件表';

-- ----------------------------
-- Records of system_attach
-- ----------------------------
INSERT INTO `system_attach` VALUES ('27', '202302061343596195d6cb-6c62-438c-b39a-8df5b1bac212', '《手撸 Spring》 • 小傅哥.pdf', 'pdf', '8599754', null, 'default/202302061343596195d6cb-6c62-438c-b39a-8df5b1bac212/《手撸 Spring》 • 小傅哥.pdf', 'xxx', 'default', '10', '20220822112728225efcd4-3912-4f2d-b40b-87565505456c', '系统管理员', null, '2023-02-06 13:44:00', '2023-02-06 13:44:00');

-- ----------------------------
-- Table structure for system_code_item
-- ----------------------------
DROP TABLE IF EXISTS `system_code_item`;
CREATE TABLE `system_code_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `row_id` varchar(50) DEFAULT NULL,
  `code_id` varchar(50) DEFAULT NULL COMMENT '代码项主表id',
  `item_text` varchar(100) DEFAULT NULL COMMENT '代码项文字',
  `item_value` varchar(100) DEFAULT NULL COMMENT '代码项值',
  `sort_num` int(11) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COMMENT='数据字典详情表';

-- ----------------------------
-- Records of system_code_item
-- ----------------------------
INSERT INTO `system_code_item` VALUES ('7', '20220830001911a1ccb990-51dd-40bf-a5fb-7ed00143501e', '2022082923020261334321-2f8f-41a8-a27a-d088c606a321', '男', '10', '2', '2022-09-27 15:32:47', '2022-09-27 15:32:47');
INSERT INTO `system_code_item` VALUES ('8', '20220830001911daed6173-729a-4dba-a4b8-0210c7fdf675', '2022082923020261334321-2f8f-41a8-a27a-d088c606a321', '女', '20', '1', '2022-09-27 15:32:47', '2022-09-27 15:32:47');

-- ----------------------------
-- Table structure for system_code_main
-- ----------------------------
DROP TABLE IF EXISTS `system_code_main`;
CREATE TABLE `system_code_main` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `row_id` varchar(255) DEFAULT NULL,
  `code_name` varchar(100) DEFAULT NULL COMMENT '代码项名称',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `group_id` varchar(50) DEFAULT NULL COMMENT '分组id',
  `system_init` int(11) DEFAULT NULL COMMENT '是否系统内置',
  `sort_num` int(11) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='数据字典定义表';

-- ----------------------------
-- Records of system_code_main
-- ----------------------------
INSERT INTO `system_code_main` VALUES ('2', '2022082923020261334321-2f8f-41a8-a27a-d088c606a321', '性别', '', '202208181753013aa130a9-8c71-4801-841a-167ac80f4c50', '10', '99', '2022-08-29 23:02:03', '2022-09-27 15:32:47');

-- ----------------------------
-- Table structure for system_column
-- ----------------------------
DROP TABLE IF EXISTS `system_column`;
CREATE TABLE `system_column` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `row_id` varchar(50) DEFAULT NULL,
  `table_id` varchar(50) DEFAULT NULL COMMENT '表id',
  `column_name` varchar(100) DEFAULT NULL COMMENT '物理列名',
  `column_description` varchar(100) DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) DEFAULT NULL COMMENT '物理列定义类型',
  `sort_num` int(11) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=318 DEFAULT CHARSET=utf8mb4 COMMENT='生成代码列信息表';

-- ----------------------------
-- Records of system_column
-- ----------------------------
INSERT INTO `system_column` VALUES ('45', '2022121315143070fcaf11-ea48-440a-871a-34e89879a1ba', '202212131514300c0034a8-ebce-44e9-963b-23dc8c75708f', 'account_name', '账户名称', 'varchar(100)', null, '2022-12-13 15:14:31', '2022-12-13 17:14:22');
INSERT INTO `system_column` VALUES ('46', '202212131514300e537fc8-ef06-4dc9-9dce-9fc219b3252d', '202212131514300c0034a8-ebce-44e9-963b-23dc8c75708f', 'account_no', '账户编号', 'varchar(50)', null, '2022-12-13 15:14:31', '2022-12-13 17:14:22');
INSERT INTO `system_column` VALUES ('47', '20221213151431b8dc42c7-0d1b-4e22-a668-c67a29011df4', '202212131514300c0034a8-ebce-44e9-963b-23dc8c75708f', 'account_limit', '账户限额(元）', 'decimal(18,2)', null, '2022-12-13 15:14:31', '2022-12-13 17:14:22');
INSERT INTO `system_column` VALUES ('48', '202212131514319da055f1-b99e-4b2e-b35f-5b544a6b237a', '202212131514300c0034a8-ebce-44e9-963b-23dc8c75708f', 'open_date', '开户日期', 'datetime', null, '2022-12-13 15:14:31', '2022-12-13 17:14:22');
INSERT INTO `system_column` VALUES ('49', '2022121315143195c7e733-6ac7-495d-9bd7-c88d44001d77', '202212131514300c0034a8-ebce-44e9-963b-23dc8c75708f', 'account_type', '账户类型', 'int', null, '2022-12-13 15:14:31', '2022-12-14 10:26:45');
INSERT INTO `system_column` VALUES ('50', '202212131514316d067ce1-2ef9-46fe-a370-43fac1a97c0f', '202212131514300c0034a8-ebce-44e9-963b-23dc8c75708f', 'account_rules', '账户条款', 'longtext', null, '2022-12-13 15:14:31', '2022-12-14 10:26:04');
INSERT INTO `system_column` VALUES ('56', '20221213170919368a2379-6106-496a-9dae-75934f0a2ef3', '202212131514300c0034a8-ebce-44e9-963b-23dc8c75708f', 'account_status', '账户状态', 'int', null, '2022-12-13 17:09:19', '2022-12-14 10:25:40');
INSERT INTO `system_column` VALUES ('57', '2022121317091958a6e339-fe36-47b2-bbf4-a8db867fec99', '202212131514300c0034a8-ebce-44e9-963b-23dc8c75708f', 'account_roles', '账户角色', 'varchar(200)', null, '2022-12-13 17:09:19', '2022-12-14 10:25:18');
INSERT INTO `system_column` VALUES ('58', '2022121317091963297c29-0b2d-45f6-bd7c-c4c5760e2013', '202212131514300c0034a8-ebce-44e9-963b-23dc8c75708f', 'account_user_id', '账户管理员id', 'varchar(50)', null, '2022-12-13 17:09:19', '2022-12-13 17:51:40');
INSERT INTO `system_column` VALUES ('59', '20221213170919e86f6229-dad2-44b1-a96f-9f1e27cf68ca', '202212131514300c0034a8-ebce-44e9-963b-23dc8c75708f', 'account_user_name', '账户管理员name', 'varchar(50)', null, '2022-12-13 17:09:19', '2022-12-13 17:14:22');
INSERT INTO `system_column` VALUES ('60', '202212131709192453854a-f6e7-423d-870e-a453cc45dd43', '202212131514300c0034a8-ebce-44e9-963b-23dc8c75708f', 'manage_userid_list', '管理人员id', 'varchar(500)', null, '2022-12-13 17:09:20', '2022-12-13 17:14:22');
INSERT INTO `system_column` VALUES ('61', '20221213170919c2aa924d-d7ae-417d-912c-28bc8b5378c7', '202212131514300c0034a8-ebce-44e9-963b-23dc8c75708f', 'manage_username_list', '管理人员name', 'varchar(500)', null, '2022-12-13 17:09:20', '2022-12-13 17:14:22');
INSERT INTO `system_column` VALUES ('62', '20221213171422cb3ede76-ab7c-4157-8eca-233a26c004ab', '202212131514300c0034a8-ebce-44e9-963b-23dc8c75708f', 'account_vip', '是否是VIP', 'int', null, '2022-12-13 17:14:22', '2022-12-14 10:27:01');

-- ----------------------------
-- Table structure for system_config
-- ----------------------------
DROP TABLE IF EXISTS `system_config`;
CREATE TABLE `system_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `row_id` varchar(50) DEFAULT NULL,
  `config_name` varchar(100) DEFAULT NULL COMMENT '参数名称',
  `config_value` varchar(500) DEFAULT NULL COMMENT '参数值',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `group_id` varchar(500) DEFAULT NULL COMMENT '分组id',
  `system_init` int(11) DEFAULT NULL COMMENT '是否系统内置',
  `sort_num` int(11) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COMMENT='系统参数表';

-- ----------------------------
-- Records of system_config
-- ----------------------------
INSERT INTO `system_config` VALUES ('9', '20220817180603a94aaf8e-53e1-4ebc-9eb5-62fcf618abc5', 'InitPWD', '88888', '用户初始密码', '20220818175246c053f3b5-5af3-4328-882a-6a8ca3183661', '10', '99', '2022-08-17 18:06:04', '2022-09-02 13:55:31');

-- ----------------------------
-- Table structure for system_dept
-- ----------------------------
DROP TABLE IF EXISTS `system_dept`;
CREATE TABLE `system_dept` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `row_id` varchar(50) DEFAULT NULL,
  `dept_name` varchar(300) DEFAULT NULL COMMENT '部门名称',
  `short_name` varchar(100) DEFAULT NULL COMMENT '部门简称',
  `parent_id` varchar(50) DEFAULT NULL COMMENT '父级部门id',
  `code` varchar(100) DEFAULT NULL COMMENT '部门编码',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `address` varchar(500) DEFAULT NULL COMMENT '地址',
  `zip_code` varchar(50) DEFAULT NULL COMMENT '邮编',
  `office_phone` varchar(50) DEFAULT NULL COMMENT '办公电话',
  `fax` varchar(50) DEFAULT NULL COMMENT '传真',
  `sort_num` int(11) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COMMENT='部门表';

-- ----------------------------
-- Records of system_dept
-- ----------------------------
INSERT INTO `system_dept` VALUES ('3', '20220821221046d4e18514-72fb-49f0-870d-e466034a153f', '系统管理部', '', 'Top', '', '', '', '', '', '', '99', '2022-08-21 22:10:47', '2023-02-02 15:31:55');
INSERT INTO `system_dept` VALUES ('5', '20220902141543328fd0a0-6e95-41c4-87f9-b741ccd3aac4', '财务部', '', 'Top', '', '', '', '', '', '', '0', '2022-09-02 14:15:44', '2022-09-02 14:15:44');
INSERT INTO `system_dept` VALUES ('6', '202209021415545f06599f-44fb-4118-9390-55f3cf25c796', '党建部', '', 'Top', '', '', '', '', '', '', '0', '2022-09-02 14:15:54', '2022-09-02 14:15:54');
INSERT INTO `system_dept` VALUES ('7', '202209021416046dfe1117-cb15-4182-b327-ccf0bad5036b', '开发部', '', 'Top', '', '', '', '', '', '', '0', '2022-09-02 14:16:05', '2022-09-27 10:41:03');
INSERT INTO `system_dept` VALUES ('8', '20220902141611c76e47f8-030a-4877-9b58-6bf06ed9652f', '研发一部', '', '202209021416046dfe1117-cb15-4182-b327-ccf0bad5036b', '', '', '', '', '', '', '0', '2022-09-02 14:16:12', '2022-09-27 10:41:04');
INSERT INTO `system_dept` VALUES ('9', '20220902141617c56fa00e-b82b-4c3f-9432-6075569da81c', '研发二部', '', '202209021416046dfe1117-cb15-4182-b327-ccf0bad5036b', '', '', '', '', '', '', '0', '2022-09-02 14:16:18', '2022-09-27 10:41:04');

-- ----------------------------
-- Table structure for system_example
-- ----------------------------
DROP TABLE IF EXISTS `system_example`;
CREATE TABLE `system_example` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `row_id` varchar(50) DEFAULT NULL,
  `account_name` varchar(100) DEFAULT NULL COMMENT '账户名称',
  `account_no` varchar(50) DEFAULT NULL COMMENT '账户编号',
  `account_limit` decimal(18,2) DEFAULT NULL COMMENT '账户限额(元）',
  `open_date` datetime DEFAULT NULL COMMENT '开户日期',
  `account_type` int(11) DEFAULT NULL COMMENT '账户类型',
  `account_status` int(11) DEFAULT NULL COMMENT '账户状态',
  `account_rules` longtext COMMENT '账户条款',
  `account_vip` int(11) DEFAULT NULL COMMENT '是否是VIP',
  `account_roles` varchar(200) DEFAULT NULL COMMENT '账户角色',
  `account_user_id` varchar(50) DEFAULT NULL COMMENT '账户管理员id',
  `account_user_name` varchar(50) DEFAULT NULL COMMENT '账户管理员name',
  `manage_userid_list` varchar(500) DEFAULT NULL COMMENT '管理人员id',
  `manage_username_list` varchar(500) DEFAULT NULL COMMENT '管理人员name',
  `sort_num` int(11) DEFAULT NULL COMMENT '排序',
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='系统示例表';

-- ----------------------------
-- Records of system_example
-- ----------------------------
INSERT INTO `system_example` VALUES ('1', '20230202150757c6f1bf7d-63e1-4a88-ae42-150378d94912', '江苏银行账户', 'JSBK20230202001', '500000.00', '2023-02-02 00:00:00', '10', '10', '条款12345', '10', null, null, null, null, null, '99', '2023-02-02 15:07:58', '2023-02-02 15:07:58');

-- ----------------------------
-- Table structure for system_group
-- ----------------------------
DROP TABLE IF EXISTS `system_group`;
CREATE TABLE `system_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `row_id` varchar(50) DEFAULT NULL,
  `group_name` varchar(200) DEFAULT NULL COMMENT '分组名称',
  `group_tag` varchar(100) DEFAULT NULL COMMENT '分组标识',
  `parent_id` varchar(50) DEFAULT NULL COMMENT '父级分组id',
  `system_init` int(11) DEFAULT NULL COMMENT '是否系统内置',
  `sort_num` int(11) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COMMENT='分组表';

-- ----------------------------
-- Records of system_group
-- ----------------------------
INSERT INTO `system_group` VALUES ('13', '2022081910124663481f3a-5332-4f17-8fb7-0c70d00b9eec', '参数目录', 'Config', 'Top', '10', '99', '2022-09-27 14:52:40', '2022-09-27 14:52:40');
INSERT INTO `system_group` VALUES ('14', '2022081817503502368d7e-4797-447d-b909-3ce6c6452750', '字典目录', 'DataDic', 'Top', '10', '98', '2022-09-27 14:52:40', '2022-09-27 14:52:40');
INSERT INTO `system_group` VALUES ('15', '202208181750597079356c-dcf2-46fb-be6d-56ae97f6bbb2', '角色目录', 'Role', 'Top', '10', '97', '2022-09-27 14:52:40', '2022-09-27 14:52:40');
INSERT INTO `system_group` VALUES ('16', '20220818175246c053f3b5-5af3-4328-882a-6a8ca3183661', '系统参数', 'SystemConfig', '2022081910124663481f3a-5332-4f17-8fb7-0c70d00b9eec', '10', '0', '2022-09-27 14:52:40', '2022-09-27 14:52:40');
INSERT INTO `system_group` VALUES ('17', '202208181753013aa130a9-8c71-4801-841a-167ac80f4c50', '系统字典', 'SystemDataDic', '2022081817503502368d7e-4797-447d-b909-3ce6c6452750', '10', '0', '2022-09-27 14:52:40', '2022-09-27 14:52:40');
INSERT INTO `system_group` VALUES ('18', '2022081817531661299e29-c996-47a5-98b6-f2d96b46fd80', '系统角色', 'SystemRole', '202208181750597079356c-dcf2-46fb-be6d-56ae97f6bbb2', '10', '0', '2022-09-27 14:52:40', '2022-09-27 14:52:40');

-- ----------------------------
-- Table structure for system_job
-- ----------------------------
DROP TABLE IF EXISTS `system_job`;
CREATE TABLE `system_job` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `row_id` varchar(50) DEFAULT NULL,
  `job_name` varchar(100) DEFAULT NULL COMMENT '任务名称',
  `job_type` int(11) DEFAULT NULL COMMENT '任务类型',
  `job_class_path` varchar(150) DEFAULT NULL COMMENT '执行任务类全路径',
  `execute_peroid` int(11) DEFAULT NULL COMMENT '执行间隔',
  `execute_unit` int(11) DEFAULT NULL COMMENT '执行间隔单位',
  `corn_express` varchar(100) DEFAULT NULL COMMENT 'corn表达式',
  `job_status` int(11) DEFAULT NULL COMMENT '任务状态',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  `sort_num` int(11) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='定时任务表';

-- ----------------------------
-- Records of system_job
-- ----------------------------
INSERT INTO `system_job` VALUES ('1', '20220823175735ee401349-89fd-40e3-9e8a-13fa80e05629', '测试x循环', '10', 'com.code2roc.fastboot.system.job.DemoJob', '1', '10', '', '20', '', '99', '2022-08-23 17:57:35', '2022-08-24 09:52:34');

-- ----------------------------
-- Table structure for system_menu
-- ----------------------------
DROP TABLE IF EXISTS `system_menu`;
CREATE TABLE `system_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `row_id` varchar(50) DEFAULT NULL,
  `menu_name` varchar(200) DEFAULT NULL COMMENT '菜单名称',
  `menu_code` varchar(100) DEFAULT NULL COMMENT '菜单编码',
  `url` varchar(200) DEFAULT NULL COMMENT '菜单地址',
  `icon` varchar(100) DEFAULT NULL COMMENT '菜单图标',
  `public_status` int(11) DEFAULT NULL COMMENT '是否公开',
  `open_mode` int(11) DEFAULT NULL COMMENT '打开方式',
  `menu_status` int(11) DEFAULT NULL COMMENT '菜单状态',
  `sort_num` int(11) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COMMENT='菜单表';

-- ----------------------------
-- Records of system_menu
-- ----------------------------
INSERT INTO `system_menu` VALUES ('23', '20220830110024bb171c3b-a741-4f4f-ae79-a9bdf2e655c1', '后台管理', '0099', '', 'layui-icon-set', '20', '10', '10', '99', '2022-09-27 14:45:07', '2022-09-27 14:45:07');
INSERT INTO `system_menu` VALUES ('24', '202208301102566db147f7-997e-4b03-882d-9d6ee52c6ae0', '组织架构', '00990001', '', '', '20', '10', '10', '9', '2022-09-27 14:45:07', '2022-09-27 14:45:07');
INSERT INTO `system_menu` VALUES ('25', '202209081039326760129b-ea1e-4a91-ad7f-ad0f388422ba', '代码生成', '009900040001', 'system/codegen/table_list.html', '', '20', '10', '10', '9', '2022-09-27 14:45:07', '2022-10-10 16:12:18');
INSERT INTO `system_menu` VALUES ('26', '20220908103910297a1f8a-91ab-4e19-b19a-b3cf96289b38', '定时任务', '009900030001', 'system/job/job_list.html', '', '20', '10', '10', '9', '2022-09-27 14:45:07', '2022-09-27 14:45:07');
INSERT INTO `system_menu` VALUES ('27', '20220908103754390ea1b7-17b4-4652-9557-e1a00b26ebf3', '菜单配置', '009900020001', 'system/menu/menu_list.html', '', '20', '10', '10', '9', '2022-09-27 14:45:07', '2022-09-27 14:45:07');
INSERT INTO `system_menu` VALUES ('28', '202209081036396d4f2b47-3a17-4b80-9acf-e216f974f13b', '部门管理', '009900010001', 'system/dept/dept_list.html', '', '20', '10', '10', '9', '2022-09-27 14:45:07', '2022-09-27 14:45:07');
INSERT INTO `system_menu` VALUES ('29', '202209081036539f5c0c54-4361-4c26-8971-3ce7f4a5a5c0', '人员管理', '009900010002', 'system/user/user_list.html', '', '20', '10', '10', '8', '2022-09-27 14:45:07', '2022-09-27 14:45:07');
INSERT INTO `system_menu` VALUES ('30', '20220908103806c8e85e21-2e48-4a9b-aa42-61b397033cc3', '菜单权限', '009900020002', 'system/menu/menu_setting_list.html', '', '20', '10', '10', '8', '2022-09-27 14:45:07', '2022-09-27 14:45:07');
INSERT INTO `system_menu` VALUES ('31', '20220908103922c0023e08-6cd0-4960-b81c-91cf0fca842d', '附件管理', '009900030002', 'system/attach/attach_list.html', '', '20', '10', '10', '8', '2022-09-27 14:45:07', '2022-09-27 14:45:07');
INSERT INTO `system_menu` VALUES ('32', '202208301103236d8c56aa-7c13-4c97-b91c-0bc1b53b10bb', '系统配置', '00990002', '', '', '20', '10', '10', '8', '2022-09-27 14:45:07', '2022-09-27 14:45:07');
INSERT INTO `system_menu` VALUES ('33', '20220908103707a597426e-4d29-479a-82d5-81484db7201f', '角色管理', '009900010003', 'system/role/role_list.html', '', '20', '10', '10', '7', '2022-09-27 14:45:07', '2022-09-27 14:45:07');
INSERT INTO `system_menu` VALUES ('34', '202209081038203495d196-b49e-411e-b6c5-ffe951f0c4db', '目录管理', '009900020003', 'system/group/group_list.html', '', '20', '10', '10', '7', '2022-09-27 14:45:07', '2022-09-27 14:45:07');
INSERT INTO `system_menu` VALUES ('35', '20220830110332e103b369-5fe0-4f88-84fa-c8b6e9ad2922', '系统监控', '00990003', '', '', '20', '10', '10', '7', '2022-09-27 14:45:07', '2022-09-27 14:45:07');
INSERT INTO `system_menu` VALUES ('36', '20220908103722c47f30a5-4b5a-4039-9b4b-742ba8abc82d', '角色关系', '009900010004', 'system/role/user_role_list.html', '', '20', '10', '10', '6', '2022-09-27 14:45:08', '2022-09-27 14:45:08');
INSERT INTO `system_menu` VALUES ('37', '202208301103400b2ee7a4-cc44-4f4a-b121-73487d952dae', '开发者', '00990004', '', '', '20', '10', '10', '6', '2022-09-27 14:45:08', '2022-09-27 14:45:08');
INSERT INTO `system_menu` VALUES ('38', '2022090810383255000d69-601b-4706-a3b2-7997be335f2e', '系统参数', '009900020004', 'system/config/config_list.html', '', '20', '10', '10', '6', '2022-09-27 14:45:08', '2022-09-27 14:45:08');
INSERT INTO `system_menu` VALUES ('39', '20220908103851d3e1e8cb-2354-4a5c-b2ae-d5e9bf7f5b55', '数据字典', '009900020005', 'system/datadic/codemian_list.html', '', '20', '10', '10', '5', '2022-09-27 14:45:08', '2022-09-27 14:45:08');
INSERT INTO `system_menu` VALUES ('40', '202209081040045a5b0794-a0b1-48e1-a819-c4f749fefdff', '源码查看', '009900040002', 'https://gitee.com/code2roc/fast-boot', '', '20', '20', '10', '6', '2022-09-27 14:45:08', '2023-02-02 14:56:17');
INSERT INTO `system_menu` VALUES ('41', '2022092816504265888641-b4e5-435e-a527-95c044b04276', '枚举字典', '009900030003', 'system/datadic/enumcode_list.html', '', '20', '10', '10', '7', '2022-09-28 16:50:43', '2022-09-28 17:04:38');
INSERT INTO `system_menu` VALUES ('42', '20221213180042e032a134-b6ea-43d2-b506-ed20c59211f5', '表单示例', '009900040003', 'system/example/form_example.html', '', '20', '10', '10', '8', '2022-12-13 18:00:43', '2022-12-13 18:01:21');
INSERT INTO `system_menu` VALUES ('43', '2023020214564736ea39fa-1da0-4f6e-9504-62d3fa100a47', '模块示例', '009900040004', 'system/example/example_list.html', '', '20', '10', '10', '7', '2023-02-02 14:56:47', '2023-02-02 14:56:47');
INSERT INTO `system_menu` VALUES ('44', '202302081408026943646c-1798-4d50-b208-948504c7c246', '系统缓存', '009900030004', 'system/cache/cache_list.html', '', '20', '10', '10', '6', '2023-02-08 14:08:02', '2023-02-08 14:08:02');

-- ----------------------------
-- Table structure for system_menu_setting
-- ----------------------------
DROP TABLE IF EXISTS `system_menu_setting`;
CREATE TABLE `system_menu_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `row_id` varchar(50) DEFAULT NULL,
  `menu_id` varchar(50) DEFAULT NULL COMMENT '菜单id',
  `setting_mode` int(11) DEFAULT NULL COMMENT '权限模式',
  `setting_id` varchar(50) DEFAULT NULL COMMENT '权限id',
  `sort_num` int(11) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COMMENT='菜单权限配置表';

-- ----------------------------
-- Records of system_menu_setting
-- ----------------------------
INSERT INTO `system_menu_setting` VALUES ('41', '202209081057065de9ab11-ca0e-42b3-974b-25bc29c81936', '20220830110024bb171c3b-a741-4f4f-ae79-a9bdf2e655c1', '20', '202209021416046dfe1117-cb15-4182-b327-ccf0bad5036b', '0', '2022-09-27 14:45:08', '2022-09-27 14:45:08');
INSERT INTO `system_menu_setting` VALUES ('42', '20220908105706ac87b62a-8e64-4588-a824-88e6bb00d495', '20220830110024bb171c3b-a741-4f4f-ae79-a9bdf2e655c1', '20', '20220902141611c76e47f8-030a-4877-9b58-6bf06ed9652f', '0', '2022-09-27 14:45:08', '2022-09-27 14:45:08');
INSERT INTO `system_menu_setting` VALUES ('43', '202209081057061d382099-948f-497f-847f-e5d26a916f39', '20220830110024bb171c3b-a741-4f4f-ae79-a9bdf2e655c1', '20', '20220902141617c56fa00e-b82b-4c3f-9432-6075569da81c', '0', '2022-09-27 14:45:08', '2022-09-27 14:45:08');
INSERT INTO `system_menu_setting` VALUES ('44', '202209081057065f6160aa-aca5-4d73-8c38-639b0873fe5a', '20220830110024bb171c3b-a741-4f4f-ae79-a9bdf2e655c1', '30', '20220822112728225efcd4-3912-4f2d-b40b-87565505456c', '0', '2022-09-27 14:45:08', '2022-09-27 14:45:08');
INSERT INTO `system_menu_setting` VALUES ('45', '20220908105707bae22bcc-02e6-4b79-830c-212d0fc13b3d', '20220830110024bb171c3b-a741-4f4f-ae79-a9bdf2e655c1', '10', '202208221411296e9deb1b-c29e-4015-a4b9-52dfa240e730', '0', '2022-09-27 14:45:08', '2022-09-27 14:45:08');

-- ----------------------------
-- Table structure for system_role
-- ----------------------------
DROP TABLE IF EXISTS `system_role`;
CREATE TABLE `system_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `row_id` varchar(50) DEFAULT NULL,
  `role_name` varchar(100) DEFAULT NULL COMMENT '角色名称',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `group_id` varchar(50) DEFAULT NULL COMMENT '分组id',
  `system_init` int(11) DEFAULT NULL COMMENT '是否系统内置',
  `sort_num` int(11) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COMMENT='角色表';

-- ----------------------------
-- Records of system_role
-- ----------------------------
INSERT INTO `system_role` VALUES ('3', '202208221411296e9deb1b-c29e-4015-a4b9-52dfa240e730', '系统管理员', 'null', '2022081817531661299e29-c996-47a5-98b6-f2d96b46fd80', '10', '99', '2022-09-27 14:33:52', '2022-09-27 14:33:52');
INSERT INTO `system_role` VALUES ('4', '202208221748310160df54-880e-4e5d-8b13-54c4f502c279', '部门管理员', 'null', '2022081817531661299e29-c996-47a5-98b6-f2d96b46fd80', '20', '0', '2022-09-27 14:33:52', '2022-09-27 14:33:52');

-- ----------------------------
-- Table structure for system_role_setting
-- ----------------------------
DROP TABLE IF EXISTS `system_role_setting`;
CREATE TABLE `system_role_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `row_id` varchar(50) DEFAULT NULL,
  `user_id` varchar(50) DEFAULT NULL COMMENT '用户id',
  `role_id` varchar(50) DEFAULT NULL COMMENT '角色id',
  `sort_num` int(11) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COMMENT='用户角色关系表';

-- ----------------------------
-- Records of system_role_setting
-- ----------------------------
INSERT INTO `system_role_setting` VALUES ('17', '202208231457465d445008-a8ec-4d4c-96d4-b248f4c563bf', '20220822112728225efcd4-3912-4f2d-b40b-87565505456c', '202208221411296e9deb1b-c29e-4015-a4b9-52dfa240e730', '0', '2022-09-27 14:33:53', '2022-09-27 14:33:53');

-- ----------------------------
-- Table structure for system_table
-- ----------------------------
DROP TABLE IF EXISTS `system_table`;
CREATE TABLE `system_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `row_id` varchar(50) DEFAULT NULL,
  `table_name` varchar(100) DEFAULT NULL COMMENT '物理表名',
  `table_description` varchar(100) DEFAULT NULL COMMENT '表名',
  `package_name` varchar(100) DEFAULT NULL COMMENT '包名',
  `route_path` varchar(100) DEFAULT NULL COMMENT '路由',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `sort_num` int(11) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8mb4 COMMENT='生成代码表信息表';

-- ----------------------------
-- Records of system_table
-- ----------------------------
INSERT INTO `system_table` VALUES ('17', '202212131514300c0034a8-ebce-44e9-963b-23dc8c75708f', 'system_example', '系统示例表', 'com.code2roc.fastboot.system.codegen', '/system/codegen/example', '开发示例', '99', '2022-12-13 17:14:00', null);

-- ----------------------------
-- Table structure for system_user
-- ----------------------------
DROP TABLE IF EXISTS `system_user`;
CREATE TABLE `system_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `row_id` varchar(50) DEFAULT NULL,
  `dept_id` varchar(255) DEFAULT NULL COMMENT '部门id',
  `login_name` varchar(100) DEFAULT NULL COMMENT '登录名',
  `password` varchar(100) DEFAULT NULL COMMENT '密码',
  `user_name` varchar(100) DEFAULT NULL COMMENT '用户名',
  `user_status` int(11) DEFAULT NULL COMMENT '用户状态',
  `mobile` varchar(100) DEFAULT NULL COMMENT '手机',
  `email` varchar(100) DEFAULT NULL COMMENT '邮件',
  `sort_num` int(11) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COMMENT='用户表';

-- ----------------------------
-- Records of system_user
-- ----------------------------
INSERT INTO `system_user` VALUES ('3', '20220822112728225efcd4-3912-4f2d-b40b-87565505456c', '20220821221046d4e18514-72fb-49f0-870d-e466034a153f', 'sysadmin', '7415da5eef383f0c6f2175b20d135810', '系统管理员', '10', '13822222222', '123@qq.com', '99', '2022-08-22 11:27:29', '2022-09-28 16:30:26');
INSERT INTO `system_user` VALUES ('5', '20220822174817ab0b3514-47db-4027-9b45-196622847525', '20220821221046d4e18514-72fb-49f0-870d-e466034a153f', 'xxxx', 'fbee4d164bec478a686dc1aa639ddc6a', 'xxx', '20', '', '', '0', '2022-09-27 14:07:33', '2022-09-27 14:07:33');
