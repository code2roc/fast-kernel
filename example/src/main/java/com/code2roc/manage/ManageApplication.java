package com.code2roc.manage;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@MapperScan(basePackages = {"com.code2roc.fastboot.*.dao", "com.code2roc.fastboot.*.*.dao"})
@SpringBootApplication(scanBasePackages = {"com.code2roc"})
@ComponentScan(basePackages = {"com.code2roc"})
public class ManageApplication {

    public static void main(String[] args) {
        SpringApplication.run(ManageApplication.class, args);
    }

}
